(function () {
    'use strict';

    angular
        .module('app', [
            'ngStorage',
            // My Init Module
            'app.config',
            'app.routes',

            // third modules
            'file-model',
            'angular.snackbar',
            '720kb.datepicker',
            '720kb.tooltips',
            'cgBusy',
            'angularValidator',
            'ngTable',
            'angular.panels',
            'pageslide-directive',


            // My modules
            'app.authentication',
            'app.profile',
            'app.utils',
            'app.layout',
            'app.company',
            'app.product',
            'app.category',
            'app.catalog',
            'app.permission',
            'app.price-list',
            'app.user-type',
            'app.user',
            'app.order',
            'app.dispatch'
        ]).value('cgBusyDefaults',{
              message:'Procesando solicitud...',
              backdrop: false,
              templateUrl: '/static/util-templates/load.html',
              delay: 300,
              minDuration: 700,
              wrapperClass: 'cg-busy cg-busy-backdrop'
        });

    angular
        .module('app.routes', ['ui.router']);

    angular
        .module('app.config', []);



    angular
        .module('app')
        .run(run);

    run.$inject = ['$http']; //, '$state', '$stateParams'

    /**
     * @name run
     * @desc Update xsrf $http headers to align with Django's defaults
     */
    function run($http) {

        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';

//        $rootScope.$on('$locationChangeSuccess', function(evt) {
//            // Halt state change from even starting
//            evt.preventDefault();
//            // Perform custom logic
//            var meetsRequirement = false;
//            if($location.$$path != '/register' && $location.$$path != '/companies/activation-success/' &&
//                $location.$$path != '/companies/activation-is-active/' && $location.$$path != '/companies/activation-error/'){
//                if(Authentication.isAuthenticated()){
//                    var user = Authentication.getAuthenticatedAccount();
//                    if(user != undefined && user.data_required == true){
//                        meetsRequirement = true;
//                    }else{
//                        $state.go('company-data-required');
//                    }
//                }else{
//                    $location.path('/login');
//                }
//            }else{
//                meetsRequirement = true;
//            }
//            // Continue with the update and state transition if logic allows
//            if (meetsRequirement) $urlRouter.sync();
//        });



//        $rootScope.$state = $state;
//        $rootScope.$stateParams = $stateParams;
    }
})();
