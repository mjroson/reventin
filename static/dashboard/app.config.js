(function () {
    'use strict';

    angular
        .module('app.config')
        .config(config);

    config.$inject = ['$locationProvider'];

    /**
     * @name config
     * @desc Enable HTML5 routing
     */
    function config($locationProvider) {
        // permite que las url no lleven el caracter # al inicio de ellas, (como lo utiliza por defecto angular)
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }
})();
