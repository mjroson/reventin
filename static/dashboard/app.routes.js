(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    /**
     * @name config
     * @desc Define valid application routes
     */
    function config($stateProvider, $urlRouterProvider) {



        $stateProvider
            /*.state('config-attr-product', {
                url: '/companies/config-attributes-products',
                templateUrl: '/static/dashboard/modules/company/templates/config-attrs.html',
                controller: 'ConfigAttributeController',
                controllerAs: 'vm'
            })

            .state('config-attr-user', {
                url: '/companies/config-attributes-user',
                templateUrl: '/static/dashboard/modules/company/templates/config-attrs.html',
                controller: 'ConfigAttributeController',
                controllerAs: 'vm'
            })*/

            .state('products', {
                url: '/products',
                templateUrl: '/static/dashboard/modules/product/templates/list-product.html',
                controller: 'ListProductController',
                controllerAs: 'vm'
            })

            /*.state('product-create', {
                url: '/products-create',
                templateUrl: '/static/dashboard/modules/product/templates/new-product.html',
                controller: 'NewProductController',
                controllerAs: 'vm'
            })*/

            .state('product-update', {
                url: '/product-update/:id',
                templateUrl: '/static/dashboard/modules/product/templates/update-product.html',
                controller: 'UpdateProductController',
                controllerAs: 'vm'
            })

            .state('product-csv-import', {
                url: '/product-import/csv',
                templateUrl: '/static/dashboard/modules/product/templates/import-csv-product.html',
                controller: 'ImportCsvProductController',
                controllerAs: 'vm'
            })

            .state('categories', {
                url: '/categories/',
                templateUrl: '/static/dashboard/modules/category/templates/list-category.html',
                controller: 'ListCategoryController',
                controllerAs: 'vm'
            })

            /*.state('category-create', {
                url: '/categories-create',
                templateUrl: '/static/dashboard/modules/category/templates/new-category.html',
                controller: 'NewCategoryController',
                controllerAs: 'vm'
            })

            .state('category-update', {
                url: '/category-update/:id',
                templateUrl: '/static/dashboard/modules/category/templates/new-category.html',
                controller: 'UpdateCategoryController',
                controllerAs: 'vm'
            })*/

            .state('catalogs', {
                url: '/catalogs/',
                templateUrl: '/static/dashboard/modules/catalog/templates/list-catalog.html',
                controller: 'ListCatalogController',
                controllerAs: 'vm'
            })
            .state('catalog-update', {
                url: '/catalog-update/:id',
                templateUrl: '/static/dashboard/modules/catalog/templates/update-catalog.html',
                controller: 'UpdateCatalogController',
                controllerAs: 'vm'
            })

            .state('catalog-create', {
                url: '/catalog-create',
                templateUrl: '/static/dashboard/modules/catalog/templates/new-catalog.html',
                controller: 'NewCatalogController',
                controllerAs: 'vm'
            })

            .state('catalog-detail', {
                url: '/catalog-detail/:id',
                templateUrl: '/static/dashboard/modules/catalog/templates/detail-catalog.html',
                controller: 'DetailCatalogController',
                controllerAs: 'vm'
            })
            
            .state('permissions', {
                url: '/permission/',
                templateUrl: '/static/dashboard/modules/permission/templates/list-permission.html',
                controller: 'ListPermissionController',
                controllerAs: 'vm'
            })
//            .state('permission-update', {
//                url: '/permission-update/:id',
//                templateUrl: '/static/dashboard/modules/permission/templates/update-permission.html',
//                controller: 'UpdatePermissionController',
//                controllerAs: 'vm'
//            })
//
//            .state('permission-create', {
//                url: '/permission-create',
//                templateUrl: '/static/dashboard/modules/permission/templates/new-permission.html',
//                controller: 'NewPermissionController',
//                controllerAs: 'vm'
//            })
//
//            .state('permission-detail', {
//                url: '/permission-detail/:id',
//                templateUrl: '/static/dashboard/modules/permission/templates/detail-permission.html',
//                controller: 'DetailPermissionController',
//                controllerAs: 'vm'
//            })


            .state('price-lists', {
                url: '/price-list/',
                templateUrl: '/static/dashboard/modules/price-list/templates/list-price-list.html',
                controller: 'ListPriceListController',
                controllerAs: 'vm'
            })
            /*.state('price-list-update', {
                url: '/price-list-update/:id',
                templateUrl: '/static/dashboard/modules/price-list/templates/update-price-list.html',
                controller: 'UpdatePriceListController',
                controllerAs: 'vm'
            })

            .state('price-list-create', {
                url: '/price-list-create',
                templateUrl: '/static/dashboard/modules/price-list/templates/new-price-list.html',
                controller: 'NewPriceListController',
                controllerAs: 'vm'
            })

            .state('price-list-detail', {
                url: '/price-list-detail/:id',
                templateUrl: '/static/dashboard/modules/price-list/templates/detail-price-list.html',
                controller: 'DetailPriceListController',
                controllerAs: 'vm'
            })*/


            .state('user-types', {
                url: '/user-types/',
                templateUrl: '/static/dashboard/modules/user-type/templates/list-user-type.html',
                controller: 'ListUserTypeController',
                controllerAs: 'vm'
            })
           /* .state('user-type-update', {
                url: '/user-type-update/:id',
                templateUrl: '/static/dashboard/modules/user-type/templates/update-user-type.html',
                controller: 'UpdateUserTypeController',
                controllerAs: 'vm'
            })

            .state('user-type-create', {
                url: '/user-type-create',
                templateUrl: '/static/dashboard/modules/user-type/templates/new-user-type.html',
                controller: 'NewUserTypeController',
                controllerAs: 'vm'
            })

            .state('user-type-detail', {
                url: '/user-type-detail/:id',
                templateUrl: '/static/dashboard/modules/user-type/templates/detail-user-type.html',
                controller: 'DetailUserTypeController',
                controllerAs: 'vm'
            })*/

            .state('users', {
                url: '/users/',
                templateUrl: '/static/dashboard/modules/user/templates/list-user.html',
                controller: 'ListUserController',
                controllerAs: 'vm'
            })
            /*.state('user-update', {
                url: '/users-update/:id',
                templateUrl: '/static/dashboard/modules/user/templates/new-user.html',
                controller: 'NewUserController',
                controllerAs: 'vm'
            })

            .state('user-create', {
                url: '/users-create',
                templateUrl: '/static/dashboard/modules/user/templates/new-user.html',
                controller: 'NewUserController',
                controllerAs: 'vm'
            })

            .state('user-create-email', {
                url: '/users-create-by-email',
                templateUrl: '/static/dashboard/modules/user/templates/new-user-by-email.html',
                controller: 'NewUserEmailController',
                controllerAs: 'vm'
            })

            .state('user-detail', {
                url: '/users/:id',
                templateUrl: '/static/dashboard/modules/user/templates/detail-user.html',
                controller: 'DetailUserController',
                controllerAs: 'vm'
            })*/

            .state('orders', {
                url: '/orders/',
                templateUrl: '/static/dashboard/modules/order/templates/list-order.html',
                controller: 'ListOrderController',
                controllerAs: 'vm'
            })

            .state('order-create', {
                url: '/orders-create',
                templateUrl: '/static/dashboard/modules/order/templates/new-order.html',
                controller: 'NewOrderController',
                controllerAs: 'vm'
            })

            .state('order-detail', {
                url: '/orders/:id',
                templateUrl: '/static/dashboard/modules/order/templates/detail-order.html',
                controller: 'DetailOrderController',
                controllerAs: 'vm'
            })

            .state('dispatchs', {
                url: '/dispatchs/',
                templateUrl: '/static/dashboard/modules/dispatch/templates/list-dispatch.html',
                controller: 'ListDispatchController',
                controllerAs: 'vm'
            })

            .state('dispatch-create', {
                url: '/dispatchs-create',
                templateUrl: '/static/dashboard/modules/dispatch/templates/new-dispatch.html',
                controller: 'NewDispatchController',
                controllerAs: 'vm'
            })

            .state('dispatch-detail', {
                url: '/dispatchs/:id',
                templateUrl: '/static/dashboard/modules/dispatch/templates/detail-dispatch.html',
                controller: 'DetailDispatchController',
                controllerAs: 'vm'
            })



            .state('profile', {
                url: '/panel/',
                templateUrl: '/static/dashboard/modules/profile/templates/panel.html',
                controller: 'PanelController',
                controllerAs: 'vm'
            })
            .state('profile-update', {
                url: '/profile-update',
                templateUrl: '/static/dashboard/modules/profile/templates/edit-profile.html',
                controller: 'UpdateProfileController',
                controllerAs: 'vm'
            })

            /*
            .state('company-update', {
                url: '/company-update',
                templateUrl: '/static/dashboard/modules/company/templates/update-company.html',
                controller: 'CompanyUpdateController',
                controllerAs: 'vm'
            })*/

            .state('settings', {
                url: '/company-update',
                templateUrl: '/static/dashboard/modules/company/templates/settings.html',
                controller: 'SettingsController',
                controllerAs: 'vm'
            })
        ;

        $urlRouterProvider.otherwise('/panel/');
    }
})();
