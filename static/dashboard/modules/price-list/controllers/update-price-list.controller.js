/**
 * UpdatePriceListController
 * @namespace app.price-list.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.price-list.controllers')
        .controller('UpdatePriceListController', UpdatePriceListController);

    UpdatePriceListController.$inject = ['AlertNotification', 'PriceLists', '$rootScope', '$scope'];

    /**
     * @namespace UpdatePriceListController
     */
    function UpdatePriceListController(AlertNotification, PriceLists, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;
        vm.obj = {};

        vm.old_obj = {};
        vm.close = close;

        vm.edit = function(){
            vm.objEdit = !vm.objEdit;
        }

        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
            vm.objEdit = false;
        }

        activate();

        function submit() {
            PriceLists.update(vm.obj)
                .then(updatePriceListSuccessFn, updatePriceListErrorFn);

            function updatePriceListSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.edited', data.data, vm.old_obj);
                vm.objEdit = false;
                AlertNotification.success('Success! Price List created.');
            }

            function updatePriceListErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }

        function activate(){
            $scope.$on('objData', function(event, args){
                vm.old_obj = args;
                vm.obj = angular.copy(args);
            });
        }
    }
})();
