/**
 * DetailPriceListController
 * @namespace app.price-list.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.price-list.controllers')
        .controller('DetailPriceListController', DetailPriceListController);

    DetailPriceListController.$inject = ['AlertNotification', 'PriceLists', '$stateParams'];

    /**
     * @namespace DetailPriceListController
     */
    function DetailPriceListController(AlertNotification, PriceLists, $stateParams) {
        var vm = this;

        vm.price_list = {};
        init();

        /**
         * @name init
         * @desc Call service to request detail price list
         * @memberOf app.price-list.controllers.DetailPriceListController
         */
        function init(){
            PriceLists.detail($stateParams.id).then(getDetailPriceListSuccess, getDetailPriceListError);

            function getDetailPriceListSuccess(data, status, headers, config){
                vm.price_list = data.data;
            }

            function getDetailPriceListError(data, status, headers, config){
                AlertNotification.success('Error request price list detail');
            }
        }

    }
})();
