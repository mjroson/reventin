/**
 * NewPriceListController
 * @namespace app.price-list.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.price-list.controllers')
        .controller('NewPriceListController', NewPriceListController);

    NewPriceListController.$inject = ['AlertNotification', 'PriceLists', '$rootScope', '$scope'];

    /**
     * @namespace NewPriceListController
     */
    function NewPriceListController(AlertNotification, PriceLists, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;

        vm.obj = {};

        vm.close = close;

        function close(){
            $scope.$parent.$parent.vm.objNew = !$scope.$parent.$parent.vm.objNew;
        }

        vm.cancel = function(){
            $scope.formCreatePriceList.reset();
            vm.obj = {};
            close();
        }

        /**
         * @name submit
         * @desc Create a new Catalog
         * @memberOf app.price-list.controllers.NewPriceListController
         */
        function submit() {
            PriceLists.create(vm.obj)
                .then(createCatalogSuccessFn, createCatalogErrorFn);

            /**
             * @name createCatalogSuccessFn
             */
            function createCatalogSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.created', data.data);
                $scope.formCreatePriceList.reset();
                vm.obj = {};
                close();
                AlertNotification.success('Success! Price List created.');

            }

            /**
             * @name createCatalogErrorFn
             */
            function createCatalogErrorFn(data, status, headers, config) {
                AlertNotification.error("Error! Cant created Price list");
            }
        }


    }
})();
