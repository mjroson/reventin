(function () {
    'use strict';

    angular
        .module('app.price-list', [
            'app.price-list.controllers',
            'app.price-list.services'
        ]);

    angular
        .module('app.price-list.controllers', ['ngDialog']); //'mgcrea.ngStrap',

    angular
        .module('app.price-list.services', []);

})();