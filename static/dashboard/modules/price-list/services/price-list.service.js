/**
 * PriceLists
 * @namespace app.price-list.services
 */
(function () {
    'use strict';

    angular
        .module('app.price-list.services')
        .factory('PriceLists', PriceLists);

    PriceLists.$inject = ['$http'];

    /**
     * @namespace PriceLists
     * @returns {Factory}
     */
    function PriceLists($http) {

        var PriceLists = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            update: update
        };

        return PriceLists;

        ////////////////////

        /**
         * @name all
         * @desc Get all Catalogs
         * @returns {Promise}
         * @memberOf app.price-list.services.Catalogs
         */
        function all() {
            return $http.get('/api/price-list/');
        }


        /**
         * @name create
         * @desc Create a new Catalog
         * @param {Objects} Objects to data info price-list, and details price-list data.
         * @returns {Promise}
         * @memberOf app.price-list.services.Catalogs
         */
        function create(data) {

            return $http.post('/api/price-list/', data );
        }

        /**
         * @name destroy
         * @Delete a Catalog
         * @returns {Promise}
         * @memberOf app.price-list.services.PriceList
         */
        function destroy(id) {
            return $http.delete('/api/price-list/' + id + "/");
        }


        /**
         * @name detail
         * @Detail a Price List
         * @returns {Promise}
         * @memberOf app.price-lists.services.PriceList
         */
        function detail(id) {
            return $http.get('/api/price-list/' + id + "/");
        }


        /**
         * @name update
         * @desc Update a Price Lists
         * @param {Object} object to data price lists
         * @returns {Promise}
         * @memberOf app.price-list.services.PriceList
         */
        function update(data) {
            return $http.put('/api/price-list/' + data.id + '/', data);
        }

    }
})();