/**
 * Company
 * @namespace app.company.services
 */
(function () {
    'use strict';

    angular
        .module('app.company.services')
        .factory('Companies', Companies);

    Companies.$inject = ['$http'];

    /**
     * @namespace Product
     * @returns {Factory}
     */
    function Companies($http) {

        var Companies = {
            data_required: data_required,
            config_attr_update: config_attr_update,
            config_attribute_create: config_attr_create,
            get_attr_product: get_attr_product,
            config_attr_user: config_attr_user,
            detail: get_company,
            update: update,
            get_attr_user: get_attr_user

        };

        return Companies;


        /**
         * @name create
         * @desc Create a new Product
         * @param {status_stock} indica si la empresa trabaja con stock
         * @param {status_points} indica si la empresa trabaja con puntos
         * @param {currency} tipo de moneda
         * @param {logo} logotipo
         * @param {country_work} paises en los que trabaja
         * @returns {Promise}
         * @memberOf app.company.services.Companies
         */
        function data_required(logo, vm) {
            var fd = new FormData();
            //Take the first selected file
            //fd.append('country_work', vm.country_work);
            fd.append('currency', vm.money);
            fd.append('work_stock', vm.work_stock);
            fd.append('work_point', vm.work_point);
            var countries_work = [];
            countries_work.push(vm.country_work);
            fd.append('country_work', countries_work);

            fd.append("logo", logo);
            return $http.post('/api/companies/data-required/', fd, {
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            });
        }

        /**
         * @name config_attribute_create
         * @Create Custom attributes to products
         * @returns {Promise}
         * @memberOf app.company.services.Company
         */
        function config_attr_create(properties){
            return $http.patch('/api/companies/cfg-attr-product/create/', {
                properties: properties
            })
        }

        /**
         * @name config_attribute_update
         * @Update Custom attributes to products
         * @returns {Promise}
         * @memberOf app.company.services.Company
         */
        function config_attr_update(properties){
            return $http.patch('/api/companies/cfg-attr-product/update/', {
                properties: properties
            })
        }

        /**
         * @name get_attr_product
         * @List Custom attributes to products
         * @returns list custom attribute to products
         * @memberOf app.company.services.Company
         */
        function get_attr_product(){
            return $http.get('/api/companies/cfg-attr-product/all/');
        }

        function config_attr_user(properties, belongs_to){
            return $http.patch('/api/companies/config-attributes/'+ belongs_to +'/', {
                properties: properties
            });

        }

        function get_attr_user(belongs_to){
            return $http.get('/api/companies/config-attributes/'+ belongs_to +'/all/');

        }

        function get_company(){
            return $http.get('/api/company/');
        }

        function update(company){
            return $http.put('/api/company/update/', company);
        }
    }
})()