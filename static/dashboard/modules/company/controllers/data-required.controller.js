/**
 * DataRequiredController
 * @namespace app.company.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.company.controllers')
        .controller('DataRequiredController', DataRequiredController);

    DataRequiredController.$inject = ['$scope', 'AlertNotification', 'Companies', '$state', 'Authentication'];

    /**
     * @namespace DataRequiredController
     */
    function DataRequiredController($scope, AlertNotification, Companies, $state, Authentication) {
        var vm = this;

        vm.submit = submit;

        /**
         * @name submit
         * @desc Set data required
         * @memberOf app.company.controllers.DataRequiredController
         */
        function submit() {

            Companies.data_required($scope.logo, vm) //.country_work, vm.money, vm.work_stock, vm.work_point
                .then(setDataRequiredSuccessFn, setDataRequiredErrorFn);


            /**
             * @name setDataRequiredSuccessFn
             * @desc Show snackbar with success message
             */
            function setDataRequiredSuccessFn(data, status, headers, config) {
                AlertNotification.success('Bien! Ahora puedes empezar a usar Nubiquo');
                var user = Authentication.getAuthenticatedAccount();
                user.data_required = true;
                user.logo = data.data.logo_url;
                //Authentication.setAuthenticatedAccount(user);
                $state.go('products');
            }


            /**
             * @name setDataRequiredErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function setDataRequiredErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('product.created.error');
                AlertNotification.error(data.error);
            }
        }
    }
})();
