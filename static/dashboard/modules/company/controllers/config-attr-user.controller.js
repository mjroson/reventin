/**
 * ConfigAttributeUserController
 * @namespace app.company.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.company.controllers')
        .controller('ConfigAttributeUserController', ConfigAttributeUserController);

    ConfigAttributeUserController.$inject = ['AlertNotification', 'Companies'];
    /**
     * @namespace ConfigAttributeController
     */
    function ConfigAttributeUserController(AlertNotification, Companies) {
        var vm = this;

        vm.submit = submit;
        vm.destroy = destroy;
        vm.restore = restore;
        vm.add = add;

        vm.properties = [];
        vm.values = [];

        activate();
        /**
         * @name submit
         * @desc submit change custom attribute
         * @memberOf app.company.controllers.ConfigAttributeUserController
         */
        function submit() {

            Companies.config_attr_user(vm.properties, vm.belongs_to)
                .then(updateCfgAttrSuccessFn, updateCfgAttrErrorFn);

            /**
             * @name updateCfgAttrSuccessFn
             * @desc Show snackbar with success message
             */
            function updateCfgAttrSuccessFn(data, status, headers, config) {
                AlertNotification.success("Los attributos se modificacion exitosamente.");
            }


            /**
             * @name updateCfgAttrErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function updateCfgAttrErrorFn(data, status, headers, config) {
                AlertNotification.error("Error al modificar los atributos, vuelva a intentarlo.");
            }
        }

        /**
         * @name activate
         * @desc init exec function by call service to set list custom attributes
         */
        function activate(){
            vm.belongs_to = 'user';
            vm.title = 'usuarios';
            Companies.get_attr_user(vm.belongs_to).then(getSuccess, getError);


            function getSuccess(data, headers, status){
                vm.properties = data.data;
            }

            function getError(data, headers, status){
                AlertNotification.error(data.data);
            }
        }


        /**
         * @name add propertie
         * @param {object} Property object
         * @desc change status (true) to custom attribute
         */
        function add(){
            var prop = {};
            prop.attr_name = "";
            prop.attr_type = "";
            prop.attr_default = "";
            prop.attr_required = false;
            prop.attr_values = [];
            prop.status = 1;
            prop.is_new = true;
            vm.properties.push(prop);
        }


        /**
         * @name destroy
         * @param {object} Property object
         * @desc change status (false) to custom attribute
         */
        function destroy(prop){
            vm.properties[vm.properties.indexOf(prop)].status = 0;
        }

        /**
         * @name restore
         * @param {object} Property object
         * @desc change status (true) to custom attribute
         */
        function restore(prop){
            vm.properties[vm.properties.indexOf(prop)].status = 1;

        }

    }
})();

