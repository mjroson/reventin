/**
 * CompanyUpdateController
 * @namespace app.company.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.company.controllers')
        .controller('CompanyUpdateController', CompanyUpdateController);

    CompanyUpdateController.$inject = ['$scope', 'AlertNotification', 'Companies', 'Authentication'];

    /**
     * @namespace CompanyUpdateController
     */
    function CompanyUpdateController(AlertNotification, Companies, Authentication) {
        var vm = this;

        vm.submit = submit;

        /**
         * @name submit
         * @desc Set data required
         * @memberOf app.company.controllers.CompanyUpdateController
         */
        function submit() {
            vm.company.country_work = [];
            vm.company.country_work.push(vm.country_work);
            Companies.update(vm.company)
                .then(setUpdateSuccessFn, setUpdateErrorFn);


            /**
             * @name setDataRequiredSuccessFn
             * @desc Show snackbar with success message
             */
            function setUpdateSuccessFn(data, status, headers, config) {
                Authentication.setCompanyName(vm.company.company_name);

                //$state.go('profile');
            }


            /**
             * @name setDataRequiredErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function setUpdateErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }

        activate();
        function activate(){
            Companies.detail().then(getCompanySuccess, getCompanyError);

            function getCompanySuccess(data, status, headers, config){
                vm.company = data.data;

                if(vm.company.country_work.length > 0){
                    vm.country_work = vm.company.country_work[0];
                }
            }

            function getCompanyError(data, status, headers, config){
                AlertNotification.success("Error al intentar acceder a los datos de la empresa");
                //$state.go('profile');
            }
        }
    }
})();
