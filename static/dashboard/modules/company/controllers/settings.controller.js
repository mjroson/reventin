/**
 * SettingsController
 * @namespace app.company.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.company.controllers')
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = [];
        // TODO: User Este controlador y el template config-attrs.html tanto para atributos de usuario como de producto.
    /**
     * @namespace SettingsController
     */
    function SettingsController() {
        var vm = this;

        vm.tab = 1;

    }
})();
