(function () {
    'use strict';

    angular
        .module('app.company', [
            'app.company.controllers',
            //'app.product.directives',
            'app.company.services'
        ]);

    angular
        .module('app.company.controllers', []);

    /*angular
        .module('app.product.directives', ['ngDialog']);
    */
    angular
        .module('app.company.services', []);

})();