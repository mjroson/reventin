/**
 * NewCategoryController
 * @namespace app.category.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.category.controllers')
        .controller('NewCategoryController', NewCategoryController);

    NewCategoryController.$inject = ['AlertNotification', 'Categories', '$rootScope', '$scope'];

    /**
     * @namespace NewCategoryController
     */
    function NewCategoryController(AlertNotification, Categories, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;
        vm.close = close;
        vm.cancel = cancel;


        vm.obj = {};

        function close(){
            $scope.$parent.$parent.vm.objNew = !$scope.$parent.$parent.vm.objNew;
        }

        function cancel(){
            $scope.formCreateCategory.reset();
            vm.obj = {};
            close();
        }


        /**
         * @name submit
         * @desc Create a new Category
         * @memberOf app.category.controllers.NewCategoryController
         */
        function submit() {

            Categories.create(vm.obj)
                .then(createCategorySuccessFn, createCategoryErrorFn);


            /**
             * @name createCategorySuccessFn
             * @desc Show snackbar with success message
             */
            function createCategorySuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.created', data.data);
                $scope.formCreateCategory.reset();
                vm.obj = {};
                close();
                AlertNotification.success('Success! Category created.');
            }


            /**
             * @name createCategoryErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function createCategoryErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('category.created.error');
                AlertNotification.error(data.error);
            }
        }
    }
})();

