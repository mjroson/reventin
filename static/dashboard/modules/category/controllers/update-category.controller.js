/**
 * UpdateCategoryController
 * @namespace app.category.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.category.controllers')
        .controller('UpdateCategoryController', UpdateCategoryController);

    UpdateCategoryController.$inject = ['$rootScope', 'AlertNotification', 'Categories', '$scope'];

    /**
     * @namespace UpdateCategoryController
     */
    function UpdateCategoryController($rootScope, AlertNotification, Categories, $scope) {
        var vm = this;

        vm.submit = submit;

        vm.title = "Editando";
        activate();

        vm.close = close;

        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
            vm.objEdit = false;
        }

        /**
         * @name submit
         * @desc Update a Category
         * @memberOf app.category.controllers.UpdateCategoryController
         */
        function submit() {

            //vm.category.category_father = vm.category_father;
            Categories.update(vm.obj)
                .then(createCategorySuccessFn, createCategoryErrorFn);


            /**
             * @name createCategorySuccessFn
             * @desc Show snackbar with success message
             */
            function createCategorySuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.edited', data.data, vm.old_obj);
                close();
                AlertNotification.success('Success! Category edited.');
                $scope.formCreateCategory.reset();
                vm.obj = {};
            }


            /**
             * @name createCategoryErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function createCategoryErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('category.created.error');
                AlertNotification.error(data.error);
            }
        }

        /**
         * @name submit
         * @desc Update a Category
         * @memberOf app.category.controllers.UpdateCategoryController
         */
        function activate(){
            $scope.$on('objData', function(event, args){
                vm.old_obj = args;
                vm.obj = angular.copy(args);
            });
        }

    }
})();

