(function () {
    'use strict';

    angular
        .module('app.category', [
            'app.category.controllers',
            //'app.category.directives',
            'app.category.services'
        ]);


    angular
        .module('app.category.controllers', ['ngDialog']);

    /*angular
        .module('app.categories.directives', ['ngDialog']);
*/
    angular
        .module('app.category.services', []);


})();