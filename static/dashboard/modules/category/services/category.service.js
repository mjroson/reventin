/**
 * Categories
 * @namespace app.category.services
 */
(function () {
    'use strict';

    angular
        .module('app.category.services')
        .factory('Categories', Categories);

    Categories.$inject = ['$http'];

    /**
     * @namespace Category
     * @returns {Factory}
     */
    function Categories($http) {

        var Categories = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            update: update
        };

        return Categories;

        ////////////////////

        /**
         * @name all
         * @desc Get all Categories
         * @returns {Promise}
         * @memberOf app.category.services.Categories
         */
        function all() {
            return $http.get('/api/products/categories/');
        }


        /**
         * @name create
         * @desc Create a new Category
         * @param {Object} Object category
         * @returns {Promise}
         * @memberOf app.category.services.Categories
         */
        function create(category) {
            return $http.post('/api/products/categories/', {
                name: category.name,
                color: category.color,
                description: category.description,
                category_father: category.category_father
            });
        }

        /**
         * @name destroy
         * @Delete a Category
         * @returns {Promise}
         * @memberOf app.category.services.Categories
         */
        function destroy(id) {
            return $http.delete('/api/products/categories/' + id + '/');
        }


        /**
         * @name detail
         * @Detail a Category
         * @returns {Promise}
         * @memberOf app.category.services.Categories
         */
        function detail(id) {
            return $http.get('/api/products/categories/' + id + "/");
        }

        /**
         * @name update
         * @desc Update a Category
         * @param {Object} object category
         * @returns {Promise}
         * @memberOf app.category.services.Categories
         */
        function update(category) {
            return $http.put('/api/products/categories/' + category.id + '/', {
                name: category.name,
                color: category.color,
                description: category.description,
                category_father: category.category_father
            });
        }
    }
})();