/**
 * Catalogs
 * @namespace app.catalog.services
 */
(function () {
    'use strict';

    angular
        .module('app.catalog.services')
        .factory('Catalogs', Catalogs);

    Catalogs.$inject = ['$http'];

    /**
     * @namespace Catalogs
     * @returns {Factory}
     */
    function Catalogs($http) {

        var Catalogs = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            update: update,
            upload: upload,
            get_fields: get_fields,
            get_data_update: get_data_update
        };

        return Catalogs;

        ////////////////////

        /**
         * @name all
         * @desc Get all Catalogs
         * @returns {Promise}
         * @memberOf app.catalog.services.Catalogs
         */
        function all() {
            return $http.get('/api/catalogs/');
        }


        /**
         * @name create
         * @desc Create a new Catalog
         * @param {Objects} Objects to data info catalog, and details catalog data.
         * @returns {Promise}
         * @memberOf app.catalog.services.Catalogs
         */
        function create(data) {

            return $http.post('/api/catalogs/', data );
        }

        /**
         * @name destroy
         * @Delete a Catalog
         * @returns {Promise}
         * @memberOf app.catalog.services.Catalogs
         */
        function destroy(id) {
            return $http.delete('/api/catalogs/' + id + "/");
        }


        /**
         * @name detail
         * @Detail a Catalog
         * @returns {Promise}
         * @memberOf app.catalog.services.Catalogs
         */
        function detail(id) {
            return $http.get('/api/catalogs/' + id + "/");
        }


        /**
         * @name update
         * @desc Update a Catalog
         * @param {Object} object to data catalogs and details catalog
         * @returns {Promise}
         * @memberOf app.catalog.services.Catalogs
         */
        function update(data) {
            return $http.put('/api/catalogs/' + data.catalog.id + '/', data);
        }


        /**
         * @name create
         * @desc Upload a csv to update product
         * @param {object} object with file csv
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function upload(obj) {
            var fd = new FormData();
            fd.append('file_csv', obj.file_csv);
            fd.append('config', angular.toJson(obj.config));
            return $http.post('/api/products/upload-csv-import/', fd,{
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            } );
        }

        function get_fields(){
            return $http.get('/api/products/fields/');
        }

        function get_data_update(id){
            return $http.get('/api/catalogs/update/data/' + id);
        }

    }
})();