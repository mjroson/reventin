(function () {
    'use strict';

    angular
        .module('app.catalog', [
            'app.catalog.controllers',
            'app.catalog.services'
        ]);

    angular
        .module('app.catalog.controllers', ['ngDialog']); //'mgcrea.ngStrap',

    angular
        .module('app.catalog.services', []);

})();