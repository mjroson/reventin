/**
 * DetailCatalogController
 * @namespace app.catalog.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.product.controllers')
        .controller('DetailCatalogController', DetailCatalogController);

    DetailCatalogController.$inject = ['AlertNotification', 'Catalogs', '$stateParams'];

    /**
     * @namespace DetailCatalogController
     */
    function DetailCatalogController(AlertNotification, Catalogs, $stateParams) {
        var vm = this;

        vm.catalog = {};
        vm.catalog_details = [];
        init();

        /**
         * @name init
         * @desc Call service to request detail catalog
         * @memberOf app.catalog.controllers.DetailCatalogController
         */
        function init(){
            Catalogs.detail($stateParams.id).then(getDetailCatalogSuccess, getDetailCatalogError);

            function getDetailCatalogSuccess(data, status, headers, config){
                vm.catalog = data.data.catalog;
                vm.catalog_details = data.data.catalog_details;
            }

            function getDetailCatalogError(data, status, headers, config){
                AlertNotification.success('Error request catalogs');
            }
        }

    }
})();
