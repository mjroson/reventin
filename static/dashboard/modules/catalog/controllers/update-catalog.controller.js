/**
 * UpdateCatalogController
 * @namespace app.catalog.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.catalog.controllers')
        .controller('UpdateCatalogController', UpdateCatalogController);

    UpdateCatalogController.$inject = ['AlertNotification', 'Catalogs', '$state', '$stateParams'];

    /**
     * @namespace UpdateCatalogController
     */
    function UpdateCatalogController(AlertNotification, Catalogs, $state, $stateParams) {
        var vm = this;

        vm.submit = submit;
        vm.changeProductCheck = changeProductCheck;
        vm.changeFactor = changeFactor;
        vm.changeEditCustomPrice = changeEditCustomPrice;

        vm.fields_status = false;

        vm.products = [];
        vm.products_select = [];
        vm.catalog_details = [];



        activate();

        /**
         * @name submit
         * @desc Update a Catalog
         * @memberOf app.catalog.controllers.UpdateCatalogController
         */
        function submit() {
            //vm.catalog.start_date = $filter('date')(vm.catalog.start_date,'yyyy-MM-dd');
            //vm.catalog.end_date = $filter('date')(vm.catalog.end_date,'yyyy-MM-dd');
            Catalogs.update(vm)
                .then(updateCatalogSuccessFn, updateCatalogErrorFn);

            /**
             * @name updateCatalogSuccessFn
             */
            function updateCatalogSuccessFn(data, status, headers, config) {
                AlertNotification.success('Success! Product created.');
                $state.go('catalogs');
            }


            /**
             * @name updateCatalogErrorFn
             */
            function updateCatalogErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('product.created.error');
                AlertNotification.error(data.error);
            }
        }

        /**
         * Event change select product to include catalog
         * @param {Object} product
         */
        function changeProductCheck(product){
           if(product.check){
               calculatePriceByFactor(product);
               vm.products_select.push(product);
           }else{
               vm.products_select.splice(vm.products_select.indexOf(product), 1);
           }
        }

        /**
         * Event change factor increment
         */
        function changeFactor(){
            angular.forEach(vm.products_select, function(product){
                calculatePriceByFactor(product);
            });
        }

        /**
         * Event change edit custom price
         * @param product
         */
        function changeEditCustomPrice(product){
            calculatePriceByFactor(product);
        }

        /**
         * Calculate custom price with general factor increment
         * @param product
         */
        function calculatePriceByFactor(product){
            if(!product.edit_custom_price){
                var factor = 0;
                if(vm.catalog.factor != undefined && vm.catalog.factor != ""){
                    factor = parseInt(vm.catalog.factor);
                }
                product.custom_price = (factor / 100 * parseFloat(product.sale_price)) + parseFloat(product.sale_price);
            }
        }



        function activate(){
            Catalogs.get_data_update($stateParams.id).then(getAllProductSuccess, getAllProductError);

            function getAllProductSuccess(data, status, headers, config){

                vm.catalog = data.data.catalog;
                vm.products = data.data.products;
                vm.catalog_details = data.data.catalog_details;
                if(data.data.products_select){
                    vm.products_select = data.data.products_select;
                }
                if(vm.catalog.catalog_status == 1){
                    vm.fields_status = true;
                }
            }

            function getAllProductError(data, status, headers, config){
                AlertNotification.success('Error request categories');
            }
        }
    }
})();
