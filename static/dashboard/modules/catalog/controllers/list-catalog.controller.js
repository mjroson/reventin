/**
 * ListCatalogController
 * @namespace app.catalog.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.product.controllers')
        .controller('ListCatalogController', ListCatalogController);

    ListCatalogController.$inject = ['AlertNotification', 'Catalogs', '$scope', '$filter', 'ngTableParams'];

    /**
     * @namespace ListCatalogController
     */
    function ListCatalogController(AlertNotification, Catalogs, $scope, $filter, ngTableParams) {
        var vm = this;

        vm.destroy = destroy;

        vm.clear_text = clear_text;

        vm.obj_list = [];

        vm.search_field = 'name';

        vm.search_text = '';

        vm.filters = {
            name: "",
            start_date: "",
            end_date: ""
        };

        vm.edit_obj = function(obj){
            $scope.$broadcast('objData', obj);
            vm.objEdit = !vm.objEdit;
        }

        vm.change_search_text = change_search_text;

        function change_search_text(){
            // Clear All filter
            for(var filter in vm.filters){
                vm.filters[filter] = "";
            }

            vm.filters[vm.search_field] = vm.search_text;
        }

        function clear_text(){
            vm.search_text = "";
            change_search_text();
        }

        activate();

        /**
         * @name activate
         * @desc Call service to request all catalogs
         * @memberOf app.catalog.controllers.ListCatalogController
         */
        function activate(){
            Catalogs.all().then(getAllCatalogSuccess, getAllCatalogError);

            function getAllCatalogSuccess(data, status, headers, config){
                vm.obj_list = data.data;
            }

            function getAllCatalogError(data, status, headers, config){
                AlertNotification.success('Error request catalogs');
            }

            $scope.$on('obj.created', function (event, newObj) {
                vm.obj_list.unshift(newObj);
            });

            $scope.$on('obj.edited', function (event, newObjList, oldObjList) {
                vm.obj_list[vm.obj_list.indexOf(oldObjList)] = newObjList;
            });
        }


        /**
         * @name destroy
         * @desc remove one catalog
         * @Param {String} id to catalog want delete
         * @memberOf app.catalo.controllers.ListCatalogController
         */
        function destroy(obj){
            vm.requestPromise = Catalogs.destroy(obj.id).then(destroySuccess, destroyError);

            function destroySuccess(data, status, headers, config){
                AlertNotification.success("El catalogo se elimino con exito");
                activate();
            }

            function destroyError(data, status, headers, config){
                AlertNotification.error(data.data.message);
            }
        }

        vm.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            filter: vm.filters,
            sorting: {
                last_name: 'asc'     // initial sorting
            }
        }, {
            total: vm.obj_list.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                    $filter('filter')(vm.obj_list, params.filter()) :
                    vm.obj_list;
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.$watchCollection("vm.obj_list", function () {
            vm.tableParams.reload();
        });

    }
})();
