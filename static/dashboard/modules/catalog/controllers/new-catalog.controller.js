/**
 * NewCatalogController
 * @namespace app.catalog.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.catalog.controllers')
        .controller('NewCatalogController', NewCatalogController);

    NewCatalogController.$inject = ['AlertNotification', 'Catalogs', '$state', 'Products'];

    /**
     * @namespace NewCatalogController
     */
    function NewCatalogController(AlertNotification, Catalogs, $state, Products) {
        var vm = this;

        vm.submit = submit;

        vm.products = [];

        vm.products_select = [];
        vm.changeProductCheck = changeProductCheck;
        vm.changeFactor = changeFactor;

        vm.changeEditCustomPrice = changeEditCustomPrice;

        activate();

        vm.catalog = {};

        /**
         * @name submit
         * @desc Create a new Catalog
         * @memberOf app.catalog.controllers.NewCatalogController
         */
        function submit() {
            Catalogs.create(vm)
                .then(createCatalogSuccessFn, createCatalogErrorFn);

            /**
             * @name createCatalogSuccessFn
             */
            function createCatalogSuccessFn(data, status, headers, config) {
                AlertNotification.success('Success! Product created.');
                $state.go('catalogs');
            }


            /**
             * @name createCatalogErrorFn
             */
            function createCatalogErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('product.created.error');
                AlertNotification.error(data.error);
            }
        }

        /**
         * Event change select product to include catalog
         * @param {Object} product
         */
        function changeProductCheck(product){
           if(product.check){
               calculatePriceByFactor(product);
               vm.products_select.push(product);
           }else{
               vm.products_select.splice(vm.products_select.indexOf(product), 1);
           }
        }

        /**
         * Event change factor increment
         */
        function changeFactor(){
            angular.forEach(vm.products_select, function(product){
                calculatePriceByFactor(product);
            });
        }

        /**
         * Event change edit custom price
         * @param product
         */
        function changeEditCustomPrice(product){
            calculatePriceByFactor(product);
        }

        /**
         * Calculate custom price with general factor increment
         * @param product
         */
        function calculatePriceByFactor(product){
            if(!product.edit_custom_price){
                var factor = 0;
                if(vm.catalog.factor != undefined && vm.catalog.factor != ""){
                    factor = parseInt(vm.catalog.factor);
                }
                product.custom_price = (factor / 100 * parseFloat(product.sale_price)) + parseFloat(product.sale_price);
            }
        }


        function activate(){
            Products.all().then(getAllProductSuccess, getAllProductError);

            function getAllProductSuccess(data, status, headers, config){
                vm.products = data.data;
            }

            function getAllProductError(data, status, headers, config){
                AlertNotification.success('Error request categories');
            }
        }
    }
})();
