(function () {
    'use strict';

    angular
        .module('app.permission', [
            'app.permission.controllers',
            'app.permission.services',
            'app.permission.directives'
        ]);

    angular
        .module('app.permission.controllers', ['ngDialog']); //'mgcrea.ngStrap',

    angular
        .module('app.permission.services', []);

    angular
        .module('app.permission.directives', []);

})();