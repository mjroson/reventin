/**
 *
 * @namespace app.permission.directives
 */
(function () {
    'use strict';

    angular
        .module('app.permission.directives')
        .directive('hasPermission', hasPermission);

    hasPermission.$inject = ['Authentication'];

    function hasPermission(Authentication) {


        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var permissions_user = Authentication.getPermissions();

                //Example: has-permission="dispatch-list,dispatch-create"

                if(permissions_user){
                    var permissions = String(attrs.hasPermission).split(',');

                    for(var i=0; i < permissions.length; i++){
                        for(var p=0; p < permissions_user.length; p++){
                            if(String(permissions[i]).split('-')[0].toLowerCase() == permissions_user[p].entity.toLowerCase()){
                                for(var a=0; a < permissions_user[p].actions.length; a++){
                                    if(String(permissions[i]).split('-')[1].toLowerCase() ==  permissions_user[p].actions[a].name.toLowerCase()){
                                        if(!permissions_user[p].actions[a].value){
                                            element.hide();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        };

    }
})();