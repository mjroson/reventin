/**
 * Permissions
 * @namespace app.permission.services
 */
(function () {
    'use strict';

    angular
        .module('app.permission.services')
        .factory('Permissions', Permissions);

    Permissions.$inject = ['$http'];

    /**
     * @namespace Permissions
     * @returns {Factory}
     */
    function Permissions($http) {

        var Permissions = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            update: update,
            upload: upload,
            get_fields: get_fields,
            get_data_update: get_data_update
        };

        return Permissions;

        ////////////////////

        /**
         * @name all
         * @desc Get all Permissions
         * @returns {Promise}
         * @memberOf app.permission.services.Permissions
         */
        function all() {
            return $http.get('/api/permissions/');
        }


        /**
         * @name create
         * @desc Create a new Permission
         * @param {Objects} Objects to data info permission, and details permission data.
         * @returns {Promise}
         * @memberOf app.permission.services.Permissions
         */
        function create(data) {

            return $http.post('/api/permissions/', data );
        }

        /**
         * @name destroy
         * @Delete a Permission
         * @returns {Promise}
         * @memberOf app.permission.services.Permissions
         */
        function destroy(id) {
            return $http.delete('/api/permissions/' + id + "/");
        }


        /**
         * @name detail
         * @Detail a Permission
         * @returns {Promise}
         * @memberOf app.permission.services.Permissions
         */
        function detail(id) {
            return $http.get('/api/permissions/' + id + "/");
        }


        /**
         * @name update
         * @desc Update a Permission
         * @param {Object} object to data permissions and details permission
         * @returns {Promise}
         * @memberOf app.permission.services.Permissions
         */
        function update(data) {
            return $http.put('/api/permissions/' + data.id + '/', data);
        }


        /**
         * @name create
         * @desc Upload a csv to update product
         * @param {object} object with file csv
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function upload(obj) {
            var fd = new FormData();
            fd.append('file_csv', obj.file_csv);
            fd.append('config', angular.toJson(obj.config));
            return $http.post('/api/products/upload-csv-import/', fd,{
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            } );
        }

        function get_fields(){
            return $http.get('/api/products/fields/');
        }

        function get_data_update(id){
            return $http.get('/api/permissions/update/data/' + id);
        }

    }
})();
