/**
 * NewPermissionController
 * @namespace app.permission.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.permission.controllers')
        .controller('NewPermissionController', NewPermissionController);

    NewPermissionController.$inject = ['AlertNotification', 'Permissions', '$rootScope', '$scope'];

    /**
     * @namespace NewPermissionController
     */
    function NewPermissionController(AlertNotification, Permissions, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;

        vm.obj = {};

        vm.close = close;

        function close(){
            $scope.$parent.$parent.vm.objNew = !$scope.$parent.$parent.vm.objNew;
        }

        vm.cancel = function(){
            $scope.formCreatePermission.reset();
            vm.obj = {};
            close();
        }
        
        vm.obj.permissions = [
                {"entity":"PRODUCT", "actions": [{"name": "List", "value": false },
                                                 {"name": "Detail", "value": false },
                                                 {"name": "Delete", "value": false },
                                                 {"name": "Create", "value": false },
                                                 {"name": "Edit", "value": false }]},
                {"entity":"CATALOG", "actions": [{"name": "List", "value": false },
                                                 {"name": "Detail", "value": false },
                                                 {"name": "Delete", "value": false },
                                                 {"name": "Create", "value": false },
                                                 {"name": "Edit", "value": false }]},
                {"entity":"ORDER", "actions": [{"name": "List", "value": false },
                                                 {"name": "Detail", "value": false },
                                                 {"name": "Delete", "value": false },
                                                 {"name": "Create", "value": false },
                                                 {"name": "Edit", "value": false }]},
                {"entity":"DISPATCH", "actions": [{"name": "List", "value": false },
                                                 {"name": "Detail", "value": false },
                                                 {"name": "Delete", "value": false },
                                                 {"name": "Create", "value": false },
                                                 {"name": "Edit", "value": false }]},
                {"entity":"USER", "actions": [{"name": "List", "value": false },
                                                 {"name": "Detail", "value": false },
                                                 {"name": "Delete", "value": false },
                                                 {"name": "Create", "value": false },
                                                 {"name": "Edit", "value": false }]},
                {"entity":"PRICELIST", "actions": [{"name": "List", "value": false },
                                                 {"name": "Detail", "value": false },
                                                 {"name": "Delete", "value": false },
                                                 {"name": "Create", "value": false },
                                                 {"name": "Edit", "value": false }]}

        ];
        


        /**
         * @name submit
         * @desc Create a new Permission
         * @memberOf app.permission.controllers.NewPermissionController
         */
        function submit() {
            Permissions.create(vm.obj)
                .then(createPermissionSuccessFn, createPermissionErrorFn);

            /**
             * @name createPermissionSuccessFn
             */
            function createPermissionSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.created', data.data);
                $scope.formCreatePermission.reset();
                vm.obj = {};
                close();
                AlertNotification.success('Success! Permissions created.');
            }


            /**
             * @name createPermissionErrorFn
             */
            function createPermissionErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }
    }
})();
