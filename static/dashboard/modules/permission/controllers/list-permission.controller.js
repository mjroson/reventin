/**
 * ListPermissionController
 * @namespace app.permission.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.permission.controllers')
        .controller('ListPermissionController', ListPermissionController);

    ListPermissionController.$inject = ['AlertNotification', 'Permissions', 'ngTableParams', '$filter', '$scope'];

    /**
     * @namespace ListPermissionController
     */
    function ListPermissionController(AlertNotification, Permissions, ngTableParams, $filter, $scope) {
        var vm = this;

        vm.obj_list = [];

        vm.destroy = destroy;

        vm.search_field = 'name';

        vm.search_text = '';

        vm.filters = {
            name: "",
            description: ""
        };

        vm.edit_obj = function(obj){
            $scope.$broadcast('objData', obj);
            vm.objEdit = !vm.objEdit;
        }

        vm.change_search_text = change_search_text;

        function change_search_text(){
            for(var filter in vm.filters){
                vm.filters[filter] = "";
            }
            vm.filters[vm.search_field] = vm.search_text;
        }

        vm.clear_text = clear_text;

        function clear_text(){
            vm.search_text = "";
            change_search_text();
        }

        activate();

        /**
         * @name activate
         * @desc Call service to request all permissions
         * @memberOf app.permission.controllers.ListPermissionController
         */
        function activate(){
            vm.requestPromise = Permissions.all().then(getAllPermissionSuccess, getAllPermissionError);

            function getAllPermissionSuccess(data, status, headers, config){
                vm.obj_list = data.data;
            }

            function getAllPermissionError(data, status, headers, config){
                AlertNotification.success('Error request permissions');
            }

            $scope.$on('obj.created', function (event, newObj) {
                vm.obj_list.unshift(newObj);
            });

            $scope.$on('obj.edited', function (event, newObjList, oldObjList) {
                vm.obj_list[vm.obj_list.indexOf(oldObjList)] = newObjList;
            });
        }


        /**
         * @name destroy
         * @desc remove one permission
         * @Param {String} id to permission want delete
         * @memberOf app.catalo.controllers.ListPermissionController
         */
        function destroy(obj){
            Permissions.destroy(obj.id).then(destroySuccess, destroyError);

            function destroySuccess(data, status, headers, config){
                AlertNotification.success("El permissiono se elimino con exito");
                vm.obj_list.splice(vm.obj_list.indexOf(obj), 1);
            }

            function destroyError(data, status, headers, config){
                AlertNotification.error(data.data.message);
            }
        }


        vm.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            filter: vm.filters,
            sorting: {
                name: 'asc'     // initial sorting
            }
        }, {
            total: vm.obj_list.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                    $filter('filter')(vm.obj_list, params.filter()) :
                    vm.obj_list;
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.$watchCollection("vm.obj_list", function () {
            vm.tableParams.reload();
        });

    }
})();
