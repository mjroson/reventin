/**
 * UpdatePermissionController
 * @namespace app.permission.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.permission.controllers')
        .controller('UpdatePermissionController', UpdatePermissionController);

    UpdatePermissionController.$inject = ['AlertNotification', 'Permissions', '$rootScope', '$scope'];

    /**
     * @namespace UpdatePermissionController
     */
    function UpdatePermissionController(AlertNotification, Permissions, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;
        vm.obj = {};

        vm.old_obj = {};
        vm.close = close;

        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
            vm.objEdit = false;
        }

        activate();

        function submit() {
            Permissions.update(vm.obj)
                .then(updatePermissionSuccessFn, updatePermissionErrorFn);

            function updatePermissionSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.edited', data.data, vm.old_obj);
                vm.objEdit = false;
                AlertNotification.success('Success! Permission updated.');
            }

            function updatePermissionErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }

        function activate(){
            $scope.$on('objData', function(event, args){
                vm.old_obj = args;
                vm.obj = angular.copy(args);
            });
        }
    }
})();
