(function () {
    'use strict';

    angular
        .module('app.authentication', [
            'app.authentication.services',
            'app.authentication.controllers'
        ]);

    angular
        .module('app.authentication.services', ['ngCookies']);

    angular
        .module('app.authentication.controllers', []);


})();