/**
 * NavbarController
 * @namespace app.layout.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.layout.controllers')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['Authentication'];

    /**
     * @namespace NavbarController
     */
    function NavbarController(Authentication) {
        var vm = this;

        vm.user = Authentication.getUser();


        vm.logout = logout;

        function logout(){
            Authentication.unauthenticate();
            window.location = '/logout/';
        }
    }
})();
