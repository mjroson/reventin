/**
 * Orders
 * @namespace app.orders.services
 */
(function () {
    'use strict';

    angular
        .module('app.order.services')
        .factory('Orders', Orders);

    Orders.$inject = ['$http'];

    /**
     * @namespace Orders
     * @returns {Factory}
     */
    function Orders($http) {

        var Orders = {
            all: all,
            create: create,
            detail: detail,
            active_catalog_details: active_catalog_details,
            users_child: users_child
        };

        return Orders;

        ////////////////////

        /**
         * @name all
         * @desc Get all Catalogs
         * @returns {Promise}
         * @memberOf app.order.services.Catalogs
         */
        function all() {
            return $http.get('/api/orders/');
        }


        /**
         * @name create
         * @desc Create a new Catalog
         * @param {Objects} Objects to data info order, and details order data.
         * @returns {Promise}
         * @memberOf app.order.services.Catalogs
         */
        function create(data) {

            return $http.post('/api/orders/', data );
        }

        /**
         * @name destroy
         * @Delete a Catalog
         * @returns {Promise}
         * @memberOf app.orders.services.Order

        function destroy(id) {
            return $http.delete('/api/orders/' + id + "/");
        }*/


        /**
         * @name detail
         * @Detail a Price List
         * @returns {Promise}
         * @memberOf app.orderss.services.Order
         */
        function detail(id) {
            return $http.get('/api/orders/' + id + "/");
        }


        /**
         * @name update
         * @desc Update a Price Lists
         * @param {Object} object to data price lists
         * @returns {Promise}
         * @memberOf app.orders.services.Order

        function update(data) {
            return $http.put('/api/orders/' + data.id + '/', data);
        }*/

        function active_catalog_details(catalog_id){
            return $http.get('/api/catalogs/details/active/');
        }


        function users_child(){
            return $http.get('/api/users/childrens/');
        }
    }
})();
