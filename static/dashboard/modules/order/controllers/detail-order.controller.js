/**
 * DetailOrderController
 * @namespace app.order.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.order.controllers')
        .controller('DetailOrderController', DetailOrderController);

    DetailOrderController.$inject = ['AlertNotification', 'Orders', '$stateParams'];

    /**
     * @namespace DetailOrderController
     */
    function DetailOrderController(AlertNotification, Orders, $stateParams) {
        var vm = this;

        vm.order = {};
        init();

        /**
         * @name init
         * @desc Call service to request detail price list
         * @memberOf app.order.controllers.DetailOrderController
         */
        function init(){
            Orders.detail($stateParams.id).then(getDetailOrderSuccess, getDetailOrderError);

            function getDetailOrderSuccess(data, status, headers, config){
                vm.order = data.data;
            }

            function getDetailOrderError(data, status, headers, config){
                AlertNotification.success('Error request price list detail');
            }
        }

    }
})();
