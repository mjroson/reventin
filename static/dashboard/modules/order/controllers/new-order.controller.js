/**
 * NewOrderController
 * @namespace app.order.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.order.controllers')
        .controller('NewOrderController', NewOrderController);

    NewOrderController.$inject = ['AlertNotification', 'Orders', '$state', 'Products'];

    /**
     * @namespace NewOrderController
     */
    function NewOrderController(AlertNotification, Orders, $state, Products) {
        var vm = this;

        vm.submit = submit;

        vm.order = {};
        vm.order.details = [];
        vm.catalog_details = [];

        vm.selectDetail = selectDetail;

        vm.profit = 0.00;
        vm.cost = 0.00;
        vm.total_price = 0.00;

        vm.users = [];

        vm.changeQuantity = changeQuantity;
        vm.changeSelectUser = changeSelectUser;

        get_catalog_details();

        get_user_child();
        /**
         * @name submit
         * @desc Create a new Catalog
         * @memberOf app.order.controllers.NewOrderController
         */
        function submit() {
            Orders.create(vm.order)
                .then(createOrderSuccessFn, createOrderErrorFn);

            function createOrderSuccessFn(data, status, headers, config) {
                AlertNotification.success('Success! Order created.');
                $state.go('orders');
            }

            function createOrderErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }
        function get_catalog_details(){
            Orders.active_catalog_details().then(getCatalogDetailSuccess, getCatalogDetailError)
            
            function getCatalogDetailSuccess(data, status, headers, config){
                vm.catalog_details = data.data; 
            }
            
            function getCatalogDetailError(data, status, headers, config){
                AlertNotification.success("Error al traer la lista de detalle de catalogo");
            }
        }

        function calcAllPrice(){
            vm.cost = vm.total_price - (vm.total_price * 0.2);
            vm.profit = vm.total_price - vm.cost;
        }

        function selectDetail(detail){
            if(detail.check){
                vm.order.details.push(detail);
                if(detail.quantity == undefined || detail.quantity == "" || detail.quantity == 0 || detail.quantity != null){
                    detail.quantity = 1;
                }
                vm.total_price += (detail.custom_price * detail.quantity);
            }else{
                vm.order.details.splice(vm.order.details.indexOf(detail), 1);
                vm.total_price -= (detail.custom_price * detail.quantity);
            }
            calcAllPrice();
        }

        function changeQuantity(detail){
            if(detail.quantity != null || detail.quantity != undefined){
                if(detail.quantity_old == undefined || detail.quantity_old == 0){
                    vm.total_price = (vm.total_price - detail.custom_price) + (detail.quantity * detail.custom_price);
                }else{
                    vm.total_price = (vm.total_price - (detail.custom_price * detail.quantity_old)) + (detail.quantity * detail.custom_price);
                }
                detail.quantity_old = detail.quantity;
                calcAllPrice()
            }

        }


        function get_user_child(){
            Orders.users_child().then(getUserSuccess, getUserError)

            function getUserSuccess(data, status, headers, config){
                vm.users = data.data;
            }

            function getUserError(data, status, headers, config){
                AlertNotification.success("Error al traer la lista de usuarios");
            }
        }


        function changeSelectUser(){
            if(vm.custom_user == false){
                vm.order.user = '';
            }
        }
    }
})();
