/**
 * ListOrderController
 * @namespace app.order.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.order.controllers')
        .controller('ListOrderController', ListOrderController);

    ListOrderController.$inject = ['AlertNotification', 'Orders', '$scope', '$filter', 'ngTableParams'];

    /**
     * @namespace ListOrderController
     */
    function ListOrderController(AlertNotification, Orders, $scope, $filter, ngTableParams) {
        var vm = this;

        vm.clear_text = clear_text;

        vm.obj_list = [];

        vm.search_field = 'date';

        vm.search_text = '';

        vm.filters = {
            description: "",
            date: ""
        };

        vm.edit_obj = function(obj){
            $scope.$broadcast('objData', obj);
            vm.objEdit = !vm.objEdit;
        }

        vm.change_search_text = change_search_text;

        function change_search_text(){
            // Clear All filter
            for(var filter in vm.filters){
                vm.filters[filter] = "";
            }

            vm.filters[vm.search_field] = vm.search_text;
        }

        function clear_text(){
            vm.search_text = "";
            change_search_text();
        }

        activate();

        /**
         * @name activate
         * @desc Call service to request all orders
         * @memberOf app.order.controllers.ListOrderController
         */
        function activate(){
            vm.requestPromise = Orders.all().then(getAllOrderSuccess, getAllOrderError);

            function getAllOrderSuccess(data, status, headers, config){
                vm.obj_list = data.data;
            }

            function getAllOrderError(data, status, headers, config){
                AlertNotification.success('Error request price lists');
            }

            $scope.$on('obj.created', function (event, newObj) {
                vm.obj_list.unshift(newObj);
            });

            $scope.$on('obj.edited', function (event, newObjList, oldObjList) {
                vm.obj_list[vm.obj_list.indexOf(oldObjList)] = newObjList;
            });
        }


        vm.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            filter: vm.filters,
            sorting: {
                last_name: 'asc'     // initial sorting
            }
        }, {
            total: vm.obj_list.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                    $filter('filter')(vm.obj_list, params.filter()) :
                    vm.obj_list;
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.$watchCollection("vm.obj_list", function () {
            vm.tableParams.reload();
        });

    }
})();
