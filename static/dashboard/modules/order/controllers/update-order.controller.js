/**
 * UpdateOrderController
 * @namespace app.order.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.order.controllers')
        .controller('UpdateOrderController', UpdateOrderController);

    UpdateOrderController.$inject = ['AlertNotification', 'Orders', '$state', '$stateParams'];

    /**
     * @namespace UpdateOrderController
     */
    function UpdateOrderController(AlertNotification, Orders, $state, $stateParams) {
        var vm = this;

        vm.submit = submit;
        vm.price_list = {};



        activate();

        /**
         * @name submit
         * @desc Update a Price List
         * @memberOf app.order.controllers.UpdateOrderController
         */
        function submit() {
            Orders.update(vm.price_list)
                .then(updateOrderSuccessFn, updateOrderErrorFn);

            /**
             * @name updateOrderSuccessFn
             */
            function updateOrderSuccessFn(data, status, headers, config) {
                AlertNotification.success('Success! Price List created.');
                $state.go('orders');
            }


            /**
             * @name updateOrderErrorFn
             */
            function updateOrderErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }


        function activate(){
            Orders.detail($stateParams.id).then(getOrderSuccess, getOrderError);

            function getOrderSuccess(data, status, headers, config){
                vm.price_list = data.data;
            }

            function getOrderError(data, status, headers, config){
                AlertNotification.success('Error request price lists');
            }
        }
    }
})();
