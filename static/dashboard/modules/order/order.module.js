(function () {
    'use strict';

    angular
        .module('app.order', [
            'app.order.controllers',
            'app.order.services'
        ]);

    angular
        .module('app.order.controllers', ['ngDialog']); //'mgcrea.ngStrap',

    angular
        .module('app.order.services', []);

})();