(function () {
    'use strict';

    angular
        .module('app.user-type', [
            'app.user-type.controllers',
            'app.user-type.services'
        ]);

    angular
        .module('app.user-type.controllers', ['ngDialog']); //'mgcrea.ngStrap',

    angular
        .module('app.user-type.services', []);

})();