/**
 * NewUserTypeController
 * @namespace app.user-type.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user-type.controllers')
        .controller('NewUserTypeController', NewUserTypeController);

    NewUserTypeController.$inject = ['AlertNotification', 'UserTypes', '$rootScope', 'PriceLists', 'Permissions', '$scope'];

    /**
     * @namespace NewUserTypeController
     */
    function NewUserTypeController(AlertNotification, UserTypes, $rootScope, PriceLists, Permissions, $scope) {
        var vm = this;

        vm.submit = submit;
        vm.close = close;
        vm.cancel = cancel;


        vm.obj = {};

        function close(){
            $scope.$parent.$parent.vm.objNew = !$scope.$parent.$parent.vm.objNew;
        }

        function cancel(){
            $scope.formCreateUserType.reset();
            vm.obj = {};
            close();
        }


        activate();
        /**
         * @name submit
         * @desc Create a new UserType
         * @memberOf app.user-type.controllers.NewUserTypeController
         */
        function submit() {

            UserTypes.create(vm.obj)
                .then(createUserTypeSuccessFn, createUserTypeErrorFn);

            /**
             * @name createUserTypeSuccessFn
             */
            function createUserTypeSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.created', data.data);
                $scope.formCreateUserType.reset();
                vm.obj = {};
                close();
                AlertNotification.success('Success! User Type created.');
            }

            /**
             * @name createUserTypeErrorFn
             */
            function createUserTypeErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('user-type.created.error');
                AlertNotification.error(data.error);
            }
        }

        function activate(){
            PriceLists.all().then(getUserTypesSuccess);

            function getUserTypesSuccess(data,status,headers,config){
                vm.price_lists = data.data;
            }
            Permissions.all().then(getPermissionsSuccess);

            function getPermissionsSuccess(data,status,headers,config){
                vm.permissions = data.data;
            }
        }

    }
})();
