/**
 * ListUserTypeController
 * @namespace app.user-type.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user-type.controllers')
        .controller('ListUserTypeController', ListUserTypeController);

    ListUserTypeController.$inject = ['AlertNotification', 'UserTypes', '$scope', '$filter', 'ngTableParams'];

    /**
     * @namespace ListUserTypeController
     */
    function ListUserTypeController(AlertNotification, UserTypes, $scope, $filter, ngTableParams) {
        var vm = this;

        vm.destroy = destroy;
        vm.clear_text = clear_text;

        vm.obj_list = [];

        vm.search_field = 'name';

        vm.search_text = '';

        vm.filters = {
            name: "",
            comision: ""
        };

        vm.edit_obj = function(obj){
            $scope.$broadcast('objData', obj);
            vm.objEdit = !vm.objEdit;
        }

        vm.change_search_text = change_search_text;

        function change_search_text(){
            // Clear All filter
            for(var filter in vm.filters){
                vm.filters[filter] = "";
            }

            vm.filters[vm.search_field] = vm.search_text;
        }

        function clear_text(){
            vm.search_text = "";
            change_search_text();
        }

        activate();

        /**
         * @name activate
         * @desc Call service to request all user-types
         * @memberOf app.user-type.controllers.ListUserTypeController
         */
        function activate(){
            vm.requestPromise = UserTypes.all().then(getAllUserTypeSuccess, getAllUserTypeError);

            function getAllUserTypeSuccess(data, status, headers, config){
                vm.obj_list = data.data;
            }

            function getAllUserTypeError(data, status, headers, config){
                AlertNotification.success('Error request price lists');
            }

            $scope.$on('obj.created', function (event, newObj) {
                vm.obj_list.unshift(newObj);
            });

            $scope.$on('obj.edited', function (event, newObjList, oldObjList) {
                vm.obj_list[vm.obj_list.indexOf(oldObjList)] = newObjList;
            });

        }


        /**
         * @name destroy
         * @desc remove one user-type
         * @Param {String} id to user-type want delete
         * @memberOf app.catalo.controllers.ListUserTypeController
         */
        function destroy(obj){
            UserTypes.destroy(obj.id).then(destroySuccess, destroyError);

            function destroySuccess(data, status, headers, config){
                AlertNotification.success("El tipo de usuario fue eliminado con exito");
                vm.obj_list.splice(vm.obj_list.indexOf(obj), 1);
            }

            function destroyError(data, status, headers, config){
                AlertNotification.error("Error al intentar eliminar el tipo de usuario");
            }
        }

        vm.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            filter: vm.filters,
            sorting: {
                last_name: 'asc'     // initial sorting
            }
        }, {
            total: vm.obj_list.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                    $filter('filter')(vm.obj_list, params.filter()) :
                    vm.obj_list;
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.$watchCollection("vm.obj_list", function () {
            vm.tableParams.reload();
        });

    }
})();
