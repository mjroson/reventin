/**
 * UpdateUserTypeController
 * @namespace app.user-type.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user-type.controllers')
        .controller('UpdateUserTypeController', UpdateUserTypeController);

    UpdateUserTypeController.$inject = ['AlertNotification', 'UserTypes', '$rootScope', '$scope'];

    /**
     * @namespace UpdateUserTypeController
     */
    function UpdateUserTypeController(AlertNotification, UserTypes, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;
        vm.obj = {};


        vm.old_obj = {};
        vm.close = close;

        activate();

        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
            vm.objEdit = false;
        }

        /**
         * @name submit
         * @desc Update a Price List
         * @memberOf app.user-type.controllers.UpdateUserTypeController
         */
        function submit() {
            //vm.user_type.permission_group = vm.permission_group;
            //vm.user_type.price_list = vm.price_list;

            UserTypes.update(vm.obj)
                .then(updateUserTypeSuccessFn, updateUserTypeErrorFn);

            /**
             * @name updateUserTypeSuccessFn
             */
            function updateUserTypeSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.edited', data.data, vm.old_obj);
                vm.objEdit = false;
                AlertNotification.success('Success! User Type updated.');
            }


            /**
             * @name updateUserTypeErrorFn
             */
            function updateUserTypeErrorFn(data, status, headers, config) {
                AlertNotification.error("Error! No se pudo modificar el tipo de usuario");
            }
        }

        function activate(){
            $scope.$on('objData', function(event, args){
                vm.old_obj = args;
                vm.obj = angular.copy(args);
            });
        }
    }
})();
