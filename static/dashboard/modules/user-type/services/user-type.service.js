/**
 * UserTypes
 * @namespace app.user-type.services
 */
(function () {
    'use strict';

    angular
        .module('app.user-type.services')
        .factory('UserTypes', UserTypes);

    UserTypes.$inject = ['$http'];

    /**
     * @namespace UserTypes
     * @returns {Factory}
     */
    function UserTypes($http) {

        var UserTypes = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            update: update
        };

        return UserTypes;

        ////////////////////

        /**
         * @name all
         * @desc Get all User Types
         * @returns {Promise}
         * @memberOf app.user-type.services.UserTypes
         */
        function all() {
            return $http.get('/api/users/types/');
        }


        /**
         * @name create
         * @desc Create a new User Types
         * @param {Objects} Objects to data info user-type, and details user-type data.
         * @returns {Promise}
         * @memberOf app.user-type.services.Catalogs
         */
        function create(data) {

            return $http.post('/api/users/types/', data );
        }

        /**
         * @name destroy
         * @Delete a User Types
         * @returns {Promise}
         * @memberOf app.user-type.services.UserTypes
         */
        function destroy(id) {
            return $http.delete('/api/users/types/' + id + "/");
        }


        /**
         * @name detail
         * @Detail a User Types
         * @returns {Promise}
         * @memberOf app.user-types.services.UserTypes
         */
        function detail(id) {
            return $http.get('/api/users/types/' + id + "/");
        }


        /**
         * @name update
         * @desc Update a User Types
         * @param {Object} object to data User Types
         * @returns {Promise}
         * @memberOf app.user-type.services.UserTypes
         */
        function update(data) {
            return $http.put('/api/users/types/' + data.id + '/', data);
        }

    }
})();
