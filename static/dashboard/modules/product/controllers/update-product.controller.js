/**
 * UpdateProductController
 * @namespace app.products.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.product.controllers')
        .controller('UpdateProductController', UpdateProductController);

    UpdateProductController.$inject = ['$scope', 'AlertNotification', 'Products', '$rootScope'];

    /**
     * @namespace UpdateProductController
     */
    function UpdateProductController($scope, AlertNotification, Products, $rootScope ) {
        var vm = this;

        vm.submit = submit;

        vm.change_select_attribute = change_select_attribute;

        vm.add_attribute = add_attribute;

        vm.delete_attribute = delete_attribute;

        vm.generate_product_variations = generateProductVariation;

        vm.delete_variation = delete_variation;

        vm.add_variation = add_variation;

        vm.close = close;
        vm.cancel = cancel;

        vm.tab = 1;

        vm.flag_img_show = 0;

        vm.obj = {};
        vm.old_obj = {};
        vm.attributes = [];

        function cancel(){
            $scope.formObject.reset();
            initData();
            vm.properties = angular.copy(vm.propertiesData);
            close();
        }

        //function initData(){
            //vm.obj.product_variations = [];
            //vm.attributes = [];
            //vm.old_obj = {};
        //    vm.tab = 1;
        //}


        activate();

        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
            vm.objEdit = false;
        }

        function activate(){
            //initData();
            $scope.$on('objData', function(event, args){
                vm.old_obj = args;
                vm.obj = angular.copy(args);
                vm.properties = vm.obj.properties;
                vm.images = angular.copy(vm.obj.images);
                for(var i=0; i < vm.properties.length; i++){
                    if(vm.properties[i]['selected']){
                        var attr = {};
                        attr.name = vm.properties[i].attr_name;
                        attr.selected = vm.properties[i].id;
                        attr.prop_selected = vm.properties[i];
                        vm.attributes.push(attr);
                    }
                }
                vm.minimun_stock = 1;
            });
        }

        /**
         * @name submit
         * @desc Create a new Product
         * @memberOf app.product.controllers.UpdateProductController
         */
        function submit() {



            var images = [];


            angular.forEach(vm.properties, function(prop){
                if(prop.selected == true){
                    angular.forEach(prop.values, function(prop_value, index){
                        var is_value_exist = false;
                        angular.forEach(vm.obj.product_variations, function(variation){
                            angular.forEach(variation.properties, function(attr_prop){
                               if(prop_value == attr_prop.value){
                                   is_value_exist = true;
                               }
                            });
                        });
                        if(is_value_exist == false){
                            console.log("ELIMINAR EL ATRIBUTO");
                            console.log(prop_value)
                            prop.values.splice(index, 1);
                        }
                    });
                    if(prop.values.length < 1){
                        prop.selected = false;
                    }
                }

            });

            vm.obj.properties = angular.copy(vm.properties);

            vm.obj.images = vm.images;

            vm.requestPromise = Products.update(vm.obj, vm.images)
                .then(updateObjectSuccessFn, updateObjectErrorFn);


            /**
             * @name updateUserTypeSuccessFn
             */
            function updateObjectSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.edited', data.data, vm.old_obj);
                vm.objEdit = false;
                AlertNotification.success('Success! Product updated.');
            }


            /**
             * @name updateUserTypeErrorFn
             */
            function updateObjectErrorFn(data, status, headers, config) {
                AlertNotification.error("Error! No se pudo modificar el producto");
            }
        }


        /**
         * Agrega un nuevo elemento al array de atributos para poder seleccionar otro atributo
         * validando que el largo del array de atributos no puede ser mayor al largo del array de propiedades
         */
        function add_attribute(){
            if(vm.attributes.length < vm.properties.length){
                var attr = {};
                attr.name = "";
                attr.selected = false;
                vm.attributes.push(attr);
            }else{
                AlertNotification.error("No hay mas atributos");
            }

        }

        function delete_attribute(attr){
            if(attr.prop_selected){
                vm.properties[vm.properties.indexOf(attr.prop_selected)].selected = false;
            }
            vm.attributes.splice(vm.attributes.indexOf(attr), 1);
        }

        /**
         * Cuando cambia la seleccion de un attributo, setea los datos necesarios para saber las propiedades seleccionadas
         */
        function change_select_attribute(attr){
            for(var i=0; i < vm.properties.length; i++){
                if(vm.properties[i].id == attr.selected){
                    // Si el attributo ya tiene una propiedad, antes de reemplazarla 
                    // pone esa propiedad con selected false
                    if(attr.prop_selected){
                        vm.properties[vm.properties.indexOf(attr.prop_selected)].selected = false;
                    }
                    attr.prop_selected = vm.properties[i];
                    vm.properties[i].selected = true;
                    break;
                }
            }
        }


        /**
         * A partir de una matriz y el numero de elementos combinados (el length de la matriz)
         * Te devuelve todos las combinaciones posibles
         */
        function getCombinations(arr, n){
            if(n == 1){
                var ret = [];
                for(var i = 0; i < arr.length; i++){
                    for(var j = 0; j < arr[i].length; j++){
                        ret.push([arr[i][j]]);
                    }
                }
                return ret;
            }
            else{
                var ret = [];
                for(var i = 0; i < arr.length; i++){
                    var elem = arr.shift();
                    for(var j = 0; j < elem.length; j++){
                        var childperm = getCombinations(arr.slice(), n-1);
                        for(var k = 0; k < childperm.length; k++){
                            ret.push([elem[j]].concat(childperm[k]));
                        }
                    }
                }
                return ret;
            }
        }

        /**
         * Genera un array con los diferentes valores de una propiedad
         */
        function getPropertyVariation(property){
            var variation_prop = [];

            for(var i=0; i < property.values.length; i ++){
                var prop = {};
                prop.name = angular.copy(property.attr_name);
                prop.id = angular.copy(property.id);
                prop.value =angular.copy(property.values[i]);
                variation_prop.push(prop);
            }
            return variation_prop;
        }


        /**
         * Genero todas las variaciones de producto con todas las propiedades seleccionadas.
         */
        function generateProductVariation(){
            vm.tab = 4;

            var properties_variations = [];

            // Genero una matriz, donde cada array esta compuesto por los diferentes valores de una propiedad (en estado seleccionado)
            for(var i=0; i < vm.properties.length; i++){
                if(vm.properties[i].selected){
                    if(!Array.isArray(vm.properties[i].values)){
                        var value = vm.properties[i].values;
                        vm.properties[i].values = [];
                        vm.properties[i].values.push(value);
                    }
                    // Genero las posibilidades que tengo por cada propiedad
                    var pos_by_property = getPropertyVariation(vm.properties[i]);
                    properties_variations.push(pos_by_property);
                }
            }
            // Genero todas las posibilidades con todas las propiedades
            var product_variations_properties = getCombinations(properties_variations, properties_variations.length);

            vm.obj.product_variations = [];
            angular.forEach(product_variations_properties, function(variation_properties){
                // Creo una variacion con los datos, de las propiedades posible de cada una y el minimo stock
                var variation = {};
                variation.minimun_stock = vm.minimun_stock;
                variation.properties = variation_properties;
                vm.obj.product_variations.push(variation);
            });
        }

        function delete_variation(variation){
            vm.obj.product_variations.splice(vm.obj.product_variations.indexOf(variation), 1);
        }

        function add_variation(){
            var product_variation = {};
            product_variation.minimun_stock = vm.minimun_stock;
            var properties = angular.copy(vm.obj.product_variations[0].properties);
            angular.forEach(properties, function(property){
                property.value = "";
            });
            product_variation.properties = properties;

            vm.obj.product_variations.push(product_variation);
        }

    }
})();
