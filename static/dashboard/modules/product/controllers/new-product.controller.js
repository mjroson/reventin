/**
 * NewProductController
 * @namespace app.products.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.product.controllers')
        .controller('NewProductController', NewProductController);

    NewProductController.$inject = ['$scope', 'AlertNotification', 'Products', '$rootScope'];

    /**
     * @namespace NewProductController
     */
    function NewProductController($scope, AlertNotification, Products, $rootScope ) {
        var vm = this;

        vm.submit = submit;

        vm.change_select_attribute = change_select_attribute;

        vm.add_attribute = add_attribute;

        vm.delete_attribute = delete_attribute;

        vm.generate_product_variations = generateProductVariation;

        vm.delete_variation = delete_variation;

        vm.add_variation = add_variation;

        vm.close = close;
        vm.cancel = cancel;

        vm.tab = 1;

        function close(){
            $scope.$parent.$parent.vm.objNew = !$scope.$parent.$parent.vm.objNew;
        }

        function cancel(){
            $scope.formCreateProduct.reset();
            initData();
            vm.properties = angular.copy(vm.propertiesData);
            close();
        }

        function initData(){
            vm.obj = {};
            vm.images = [];
            vm.attributes = [];
            vm.obj.product_variations = [];
            vm.tab = 1;
        }


        activate();


        /*
        vm.add_image = add_image;

        function add_image(){
            var image = {}
            image.id = Math.floor((Math.random() * 100) + 1);
            image.name = "image" + String(image.id);

            vm.images.push(image);
        }*/


        /**
         * Funcion que se ejecuta cuando se instancia el controlador
         */
        function activate(){
            initData();

            // Trae todas las propiedades de productos
            Products.get_custom_attr().then(getCustomAttrSuccess, getCustomAttrError);

            function getCustomAttrSuccess(data, status, headers, config){
                vm.propertiesData = data.data;
                vm.properties = angular.copy(vm.propertiesData);
                add_attribute();
                vm.minimun_stock = 1;
            }

            function getCustomAttrError(data, status, headers, config){
                AlertNotification.error(data.message);
            }
        }

        /**
         * @name submit
         * @desc Create a new Product
         * @memberOf app.product.controllers.NewProductController
         */
        function submit() {
            var images = [];

            vm.obj.properties = vm.properties;


            // TODO: remove image file on json
            vm.obj.images = vm.images;
            vm.requestPromise = Products.create(vm.obj, vm.images)
                .then(createProductSuccessFn, createProductErrorFn);

            /**
             * @name createProductSuccessFn
             * @desc Show snackbar with success message
             */
            function createProductSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.created', data.data);
                $scope.formCreateProduct.reset();
                initData();
                vm.properties = angular.copy(vm.propertiesData);
                close();
                AlertNotification.success('Success! Product created');
            }

            /**
             * @name createProductErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function createProductErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }


        /**
         * Agrega un nuevo elemento al array de atributos para poder seleccionar otro atributo
         * validando que el largo del array de atributos no puede ser mayor al largo del array de propiedades
         */
        function add_attribute(){
            if(vm.attributes.length < vm.properties.length){
                var attr = {};
                attr.name = "";
                attr.selected = false;
                vm.attributes.push(attr);
            }else{
                AlertNotification.error("No hay mas atributos");
            }

        }

        function delete_attribute(attr){
            if(attr.prop_selected){
                vm.properties[vm.properties.indexOf(attr.prop_selected)].selected = false;
            }
            vm.attributes.splice(vm.attributes.indexOf(attr), 1);
        }

        /**
         * Cuando cambia la seleccion de un attributo, setea los datos necesarios para saber las propiedades seleccionadas
         */
        function change_select_attribute(attr){
            for(var i=0; i < vm.properties.length; i++){
                if(vm.properties[i].id == attr.selected){
                    // Si el attributo ya tiene una propiedad, antes de reemplazarla 
                    // pone esa propiedad con selected false
                    if(attr.prop_selected){
                        vm.properties[vm.properties.indexOf(attr.prop_selected)].selected = false;
                    }
                    attr.prop_selected = vm.properties[i];
                    vm.properties[i].selected = true;
                    break;
                }
            }
        }


        /**
         * A partir de una matriz y el numero de elementos combinados (el length de la matriz)
         * Te devuelve todos las combinaciones posibles
         */
        function getCombinations(arr, n){
            if(n == 1){
                var ret = [];
                for(var i = 0; i < arr.length; i++){
                    for(var j = 0; j < arr[i].length; j++){
                        ret.push([arr[i][j]]);
                    }
                }
                return ret;
            }
            else{
                var ret = [];
                for(var i = 0; i < arr.length; i++){
                    var elem = arr.shift();
                    for(var j = 0; j < elem.length; j++){
                        var childperm = getCombinations(arr.slice(), n-1);
                        for(var k = 0; k < childperm.length; k++){
                            ret.push([elem[j]].concat(childperm[k]));
                        }
                    }
                }
                return ret;
            }
        }

        /**
         * Genera un array con los diferentes valores de una propiedad
         */
        function getPropertyVariation(property){
            var variation_prop = [];

            for(var i=0; i < property.values.length; i ++){
                var prop = {};
                prop.name = angular.copy(property.attr_name);
                prop.id = angular.copy(property.id);
                prop.value =angular.copy(property.values[i]);
                variation_prop.push(prop);
            }
            return variation_prop;
        }

        // TODO: Optimizar scripts de generar variaciones de producto (getCombinations(), getPropertyVariation(), getProductVariation())
        /**
         * Genero todas las variaciones de producto con todas las propiedades seleccionadas.
         */
        function generateProductVariation(){
            vm.tab = 4;

            var properties_variations = [];

            // Genero una matriz, donde cada array esta compuesto por los diferentes valores de una propiedad (en estado seleccionado)
            for(var i=0; i < vm.properties.length; i++){
                if(vm.properties[i].selected){
                    if(!Array.isArray(vm.properties[i].values)){
                        var value = vm.properties[i].values;
                        vm.properties[i].values = [];
                        vm.properties[i].values.push(value);
                    }
                    // Genero las posibilidades que tengo por cada propiedad
                    var pos_by_property = getPropertyVariation(vm.properties[i]);
                    properties_variations.push(pos_by_property);
                }
            }
            // Genero todas las posibilidades con todas las propiedades
            var product_variations_properties = getCombinations(properties_variations, properties_variations.length);

            vm.obj.product_variations = [];
            angular.forEach(product_variations_properties, function(variation_properties){
                // Creo una variacion con los datos, de las propiedades posible de cada una y el minimo stock
                var variation = {};
                variation.minimun_stock = vm.minimun_stock;
                variation.properties = variation_properties;
                vm.obj.product_variations.push(variation);
            });
        }

        function delete_variation(variation){
            vm.obj.product_variations.splice(vm.obj.product_variations.indexOf(variation), 1);
        }

        function add_variation(){
            var product_variation = {};
            product_variation.minimun_stock = vm.minimun_stock;
            var properties = angular.copy(vm.obj.product_variations[0].properties);
            angular.forEach(properties, function(property){
                property.value = "";
            });
            product_variation.properties = properties;

            vm.obj.product_variations.push(product_variation);
        }

    }
})();
