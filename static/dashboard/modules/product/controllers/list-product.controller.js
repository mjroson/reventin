/**
 * ListProductController
 * @namespace app.product.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.product.controllers')
        .controller('ListProductController', ListProductController);

    ListProductController.$inject = ['AlertNotification', 'Products', '$filter', 'ngTableParams', '$scope'];

    /**
     * @namespace ListProductController
     */
    function ListProductController(AlertNotification, Products, $filter, ngTableParams, $scope) {
        var vm = this;

        vm.destroy = destroy;
        vm.clear_text = clear_text;
        vm.change_search_text = change_search_text;

        vm.obj_list = [];

        vm.search_field = 'name';

        vm.search_text = '';

        vm.filters = {
            name: "",
            sku: "",
            cost_price: "",
            sale_price: ""
        };

        vm.edit_obj = function(obj){
            $scope.$broadcast('objData', obj);
            vm.objEdit = !vm.objEdit;
        }

        function change_search_text(){
            // Clear All filter
            for(var filter in vm.filters){
                vm.filters[filter] = "";
            }

            vm.filters[vm.search_field] = vm.search_text;
        }

        function clear_text(){
            vm.search_text = "";
            change_search_text();
        }

        activate();

        /**
         * @name activate
         * @desc Call service to request all categories
         * @memberOf app.product.controllers.ListProductController
         */
        function activate(){
            vm.requestPromise = Products.all().then(getObjectsSuccess, getObjectsError);

            function getObjectsSuccess(data, status, headers, config){
                vm.obj_list = data.data;
            }

            function getObjectsError(data, status, headers, config){
                AlertNotification.success('Error request categories');
            }

            $scope.$on('obj.created', function (event, newObj) {
                vm.obj_list.unshift(newObj);
            });

            $scope.$on('obj.edited', function (event, newObjList, oldObjList) {
                vm.obj_list[vm.obj_list.indexOf(oldObjList)] = newObjList;
            });
        }


        /**
         * @name destroy
         * @desc remove one category
         * @Param {String} id to category want delete
         * @memberOf app.product.controllers.ListProductController
         */
        function destroy(obj){
            Products.destroy(obj.id).then(destroySuccess, destroyError);

            function destroySuccess(data, status, headers, config){
                AlertNotification.success("El producto se elimino con exito");
                vm.obj_list.splice(vm.obj_list.indexOf(obj), 1);
            }

            function destroyError(data, status, headers, config){
                AlertNotification.error("Error al eliminar el producto");
            }
        }

        vm.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            filter: vm.filters,
            sorting: {
                last_name: 'asc'     // initial sorting
            }
        }, {
            total: vm.obj_list.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                    $filter('filter')(vm.obj_list, params.filter()) :
                    vm.obj_list;
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.$watchCollection("vm.obj_list", function () {
            vm.tableParams.reload();
        });

    }
})();
