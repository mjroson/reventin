/**
 * ImportCsvProductController
 * @namespace app.products.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.product.controllers')
        .controller('ImportCsvProductController', ImportCsvProductController);

    ImportCsvProductController.$inject = ['$scope', 'AlertNotification', 'Products'];

    /**
     * @namespace ImportCsvProductController
     */
    function ImportCsvProductController($scope, AlertNotification, Products) {
        var vm = this;

        vm.submit = submit;
        vm.selected_attr = selected_attr;
        vm.completeAttributeRequired = completeAttributeRequired;

        vm.cancel = cancel;
        vm.close = close;

        function cancel(){
            //$scope.formImportCSV.reset();
            vm.file_csv = {};
            vm.config = [];
            vm.list_attr_product = angular.copy(vm.list_attr_data);
            close();
        }

        function close(){
            $scope.$parent.$parent.vm.importCSV = !$scope.$parent.$parent.vm.importCSV;
        }


        activate();

        /**
         * @name submit
         * @desc Upload csv files to import product
         * @memberOf app.product.controllers.ImportCsvProductController
         */
        function submit() {
            vm.list_attr_required = [];
            for(var i=0; i < vm.list_attr_product.length; i++){
                if(vm.list_attr_product[i].required == "True"){
                    var flagState = false;
                    for(var ic=0; ic < vm.config.length; ic++){
                        if(vm.config[ic].attr_product){
                            var conf_attr_prod = angular.fromJson(vm.config[ic].attr_product);

                            if(conf_attr_prod.value == vm.list_attr_product[i].value){
                                flagState = true;
                                break;
                            }
                        }
                    }
                    if(flagState == false){
                        var attr_req = {};
                        attr_req = vm.list_attr_product[i];
                        attr_req.default_value = "";
                        vm.list_attr_required.push(attr_req);
                    }
                }
            }
            validateConfigToUpload();
        }


        /**
         * Completa los atributos requeridos que quedaron si asignar con las columnas del csv, con valores
         * por defecto ingresados por el usuario
         */
        function completeAttributeRequired(){
            if(vm.list_attr_required.length > 0){
                for(var i=0; i < vm.list_attr_required.length; i++ ){
                    if(vm.list_attr_required[i].default_value != ''){
                        //Generate new attr config (config attr with default value)
                        var attr = {};
                        attr.column_index = "";
                        attr.name = vm.list_attr_required[i].value;
                        attr.attr_product = angular.toJson(vm.list_attr_required[i]);
                        vm.config.push(attr);

                        vm.list_attr_required.splice(vm.list_attr_required.indexOf(vm.list_attr_required[i]), 1);

                    }
                }
                validateConfigToUpload();
            }
        }


        /**
         *  Valida si no queda ningun atributo requerido sin setear.
         */
        function validateConfigToUpload(){
            if(vm.list_attr_required.length == 0){
                uploadCsv();


            }else{
                AlertNotification.error("You need set some attribute to product is required");
            }
        }


        /**
         * Envia el archivo csv con un array de configuracion al servidor y maneja las respuestas
         */
        function uploadCsv(){

             vm.file_csv = $scope.filecsvproduct;
             Products.upload(vm)
                    .then(uploadProductSuccessFn, uploadProductErrorFn);

            /**
             * @name createProductSuccessFn
             * @desc Show snackbar with success message
             */
            function uploadProductSuccessFn(data, status, headers, config) {
                cancel();
                AlertNotification.success('Success! Import csv. Products created.');
            }


            /**
             * @name createProductErrorFn
             * @desc Propogate error event and show snackbar with error message
             */
            function uploadProductErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }



        /**
         *  Function init
         */
        function activate(){
            $scope.fileContent = {};

            get_fields();

            //Add listening csv file
            $scope.$watch('fileContent', changeCsv);

            // When csv change. Generate array to help config attribute product
            function changeCsv(changeEvent) {
                if($scope.fileContent != ''){
                    var product_array = CSVToArray($scope.fileContent);

                    var header = product_array[0];

                    var example = product_array[1];
                    vm.config = [];
                    for(var i= 0; i < header.length; i ++){
                        var attr = {};
                        attr.column_name = header[i];
                        attr.column_index = i;
                        attr.name = "";
                        attr.attr_product = {};
                        attr.example = "";
                        if(example){
                            attr.example = example[i];
                        }
                        vm.config.push(attr);
                    }
                }
            }

        }


        /**
         *  When select attribute product to a column, merge product with csv column config
         * @param obj
         */
        function selected_attr(obj){
            for(var i=0; i < vm.list_attr_product.length; i++){
                if(vm.list_attr_product[i].value == obj.name){
                    obj.attr_product = angular.toJson(vm.list_attr_product[i]);
                    break;
                }
            }
        }


        /**
         * Get all attribute to producto for select column csv
         */
        function get_fields(){
            Products.get_fields().then(getFieldsSuccess, getFieldsError);

            function getFieldsSuccess(data, headers){
                vm.list_attr_product = data.data;
                vm.list_attr_data = angular.copy(data.data);
            }

            function getFieldsError(data, headers){
                console.log(data);
            }
        }


        /**
         * Cast csv data to array
         * @param strData
         * @param strDelimiter
         * @returns {*[]}
         * @constructor
         */
        function CSVToArray( strData, strDelimiter ){
            // Check to see if the delimiter is defined. If not,
            // then default to comma.
            strDelimiter = (strDelimiter || ",");
            // Create a regular expression to parse the CSV values.
            var objPattern = new RegExp(
                (
                    // Delimiters.
                    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
                        // Quoted fields.
                        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
                        // Standard fields.
                        "([^\"\\" + strDelimiter + "\\r\\n]*))"
                    ),
                "gi"
            );
            // Create an array to hold our data. Give the array
            // a default empty first row.
            var arrData = [[]];
            // Create an array to hold our individual pattern
            // matching groups.
            var arrMatches = null;
            // Keep looping over the regular expression matches
            // until we can no longer find a match.
            while (arrMatches = objPattern.exec( strData )){
                // Get the delimiter that was found.
                var strMatchedDelimiter = arrMatches[ 1 ];
                // Check to see if the given delimiter has a length
                // (is not the start of string) and if it matches
                // field delimiter. If id does not, then we know
                // that this delimiter is a row delimiter.
                if (
                    strMatchedDelimiter.length &&
                        (strMatchedDelimiter != strDelimiter)
                    ){
                    // Since we have reached a new row of data,
                    // add an empty row to our data array.
                    arrData.push( [] );
                }
                // Now that we have our delimiter out of the way,
                // let's check to see which kind of value we
                // captured (quoted or unquoted).
                if (arrMatches[ 2 ]){
                    // We found a quoted value. When we capture
                    // this value, unescape any double quotes.
                    var strMatchedValue = arrMatches[ 2 ].replace(
                        new RegExp( "\"\"", "g" ),
                        "\""
                    );
                } else {
                    // We found a non-quoted value.
                    var strMatchedValue = arrMatches[ 3 ];
                }
                // Now that we have our value string, let's add
                // it to the data array.
                arrData[ arrData.length - 1 ].push( strMatchedValue );
            }
            // Return the parsed data.
            return( arrData );
        }

    }
})();
