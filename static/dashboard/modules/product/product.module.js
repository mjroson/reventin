(function () {
    'use strict';

    angular
        .module('app.product', [
            'app.product.controllers',
            //'app.product.directives',
            'app.product.services'
        ]);

    angular
        .module('app.product.controllers', []);

    /*angular
        .module('app.product.directives', ['ngDialog']);
    */
    angular
        .module('app.product.services', []);

})();