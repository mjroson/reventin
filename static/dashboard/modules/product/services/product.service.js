/**
 * Products
 * @namespace app.products.services
 */
(function () {
    'use strict';

    angular
        .module('app.product.services')
        .factory('Products', Products);

    Products.$inject = ['$http'];

    /**
     * @namespace Product
     * @returns {Factory}
     */
    function Products($http) {

        var Products = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            get_custom_attr: get_custom_attr,
            update: update,
            upload: upload,
            get_fields: get_fields
        };

        return Products;

        ////////////////////

        /**
         * @name all
         * @desc Get all Products
         * @returns {Promise}
         * @memberOf app.products.services.Products
         */
        function all() {
            return $http.get('/api/products/');
        }


        /**
         * @name create
         * @desc Create a new Product
         * @param {object} to product
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function create(product, images) {
            var fd = new FormData();
            fd.append('data', angular.toJson(product));
            angular.forEach(images, function (val, key) {
                fd.append(key, val.file);
            });
            return $http.post('/api/products/', fd, {
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            });
        }


        /**
         * @name destroy
         * @Delete a Product
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function destroy(id) {
            return $http.delete('/api/products/' + id + "/");
        }


        /**
         * @name detail
         * @Detail a Product
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function detail(id) {
            return $http.get('/api/products/' + id + "/");
        }


        function get_custom_attr(){
            return $http.get('/api/companies/config-attributes/product/active/');
        }


        /**
         * @name update
         * @desc Update a Product
         * @param {Object} object product
         * @returns {Promise}
         * @memberOf app.category.services.Products
         */
        function update(product, images) {
            var fd = new FormData();
            fd.append('data', angular.toJson(product));
            angular.forEach(images, function (val, key) {
                fd.append(key, val.file);
            });
            return $http.put('/api/products/' + product.id + '/', fd, {
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            });
        }


        /**
         * @name create
         * @desc Upload a csv to update product
         * @param {object} object with file csv
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function upload(obj) {
            var fd = new FormData();
            fd.append('file_csv', obj.file_csv);
            fd.append('config', angular.toJson(obj.config));
            return $http.post('/api/products/upload-csv-import/', fd,{
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            } );
        }

        function get_fields(){
            return $http.get('/api/products/fields/');
        }


    }
})();
