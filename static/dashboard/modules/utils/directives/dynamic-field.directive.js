/**
 * Select country
 * @namespace app.utils.directives
 */
(function () {
    'use strict';

    angular
        .module('app.utils.directives')
        .directive('dynamicField', dynamicField);

    dynamicField.$inject = ['$parse'];

    /**
     * @namespace Dynamic field
     */
    function dynamicField($parse) {

        return {
            controller: 'DynamicFieldController',
            //controllerAs: 'vm',
            restrict: 'AE',
            scope: {
               attrName: '@',
               attrType: '@',
               attrValues: '@',
               attrRequired: '@',
               attrValue: '@',
               attrId: '@'
            },
            templateUrl: '/static/dashboard/modules/utils/templates/dynamic-field.html',
            link: function(scope, element, attrs) {
                scope.list_values = JSON.parse(scope.attrValues);
                if(scope.attrValue != ''){
                    // If type is number, then cast value to number
                    scope.property_value = (scope.attrType == 'NUMBER') ? Number(scope.attrValue) : scope.attrValue;
                    console.log(scope.attrValue);
                    if(scope.attrType == 'LIST' && scope.attrValue){

                        scope.option = [];
                        scope.property_value = scope.attrValue;
                        var all_values = eval(scope.attrValues);
                        var values = eval(scope.attrValue);
                        for(var i=0; i < all_values.length; i++){
                            scope.option[i] = false;
                            for(var i1=0; i1 < values.length; i1++){
                               if(all_values[i] == values[i1]){
                                   scope.option[i] = true;
                                   break;
                               }
                            }
                        }
                    }
                }

                element.bind('change', function(){
                    for(var i=0; i < scope.$parent.$parent.vm.properties.length; i++){
                        if(scope.$parent.$parent.vm.properties[i].id == scope.attrId){
                            
                            var value = [];
                            if(Array.isArray(scope.$$childTail.property_value)){
                                value = scope.$$childTail.property_value;
                            }else{
                                value.push(String(scope.$$childTail.property_value))    
                            }
                            scope.$parent.$parent.vm.properties[i].values = value;
                            break;
                        }
                    }
                });
            }
        };

    }
})();