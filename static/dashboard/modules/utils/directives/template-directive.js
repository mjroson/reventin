/**
 * Select categories
 * @namespace app.utils.directives
 */
(function (angular) {
    'use strict';

    angular
        .module('app.utils.directives')
        .directive('templatePath', templatePath);

    templatePath.$inject = ['$http', '$compile'];

    /**
     * @namespace Dynamic select to services
     */
    function templatePath($http, $compile) {
        return {
			//attribute
			restrict: 'A',

			//isolate
			scope: {},

			link: function ( scope, element, attrs ) {

				//dim layer
				element.append($compile('<div></div>')(scope));

				$http.get(attrs.templatePath).success(function (panelContent) {
						var template = '<div style="height: 100%;" data-ng-controller="' + attrs.controllerName + '">' + panelContent + '</div>';
						element.append($compile(template)(scope));
					});
			}
		}
    }
})(angular);
