/**
 * Select categories
 * @namespace app.utils.directives
 */
(function () {
    'use strict';

    angular
        .module('app.utils.directives')
        .directive('selectservice', selectservice);

    selectservice.$inject = ['$http'];

    /**
     * @namespace Dynamic select to services
     */
    function selectservice($http) {

        return {
            restrict: 'E',
            scope: {
                services: '@',
                itemSelected: '@'
            },
            template: '<select class="form-control" ng-model="itemSelected" name="{{ aModel }}" ng-required="{{ selectRequired }}"  validate-on="blur" data-ng-options="obj.id as obj[name] for obj in objects"></select>',
            link: function(scope, element, attrs, ngModelCtrl) {
                scope.aModel = attrs.attrModel;


                if(attrs.attrName && attrs.attrName != null){
                    scope.name = attrs.attrName;
                }else{
                    scope.name = 'name';
                }

                if(attrs.selectRequired && attrs.selectRequired == "true"){
                    scope.selectRequired = true;
                }else{
                    scope.selectRequired = false;
                }

                if(attrs.services == undefined){
                    console.log("SELECT-SERVICE: Error description: Need assign url to service with attributes services");
                    return undefined;
                }else{
                    return $http.get(attrs.services).then(getSuccess, getError);
                }

                function getSuccess(data, headers, status){
                    scope.objects = data.data;
                }
                function getError(data, headers, status){
                    console.log('ERROR EN EL SERVICIO');
                }

                element.bind('change', function(){
                    scope.$parent.vm[scope.aModel] =  scope.itemSelected;
                });
            }
        };
    }
})();
