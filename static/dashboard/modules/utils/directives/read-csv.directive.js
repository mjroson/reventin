/**
 * fileCsvReader
 * @namespace app.utils.directives
 */
(function () {
    'use strict';

    angular
        .module('app.utils.directives')
        .directive('fileReader', fileReader);

    fileReader.$inject = [];
    /**
     * @namespace File csv read
     */
    function fileReader() {
        /**
         * @name directive
         * @desc The directive to be returned
         * @memberOf app.utils.directives.fileReader
         */
        var directive = {
            link: link,
            scope: {
                fileReader:"="
            }
        };


        function link(scope, element) {
            $(element).on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                if (files.length) {
                    var r = new FileReader();
                    r.onload = function(e) {
                        var contents = e.target.result;
                        scope.$apply(function () {
                            scope.fileReader = contents;
                        });
                    };
                    r.readAsText(files[0]);
                }
            });
        }

        return directive;
    }
})();

