/**
 * Select categories
 * @namespace app.utils.directives
 */
(function () {
    'use strict';

    angular
        .module('app.utils.directives')
        .directive('selectService', selectservice);

    selectservice.$inject = ['$http'];

    /**
     * @namespace Dynamic select to services
     */
    function selectservice($http) {
        return {
            restrict: 'A',
            scope: {
                services: '@'
            },
            link: function(scope, element, attrs, ngModelCtrl) {

                var name = 'name';
                if(attrs.attrName && attrs.attrName != null){
                    name = attrs.attrName;
                }


                if(attrs.services == undefined){
                    console.log("SELECT-SERVICE: Error description: Need assign url to service with attributes services");
                    angular.element(element[0]).append(angular.element('<option value=""></option>'));
                }else{
                    return $http.get(attrs.services).then(getSuccess, getError);
                }



                function getSuccess(data, headers, status){
                    var results = data.data;
                    for(var i=0; i < results.length; i++){
                        angular.element(element[0]).append(angular.element('<option value="'+ results[i]['id'] + '">'+ results[i][name] +'</option>'));
                    }
                }
                function getError(data, headers, status){
                    console.log('ERROR EN EL SERVICIO');
                    return $http.get(attrs.services).then(getSuccess, getError);
                }

            }
        };
    }
})();
