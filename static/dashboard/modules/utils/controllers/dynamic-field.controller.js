/**
 * DynamicFieldController
 * @namespace app.utils.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.utils.controllers')
        .controller('DynamicFieldController', DynamicFieldController);

    DynamicFieldController.$inject = ['$scope','$parse', 'AlertNotification'];

    /**
     * @namespace DynamicFieldController
     */
    function DynamicFieldController($scope,$parse, AlertNotification) {
        var vm = this;

        $scope.change_select_value = function(opt, value){
            if(value){
                if(!$scope.property_value) $scope.property_value = [];
                $scope.property_value.push(opt);     
            }else{
                $scope.property_value.splice($scope.property_value.indexOf(opt), 1);
            }
        } 
    }
})();