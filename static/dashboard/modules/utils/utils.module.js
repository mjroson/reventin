(function () {
    'use strict';

    angular
        .module('app.utils', [
            'app.utils.directives',
            'app.utils.controllers'
        ]);

    //angular
    //    .module('app.utils.services', []);

    angular
        .module('app.utils.directives', []);

    angular
        .module('app.utils.controllers', []);
})();