/**
 * PanelController
 * @namespace app.profile.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.profile.controllers')
        .controller('PanelController', PanelController);

    PanelController.$inject = ['$scope', 'AlertNotification', 'Profile', 'Authentication'];

    /**
     * @namespace PanelController
     */
    function PanelController($scope, AlertNotification, Profile, Authentication) {
        var vm = this;

        vm.upload = upload_img;
        activate();

        $scope.img_profile = {};

        vm.edit_obj = function(){
            $scope.$broadcast('objData', vm.user);
            vm.objEdit = !vm.objEdit;
        }

        function activate(){
            vm.user = Authentication.getUser();

            AlertNotification.error("Este es un error");
            AlertNotification.success("Este es un OK");
            AlertNotification.warning("ESta es una advertencia");
            AlertNotification.info("Este es de informacion");

            $scope.$on('obj.edited', function (event, objEdit) {
                vm.user = objEdit;
            });
        }

        //$scope.$watch('img_profile', upload_img);

        function upload_img(){

            if ($scope.img_profile.name){
                 vm.promise_img = Profile.upload_img($scope.img_profile).then(ChangeImgSuccess, ChangeImgError);
            }

            function ChangeImgSuccess(data, status, headers, config){
                vm.user.img = data.data.url_img;
                $scope.img_profile = {};
            }

            function ChangeImgError(data, status, headers, config){
                 AlertNotification.error(data.error);
            }
        }
        $scope.$watch('img_profile', function(){
            upload_img();
        });
        
    }
})();
