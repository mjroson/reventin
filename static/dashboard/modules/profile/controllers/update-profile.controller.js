/**
 * UpdateProfileController
 * @namespace app.profile.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.profile.controllers')
        .controller('UpdateProfileController', UpdateProfileController);

    UpdateProfileController.$inject = ['AlertNotification', 'Profile', '$rootScope', '$scope'];

    /**
     * @namespace UpdateProfileController
     */
    function UpdateProfileController(AlertNotification, Profile, $rootScope, $scope) {
        var vm = this;

        vm.submit = submit;
        
        vm.obj = {};

        vm.close = close;

        vm.add_address = add_address;
        vm.delete_address = delete_address;

        vm.add_phone = add_phone;
        vm.delete_phone = delete_phone;

        activate();


        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
        }

        /**
         * @name submit
         * @desc Update a Profile
         * @memberOf app.user.controllers.UpdateUserController
         */
        function submit() {
            //vm.obj.start_date = $filter('date')(vm.obj.start_date,'yyyy-MM-dd');
            //vm.obj.end_date = $filter('date')(vm.obj.end_date,'yyyy-MM-dd');
            vm.requestPromise = Profile.update(vm.obj)
                .then(updateUserSuccessFn, updateUserErrorFn);

            /**
             * @name updateUserSuccessFn
             */
            function updateUserSuccessFn(data, status, headers, config) {
                AlertNotification.success('Success! Profile updated.');
                $rootScope.$broadcast('obj.edited', data.data);
            }


            /**
             * @name updateUserErrorFn
             */
            function updateUserErrorFn(data, status, headers, config) {
                //$rootScope.$broadcast('product.created.error');
                AlertNotification.error(data.error);
            }
        }

        function activate(){
            $scope.$on('objData', function(event, args){
                vm.obj = angular.copy(args);
            });
        }

        function add_phone(){
            if(vm.obj.phones == undefined || vm.obj.phones == null){
                vm.obj.phones = [];
            }
            var phone = {};
            vm.obj.phones.push(phone);
        }

        function delete_phone(phone){
            vm.obj.phones.splice(vm.obj.phones.indexOf(phone), 1);
        }

        function add_address(){
            if(vm.obj.addresses == undefined || vm.obj.addresses == null){
                vm.obj.addresses = [];
            }
            var address = {};
            vm.obj.addresses.push(address);
        }

        function delete_address(address){
            vm.obj.addresses.splice(vm.obj.addresses.indexOf(address), 1);
        }
    }
})();
