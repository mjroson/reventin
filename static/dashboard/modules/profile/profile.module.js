(function () {
    'use strict';

    angular
        .module('app.profile', [
            'app.profile.controllers',
            //'app.product.directives',
            'app.profile.services'
        ]);

    angular
        .module('app.profile.controllers', []);

    /*angular
        .module('app.product.directives', ['ngDialog']);
    */
    angular
        .module('app.profile.services', []);

})();