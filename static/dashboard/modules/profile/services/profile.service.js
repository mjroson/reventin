/**
 * Profile
 * @namespace app.profile.services
 */
(function () {
    'use strict';

    angular
        .module('app.profile.services')
        .factory('Profile', Profile);

    Profile.$inject = ['$http'];

    /**
     * @namespace Profile
     * @returns {Factory}
     */
    function Profile($http) {

        var Profile = {
            upload_img: upload_img,
            update: update
        };

        return Profile;


        function update(user){
            return $http.put('/api/profile/', user)
        }

        function upload_img(image) {
            var fd = new FormData();

            fd.append("img", image);
            return $http.post('/api/profile/change-img/', fd, {
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            });
        }


    }
})()