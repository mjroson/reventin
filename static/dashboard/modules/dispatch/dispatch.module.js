(function () {
    'use strict';

    angular
        .module('app.dispatch', [
            'app.dispatch.controllers',
            'app.dispatch.services'
        ]);

    angular
        .module('app.dispatch.controllers', ['ngDialog']); //'mgcrea.ngStrap',

    angular
        .module('app.dispatch.services', []);

})();