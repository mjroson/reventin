/**
 * NewDispatchController
 * @namespace app.dispatch.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.dispatch.controllers')
        .controller('NewDispatchController', NewDispatchController);

    NewDispatchController.$inject = ['AlertNotification', 'Dispatchs', '$state', 'Products'];

    /**
     * @namespace NewDispatchController
     */
    function NewDispatchController(AlertNotification, Dispatchs, $state, Products) {
        var vm = this;


        vm.dispatch = {};
        vm.dispatch.details = [];
        vm.orders = [];
        vm.order = {};

        vm.dispatch_all = dispatch_all;
        vm.dispatch_custom = dispatch_custom;

        get_orders();

        function dispatch_all(order){
            vm.dispatch.order = order.id;
            vm.dispatch.details = order.details;
            vm.dispatch.all = true;
            create();
        }

        function dispatch_custom(order){
            vm.dispatch.order = order.id;
            vm.dispatch.details = order.details;
            create();
        }

        function create(){

            Dispatchs.create(vm.dispatch)
                .then(createDispatchSuccessFn, createDispatchErrorFn);

            /**
             * @name createDispatchSuccessFn
             */
            function createDispatchSuccessFn(data, status, headers, config) {
                AlertNotification.success('Success! Dispatch created.');
                $state.go('dispatchs');
            }

            /**
             * @name createDispatchErrorFn
             */
            function createDispatchErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }

        function get_orders(){
            Dispatchs.get_order_active().then(getOrderSuccess, getOrderError);

            function getOrderSuccess(data, status, headers, config){
                vm.orders = data.data;
            }

            function getOrderError(data, status, headers, config){
                AlertNotification.success(data.message)
            }
        }

    }
})();
