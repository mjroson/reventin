/**
 * DetailDispatchController
 * @namespace app.dispatch.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.dispatch.controllers')
        .controller('DetailDispatchController', DetailDispatchController);

    DetailDispatchController.$inject = ['AlertNotification', 'Orders', '$stateParams'];

    /**
     * @namespace DetailDispatchController
     */
    function DetailDispatchController(AlertNotification, Orders, $stateParams) {
        var vm = this;

        vm.price_list = {};
        init();

        /**
         * @name init
         * @desc Call service to request detail price list
         * @memberOf app.order.controllers.DetailDispatchController
         */
        function init(){
            Orders.detail($stateParams.id).then(getDispatchSuccess, getDispatchError);

            function getDispatchSuccess(data, status, headers, config){
                vm.price_list = data.data;
            }

            function getDispatchError(data, status, headers, config){
                AlertNotification.success('Error request price list detail');
            }
        }

    }
})();
