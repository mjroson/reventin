/**
 * Orders
 * @namespace app.dispatch.services
 */
(function () {
    'use strict';

    angular
        .module('app.dispatch.services')
        .factory('Dispatchs', Dispatchs);

    Dispatchs.$inject = ['$http'];

    /**
     * @namespace Dispatchs
     * @returns {Factory}
     */
    function Dispatchs($http) {

        var Orders = {
            all: all,
            create: create,
            detail: detail,
            get_order_active: get_order_active,
        };

        return Orders;

        ////////////////////

        /**
         * @name all
         * @desc Get all Catalogs
         * @returns {Promise}
         * @memberOf app.order.services.Catalogs
         */
        function all() {
            return $http.get('/api/dispatch/');
        }


        /**
         * @name create
         * @desc Create a new Catalog
         * @param {Objects} Objects to data info order, and details order data.
         * @returns {Promise}
         * @memberOf app.order.services.Catalogs
         */
        function create(data) {

            return $http.post('/api/dispatch/', data );
        }

        /**
         * @name destroy
         * @Delete a Catalog
         * @returns {Promise}
         * @memberOf app.orders.services.Order

        function destroy(id) {
            return $http.delete('/api/orders/' + id + "/");
        }*/


        /**
         * @name detail
         * @Detail a Price List
         * @returns {Promise}
         * @memberOf app.orderss.services.Order
         */
        function detail(id) {
            return $http.get('/api/dispatch/' + id + "/");
        }


        function get_order_active(){
            return $http.get('/api/orders/');
        }
        /**
         * @name update
         * @desc Update a Price Lists
         * @param {Object} object to data price lists
         * @returns {Promise}
         * @memberOf app.orders.services.Order

        function update(data) {
            return $http.put('/api/orders/' + data.id + '/', data);
        }*/

    }
})();
