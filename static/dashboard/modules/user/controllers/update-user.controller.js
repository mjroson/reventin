/**
 * UpdateUserController
 * @namespace app.user.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user.controllers')
        .controller('UpdateUserController', UpdateUserController);

    UpdateUserController.$inject = ['AlertNotification', 'Users', '$scope', '$rootScope', 'Authentication'];

    /**
     * @namespace UpdateUserController
     */
    function UpdateUserController(AlertNotification, Users, $scope, $rootScope, Authentication) {
        var vm = this;

        // Functions
        vm.submit = submit;
        vm.add_address = add_address;
        vm.delete_address = delete_address;
        vm.add_phone = add_phone;
        vm.delete_phone = delete_phone;
        vm.close = close;
        vm.change_is_staff = change_is_staff;

        // Vars
        vm.properties = [];

        vm.obj = {};
        vm.obj.phones = [];
        vm.obj.addresses = [];
        vm.obj.email = "";

        vm.email_error = false;
        vm.email_is_valid = false;

        vm.old_obj = {};


        vm.tab = 1;


        activate();




        function close(){
            $scope.$parent.$parent.vm.objEdit = !$scope.$parent.$parent.vm.objEdit;
            vm.objEdit = false;
        }


        /**
         * @name submit
         * @desc Create a new User
         * @memberOf app.user.controllers.NewUserController
         */
        function submit() {
            var properties = [];
            // Format properties
            for(var i=0; i < vm.properties.length; i++){
                if (vm.properties[i].values){
                    var prop = {};
                    prop.name = vm.properties[i].attr_name;
                    prop.value = vm.properties[i].values[0];
                    prop.attr_id = vm.properties[i].id
                    properties.push(prop);
                }
            }
            vm.obj.properties = properties;


            vm.requestPromise = Users.update(vm.obj)
                .then(createUserSuccessFn, createUserErrorFn);

            /**
             * @name createUserSuccessFn
             */
            function createUserSuccessFn(data, status, headers, config) {

                $rootScope.$broadcast('obj.edited', data.data, vm.old_obj);
                vm.objEdit = false;
                AlertNotification.success('Usuario editado correctamente.');
            }



            /**
             * @name createUserErrorFn
             */
            function createUserErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }


        /**
         *  Function to change staff
         */
        function change_is_staff(){
            if(vm.obj.is_staff == false){
                vm.email_error = false;
            }else if (vm.email_is_valid == false){
                vm.email_error = true;
            }
        }


        /**
         *  Function to capture change email.
         */
        $scope.$watch('vm.obj.email', function(newValue, oldValue){
            if(vm.email != vm.obj.email &&
                String(vm.obj.email).split('@').length > 1 &&
                String(vm.obj.email).split('.').length > 1 &&
                String(vm.obj.email).split('.')[1].length > 1){

                Users.is_email_valid(vm.obj.email).then(isEmailValidSuccess);
            }
            function isEmailValidSuccess(data){
                if (data.data.is_valid != 'true'){
                    vm.email_error = true;
                    vm.email_is_valid = false;
                }else{
                    vm.email_error = false;
                    vm.email_is_valid = true;
                }
            }
        });


        /**
         * Function to change default address, verify to unique default addresses
         * @param address
         */
        vm.on_change_address_default = function(address){
            if(vm.obj.addresses.length > 1){
                for(var i=0; i < vm.obj.addresses.length; i++){
                    // If address if diferent to address change and address is default true, address event cant change default
                    if (vm.obj.addresses[i] != address && vm.obj.addresses[i].default == true){
                        vm.obj.addresses[i].default = false;
                    }
                }
            }
        }

        /**
         * Function to add phone
         */
        function add_phone(){
            var phone = {};
            vm.obj.phones.push(phone);
        }

        /**
         * Delete phone
         * @param phone
         */
        function delete_phone(phone){
            vm.obj.phones.splice(vm.obj.phones.indexOf(phone), 1);
        }

        function add_address(){
            var address = {};
            vm.obj.addresses.push(address);
        }

        function delete_address(address){
            vm.obj.addresses.splice(vm.obj.addresses.indexOf(address), 1);
        }


        function activate(){
            $scope.$on('objData', function(event, args){
                vm.old_obj = args;
                vm.obj = angular.copy(args);
                vm.email = angular.copy(vm.obj.email);
                Users.get_custom_attr().then(getCustomAttrSuccess, getCustomAttrError);
            });

            // TODO Guardar datos de usuario autenticado en una variable global
            vm.user_auth = Authentication.getUser();

            function getCustomAttrSuccess(data, status, headers, config){
                vm.properties = data.data;
                //TODO: is bad, in case of hasnt property, the attributes doesnt exist
                if(vm.obj.properties.length > 0){
                    for(var i=0; i < vm.obj.properties.length; i++){
                        for(var w=0; w < vm.properties.length; w++){
                            if(vm.obj.properties[i].attr_id == vm.properties[w].id){
                                vm.properties[w].value = vm.obj.properties[i].value;
                                break;
                            }
                        }
                    }
                }
                vm.obj.user_type = vm.obj.user_type.id;
            }

            function getCustomAttrError(data, status, headers, config){
                AlertNotification.error(data.message);
            }
        }

    }
})();
