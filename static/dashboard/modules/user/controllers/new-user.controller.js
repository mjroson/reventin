/**
 * NewUserController
 * @namespace app.user.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user.controllers')
        .controller('NewUserController', NewUserController);

    NewUserController.$inject = ['AlertNotification', 'Users', '$scope', '$rootScope', 'Authentication'];

    /**
     * @namespace NewUserController
     */
    function NewUserController(AlertNotification, Users, $scope, $rootScope, Authentication) {
        var vm = this;

        vm.submit = submit;
        vm.add_address = add_address;
        vm.delete_address = delete_address;
        vm.add_phone = add_phone;
        vm.delete_phone = delete_phone;
        vm.change_is_staff = change_is_staff;
        vm.close = close;


        vm.tab = 1;

        activate();

        function close(){
            $scope.$parent.$parent.vm.objNew = !$scope.$parent.$parent.vm.objNew;
        }

        vm.cancel = function(){
            $scope.formCreateUser.reset();
            initData();
            vm.properties = angular.copy(vm.propertiesData);
            close();
        }

        /**
         * @name submit
         * @desc Create a new User
         * @memberOf app.user.controllers.NewUserController
         */
        function submit() {
            var properties = [];
            for(var i=0; i < vm.properties.length; i++){
                if (vm.properties[i].values){
                    var prop = {};
                    prop.name = vm.properties[i].attr_name;
                    prop.value = vm.properties[i].values[0];
                    prop.attr_id = vm.properties[i].id
                    properties.push(prop);
                }
            }
            vm.obj.properties = properties;

            vm.requestPromise = Users.create(vm.obj)
                .then(createUserSuccessFn, createUserErrorFn);

            /**
             * @name createUserSuccessFn
             */
            function createUserSuccessFn(data, status, headers, config) {
                $rootScope.$broadcast('obj.created', data.data);
                $scope.formCreateUser.reset();
                initData();
                vm.properties = angular.copy(vm.propertiesData);
                vm.user_auth.has_children = "True";
                close();
                AlertNotification.success('Usuario creado.');
            }
            /**
             * @name createUserErrorFn
             */
            function createUserErrorFn(data, status, headers, config) {
                AlertNotification.error(data.error);
            }
        }

        function change_is_staff(){
            if(vm.obj.is_staff == false){
                vm.email_error = false;
            }else if (vm.email_is_valid == false){
                vm.email_error = true;
            }
        }

        $scope.$watch('vm.obj.email', function(newValue, oldValue){
            if(String(vm.obj.email).split('@').length > 1 && String(vm.obj.email).split('.').length > 1 && String(vm.obj.email).split('.')[1].length > 1){
                Users.is_email_valid(vm.obj.email).then(isEmailValidSuccess);
            }
            function isEmailValidSuccess(data){
                if (data.data.is_valid != 'true'){
                    vm.email_error = true;
                    vm.email_is_valid = false;
                }else{
                    vm.email_error = false;
                    vm.email_is_valid = true;
                }
            }
        });

        vm.on_change_address_default = function(address){
            if(vm.obj.addresses.length > 1){
                for(var i=0; i < vm.obj.addresses.length; i++){
                    // If address if diferent to address change and address is default true, address event cant change default
                    if (vm.obj.addresses[i] != address && vm.obj.addresses[i].default == true){
                        vm.obj.addresses[i].default = false;
                    }
                }
            }
        }


        function add_phone(){
            var phone = {};
            vm.obj.phones.push(phone);
        }

        function delete_phone(phone){
            vm.obj.phones.splice(vm.obj.phones.indexOf(phone), 1);
        }

        function add_address(){

            var address = {};
            vm.obj.addresses.push(address);
        }

        function delete_address(address){
            vm.user.addresses.splice(vm.obj.addresses.indexOf(address), 1);
        }

        function activate(){
            initData();

            Users.get_custom_attr().then(getCustomAttrSuccess, getCustomAttrError);

            vm.user_auth = Authentication.getUser();

            function getCustomAttrSuccess(data, status, headers, config){
                vm.propertiesData = data.data;
                vm.properties = angular.copy(vm.propertiesData);
            }
            function getCustomAttrError(data, status, headers, config){
                AlertNotification.error(data.message);
            }
        }


        function initData(){
            vm.properties = [];
            vm.obj = {};

            vm.obj.phones = [];
            vm.obj.addresses = [];
            vm.obj.email = "";
            vm.email_error = false;
            vm.email_is_valid = false;
            vm.custom_user_father = false;

            vm.tab = 1;
            add_phone();
            add_address();
        }

    }
})();
