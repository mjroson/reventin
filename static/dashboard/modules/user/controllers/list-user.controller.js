/**
 * ListUserController
 * @namespace app.user.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user.controllers')
        .controller('ListUserController', ListUserController);

    ListUserController.$inject = ['AlertNotification', 'Users', '$filter', 'ngTableParams', '$scope'];

    /**
     * @namespace ListUserController
     */
    function ListUserController(AlertNotification, Users, $filter, ngTableParams, $scope) {
        var vm = this;

        vm.destroy = destroy;
        vm.clear_text = clear_text;

        vm.obj_list = [];

        vm.search_field = 'last_name';

        vm.search_text = '';

        vm.filters = {
            last_name: "",
            first_name: "",
            email: ""
        };


        vm.edit_obj = function(obj){
            $scope.$broadcast('objData', obj);
            vm.objEdit = !vm.objEdit;
        }

        vm.change_search_text = change_search_text;

        function change_search_text(){
            // Clear All filter
            for(var filter in vm.filters){
                vm.filters[filter] = "";
            }

            vm.filters[vm.search_field] = vm.search_text;
        }

        function clear_text(){
            vm.search_text = "";
            change_search_text();
        }

        activate();


        /**
         * @name activate
         * @desc Call service to request all users
         * @memberOf app.user.controllers.ListUserController
         */
        function activate(){

            vm.requestPromise = Users.all().then(getAllUserSuccess, getAllUserError);

            function getAllUserSuccess(data, status, headers, config){
                vm.obj_list = data.data;
            }

            function getAllUserError(data, status, headers, config){
                AlertNotification.error(data.data.message);
            }


            $scope.$on('obj.created', function (event, newObj) {
                vm.obj_list.unshift(newObj);
            });

            $scope.$on('obj.edited', function (event, newObjList, oldObjList) {
                vm.obj_list[vm.obj_list.indexOf(oldObjList)] = newObjList;
            });

        }


        /**
         * @name destroy
         * @desc remove one user
         * @Param {String} id to user want delete
         * @memberOf app.catalo.controllers.ListUserController
         */
        function destroy(obj){
            Users.destroy(obj.id).then(destroySuccess, destroyError);

            function destroySuccess(data, status, headers, config){
                vm.obj_list.splice(vm.obj_list.indexOf(obj), 1);
                AlertNotification.success("User deleted");
            }

            function destroyError(data, status, headers, config){
                AlertNotification.error(data.data.message);
            }
        }


        vm.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            filter: vm.filters,
            sorting: {
                last_name: 'asc'     // initial sorting
            }
        }, {
            total: vm.obj_list.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var filteredData = params.filter() ?
                    $filter('filter')(vm.obj_list, params.filter()) :
                    vm.obj_list;
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.$watchCollection("vm.obj_list", function () {
            vm.tableParams.reload();
        });


    }
})();
