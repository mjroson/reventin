/**
 * NewUserEmailController
 * @namespace app.user.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.user.controllers')
        .controller('NewUserEmailController', NewUserEmailController);

    NewUserEmailController.$inject = ['AlertNotification', 'Users', '$state', '$scope'];

    /**
     * @namespace NewUserEmailController
     */
    function NewUserEmailController(AlertNotification, Users, $state, $scope) {
        var vm = this;

        vm.submit = submit;
        vm.close = close;


        initData();


        vm.cancel = function(){
            $scope.formCreateUser.reset();
            initData();
            close();
        }


        function initData(){
            vm.obj = {};
            vm.obj.email = "";
            vm.email_error = false;
            vm.email_is_valid = false;
        }

        /**
         * @name submit
         * @desc Create a new User
         * @memberOf app.user.controllers.NewUserEmailController
         */
        function submit() {

            vm.requestPromise = Users.create_by_email(vm.obj)
                .then(createUserSuccessFn, createUserErrorFn);

            /**
             * @name createUserSuccessFn
             */
            function createUserSuccessFn(data, status, headers, config) {
                vm.cancel();
                AlertNotification.success('El email fue enviado correctamente.');

            }


            /**
             * @name createUserErrorFn
             */
            function createUserErrorFn(data, status, headers, config) {
                AlertNotification.error(data.data.message);
            }
        }


        function close(){
            $scope.$parent.$parent.vm.objNewEmail = !$scope.$parent.$parent.vm.objNewEmail;
        }


        $scope.$watch('vm.obj.email', function(newValue, oldValue){
            if(String(vm.obj.email).split('@').length > 1 && String(vm.obj.email).split('.').length > 1 && String(vm.obj.email).split('.')[1].length > 1){
                // TODO: Need manager error response
                // TODO: Need validate if emails exist in recomendations
                Users.is_email_valid(vm.obj.email).then(isEmailValidSuccess);
            }

            function isEmailValidSuccess(data){
                    if (data.data.is_valid != 'true'){
                        vm.email_error = true;
                        vm.email_is_valid = false;
                    }else{
                        vm.email_error = false;
                        vm.email_is_valid = true;
                    }
                }
        });



    }
})();
