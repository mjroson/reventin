(function () {
    'use strict';

    angular
        .module('app.user', [
            'app.user.controllers',
            'app.user.services'
        ]);

    angular
        .module('app.user.controllers', []); //'mgcrea.ngStrap',


    angular
        .module('app.user.services', []);

})();