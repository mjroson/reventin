/**
 * Users
 * @namespace app.user.services
 */
(function () {
    'use strict';

    angular
        .module('app.user.services')
        .factory('Users', Users);

    Users.$inject = ['$http'];

    /**
     * @namespace Users
     * @returns {Factory}
     */
    function Users($http) {

        var Users = {
            all: all,
            create: create,
            destroy: destroy,
            detail: detail,
            update: update,
            upload: upload,
            create_by_email: create_by_email,
            get_custom_attr: get_custom_attr,
            is_email_valid: is_email_valid,

        };

        return Users;

        ////////////////////

        /**
         * @name all
         * @desc Get all Users
         * @returns {Promise}
         * @memberOf app.user.services.Users
         */
        function all() {
            return $http.get('/api/users/');
        }


        /**
         * @name create
         * @desc Create a new User
         * @param {Objects} Objects to data info user, and details user data.
         * @returns {Promise}
         * @memberOf app.user.services.Users
         */
        function create(data) {

            return $http.post('/api/users/', data );
        }

        /**
         * @name destroy
         * @Delete a User
         * @returns {Promise}
         * @memberOf app.user.services.Users
         */
        function destroy(id) {
            return $http.delete('/api/users/' + id + "/");
        }


        /**
         * @name detail
         * @Detail a User
         * @returns {Promise}
         * @memberOf app.user.services.Users
         */
        function detail(id) {
            return $http.get('/api/users/' + id + "/");
        }


        /**
         * @name update
         * @desc Update a User
         * @param {Object} object to data users and details user
         * @returns {Promise}
         * @memberOf app.user.services.Users
         */
        function update(user) {
            return $http.put('/api/users/' + user.id + '/', user);
        }


        /**
         * @name create
         * @desc Upload a csv to update product
         * @param {object} object with file csv
         * @returns {Promise}
         * @memberOf app.product.services.Products
         */
        function upload(image) {
            var fd = new FormData();
            fd.append('img_profile', image);
            return $http.post('/api/users/upload-image/', fd,{
                headers: {'Content-Type': undefined},
                withCredentials: true,
                transformRequest: angular.identity
            } );
        }

        function get_custom_attr(){
            return $http.get('/api/companies/config-attributes/user/active/');
        }

        function create_by_email(user){
            return $http.post('/api/users/recomendation/', user );
        }

        function is_email_valid(email){
            return $http.get('/api/users/is-email-valid/'+ email +'/');
        }
    }
})();