from bson.objectid import ObjectId

from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator

from gridfs import GridFS, NoFile

from mongoengine.connection import get_db

from apps.user.serializers import UserAuthenticationSerializer


class DashboardView(TemplateView):
    template_name = 'dashboard/index.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DashboardView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        if self.request.user.company.logo:
            context['logo'] = '/images/' + str(self.request.user.company.logo._id)
        else:
            context['logo'] = '/static/image/nubiquo.png'


        context['user_data'] = UserAuthenticationSerializer(instance=self.request.user).data
        return context


def serve_file(request, file_id):
    db = get_db()
    fs = GridFS(db)
    try:
        f = fs.get(ObjectId(file_id))
    except NoFile:
        # mongoengine stores images in a separate collection by default
        fs = GridFS(db, collection='images')
        try:
            f = fs.get(ObjectId(file_id))
        except NoFile:
            raise Http404

    response = HttpResponse(f.read(), content_type=f.content_type)
    #timestamp = datetime_now()  # time.mktime(gridout.upload_date.timetuple())
    response["Last-Modified"] = f.uploadDate
    # add other header data like etags etc. here
    return response

