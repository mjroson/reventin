from django.conf.urls import patterns, include, url

from .views import DashboardView

from rest_framework import routers

# IMPORT VIEWS TO API
from apps.product.views import CategoriesViewSet, ProductModelViewSet
from apps.user.views import LoginView, LogoutView, UserTypeModelViewSet, GetUserChildren
#from apps.company.views import AttrConfigProductCreateView, AttrConfigProductUpdateView, AttrConfigUserCreateView
from apps.catalog.views import CatalogModelViewSet, ActiveCatalogDetailListView
from apps.price_list.views import PriceListModelViewSet
from apps.permission.views import PermissionModelViewSet#, GroupModelViewSet, ContentTypeModelViewSet
from apps.order.views import OrderModelViewSet
from apps.dispatch.views import DispatchModelViewSet
from apps.user.views import CustomUserModelViewSet, RecomendationCreateView, CreateUserByRecomendationView, ProfileUpdateApiView

from apps.company.views import CompanyUpdateApiView, CompanyRetrieveApiView

# DEFINED URL TO API
router = routers.DefaultRouter()
router.register(r'products/categories', CategoriesViewSet, base_name='Category')
router.register(r'products', ProductModelViewSet, base_name='Product')
router.register(r'catalogs', CatalogModelViewSet, base_name="Catalog")
router.register(r'orders', OrderModelViewSet, base_name="Order")
router.register(r'dispatch', DispatchModelViewSet, base_name="Dispatch")
router.register(r'users/types', UserTypeModelViewSet, base_name="UserType")
router.register(r'users', CustomUserModelViewSet, base_name="User")
router.register(r'price-list', PriceListModelViewSet, base_name="PriceList")
router.register(r'permissions', PermissionModelViewSet, base_name="PermissionGroup")

# DEFINED ALL URL
urlpatterns = patterns('',
                       url('^api/users/is-email-valid/(?P<email>.+)/$',
                           'apps.user.views.is_email_valid',
                           name='api-user-is-email-valid'),

                       url('^login/$', LoginView.as_view(), name='login'),

                       url('^logout/$', LogoutView.as_view(), name='logout'),

                       # app utils urls
                       url(r'^api/utils/',
                           include('apps.utils.urls',
                                   namespace="util")),

                       url(r'images/(?P<file_id>\w+)$',
                           'reventin.views.serve_file',
                           name='show_image'),

                       url('^api/users/childrens/$',
                           GetUserChildren.as_view(),
                           name='api-users-children'),

                       # Get Catalogo detail Active
                       url('^api/catalogs/details/active/$',
                           ActiveCatalogDetailListView.as_view(),
                           name='api-catalogs-details-active'),

                       # app user urls
                       #url(r'^users/',
                       #    include('apps.user.urls',
                       #            namespace="user")),

                       # app company urls
                       url(r'^companies/',
                           include('apps.company.urls',
                                   namespace="company")),

                       url(r'^products/',
                           include('apps.product.urls',
                                   namespace="product")),

                       url('^api/products/upload-csv-import/$',
                           'apps.iecsv.views.import_csv',
                           name='api-product-import-csv'),

                       url('^api/products/fields/$',
                           'apps.iecsv.views.get_product_fields',
                           name='api-product-fields'),

                       # Add url (login and logout) to djangorestfullframework
                       #url(r'^api-auth/',
                       #    include('rest_framework.urls',
                       #            namespace='rest_framework')),
                       # upload img width gridfs
                       url(r'^gridfs/(?P<file_id>[0-9a-f]{24})/$',
                           'reventin.views.serve_file',
                           name='file-upload'),

                       ###### DEFINED URL API ######



                       #url('^api/companies/data-required/$',
                       #    'apps.company.views.set_data_required',
                       #    name='api-company-data-required'),

                       # Configurate attributes
                       url('^api/companies/config-attributes/(?P<belongs_to>[a-zA-Z]+)/$',
                           'apps.company.views.config_attributes',
                           name='api-company-cfg-attr-update'),

                       # Get config attributes. belongs_to accept two options 'user' , 'product', and filter accept 'all' or 'active'
                       url('^api/companies/config-attributes/(?P<belongs_to>[a-zA-Z]+)/(?P<filter>[a-zA-Z]+)/$',
                           'apps.company.views.get_custom_attr',
                           name='api-company-cfg-attr'),

                       url(r'api/catalogs/update/data/(?P<id>\w+)$',
                           'apps.catalog.views.get_data_to_update_catalog',
                           name='catalog-update-data'),
                       # Create recomendation for create user by email
                       url('^api/users/recomendation/$',
                           RecomendationCreateView.as_view(),
                           name='user-recomendation'),

                       url(r'api/profile/change-img/',
                           'apps.user.views.upload_img_profile',
                           name='upload-image-profile'),

                       url('^api/profile/$',
                           ProfileUpdateApiView.as_view(),
                           name='profile-update'),

                       url(r'^register-by-email/(?P<key>.+)/$',
                           CreateUserByRecomendationView.as_view(),
                           name='user-recomendation-create'),

                       url('^api/company/$',
                           CompanyRetrieveApiView.as_view(),
                           name='company-detail'),

                       url('^api/company/update/$',
                           CompanyUpdateApiView.as_view(),
                           name='company-update'),
                       #url('^api/users/recomendation/(?P<recomendation>[a-zA-Z0-9]+)/$',
                       #    UserRecomendationCreateView.as_view(),
                       #    name='user-recomendation-create'),

                       url(r'^api/',
                           include(router.urls,
                                   namespace="api",
                                   app_name="apit")),

                       #url('^csrf$', CsrfView.as_view(), name='csrf'),
                       url('^.*$', DashboardView.as_view(), name='dashboard'),
                       )
