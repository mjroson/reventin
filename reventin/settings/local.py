from .base import *

from mongoengine import connect

MONGO_DATABASE_NAME = 'reventin'

# Connection with database mongodb
DEFAULT_CONNECTION_NAME = connect(MONGO_DATABASE_NAME)
#DEFAULT_CONNECTION_NAME = connect(MONGO_DATABASE_NAME, username='nbq', password='123456')

DEBUG = True

TEMPLATE_DEBUG = True

STATIC_URL = '/static/'

#STATIC_ROOT = BASE_DIR.child('static')

STATICFILES_DIRS = (BASE_DIR.child('static'),)



MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR.child('media')


STATICFILES_FINDERS = (
      'django.contrib.staticfiles.finders.FileSystemFinder',
      'django.contrib.staticfiles.finders.AppDirectoriesFinder',
      #'djangular.finders.NamespacedAngularAppDirectoriesFinder'
  )

APPEND_SLASH=False

