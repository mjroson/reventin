from unipath import Path

BASE_DIR = Path(__file__).ancestor(3)

SECRET_KEY = 'f!7*4n9di#&04x8fin#jb1-=+_9eo5mq2m%v1g%-5=3+^-p5_q'

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/'

DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)


THIRD_PARTY_APPS = (
    'mongoengine.django.mongo_auth',
    'rest_framework',
    'django_extensions',
    'corsheaders',
    'bootstrap3',
    'mongodbforms',
    'jsonify',
    'djrill',
    'apps.djmail',
)

# Local aplication
LOCAL_APPS = (
    'apps.company',
    'apps.user',
    'apps.product',
    'apps.permission',
    'apps.utils',
    'apps.iecsv',
    'apps.price_list',
    'apps.order',
    'apps.dispatch',
    'apps.catalog'
)

# All aplication
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    #'corsheaders.middleware.CorsPostCsrfMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.permission.middleware.ValidUserdMiddleware'
    #'middleware.middleware.WhodidMiddleware',
)

ROOT_URLCONF = 'reventin.urls'

WSGI_APPLICATION = 'reventin.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}

AUTHENTICATION_BACKENDS = (
    'mongoengine.django.auth.MongoEngineBackend',
    'django.contrib.auth.backends.ModelBackend'
)

AUTH_USER_MODEL = 'mongo_auth.MongoUser'

# defined custom model user
MONGOENGINE_USER_DOCUMENT = 'apps.user.models.CustomUser'


SESSION_ENGINE = 'mongoengine.django.sessions'
SESSION_SERIALIZER = 'mongoengine.django.sessions.BSONSerializer'


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# defined folder of templates
TEMPLATE_DIRS = (
    BASE_DIR.child('templates'),
)



# Config mandril email
#MANDRILL_API_KEY = "t8e53-FRhNlanPDxvf8wTA"
#DJMAIL_REAL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"

# Configuration email
# DEFAULT_FROM_EMAIL = "testnubiquo@gmail.com"
# EMAIL_HOST = 'smtp.mandrillapp.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
# EMAIL_HOST_PASSWORD = 'Mariano2015$_'
# EMAIL_USE_TLS = True

EMAIL_BACKEND="apps.djmail.backends.async.EmailBackend"
DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"

DEFAULT_FROM_EMAIL = 'testnubiquo@gmail.com'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = 'nubiquo1234567890'
EMAIL_USE_TLS = True


#EMAIL_BACKEND="djmail.backends.async.EmailBackend"
#DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"
# Configure celery

#CELERY_IMPORTS = ["djcelery_email.tasks"]


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


#Permite todos los dominios
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_CREDENTIALS = True


