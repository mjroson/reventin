from django.db.models import signals
from django.utils.functional import curry




# def process_view(self, request, view, *args, **kwargs):


class WhodidMiddleware(object):

    def mark_company(user, sender, instance, **kwargs):
        if hasattr(instance, 'company'):
            instance.company = user.company.id
            #print(instance)


    def process_request(self, request):
        if not request.method in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            if hasattr(request, 'user') and request.user.is_authenticated():
                user = request.user
                #request.POST['company'] = str(user.company.id)
            else:
                user = None

            mark_wooded = curry(self.mark_company, user)
            signals.pre_save.connect(mark_wooded, dispatch_uid = (self.__class__, request,), weak = False)

    def process_response(self, request, response):
        signals.pre_save.disconnect(dispatch_uid=(self.__class__, request,))
        return response
