from bson.objectid import ObjectId

from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.views.generic import View, CreateView
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from rest_framework_mongoengine.generics import ListAPIView, CreateAPIView, UpdateAPIView
from rest_framework_mongoengine.viewsets import ModelViewSet

from apps.permission.views import has_permission
from apps.utils.views import ObjectGenericView

from .forms import LoginForm, CreateUserForm
from .models import CustomUser, Recomendation, UserType, UserTypeLog
from .serializers import UserTypeSerializer, UserSerializer, RecomendationSerializer, UserAuthenticationSerializer


class UserTypeModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = UserTypeSerializer
    model = UserType
    queryset = UserType.objects.all()

    def update(self, request, *args, **kwargs):
        # TODO: Make usertype log with signals
        if kwargs.get("id", None):
            user_type_log = UserTypeLog()
            user_type_log.user_type =  ObjectId(kwargs['id'])
            user_type_log.save()
        return super(UserTypeModelViewSet, self).update(request, *args, **kwargs)


class CustomUserModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = UserSerializer
    model = CustomUser
    queryset = CustomUser.objects.all()

    @has_permission
    def create(self, request, *args, **kwargs):
        if request.DATA.get('user_father', '') == '':
            request.DATA['user_father'] = str(request.user.id)

        return super(CustomUserModelViewSet, self).create(request, *args, **kwargs)

    @has_permission
    def update(self, request, *args, **kwargs):
        return super(CustomUserModelViewSet, self).update(request, *args, **kwargs)

    @has_permission
    def list(self, request, *args, **kwargs):
        #self.queryset = self.queryset.objects.filter(hierarchy_user__contains=request.user)
        return super(CustomUserModelViewSet, self).list(request, *args, **kwargs)

    @has_permission
    def retrieve(self, request, *args, **kwargs):
        return super(CustomUserModelViewSet, self).retrieve(request, *args, **kwargs)

    def get_queryset(self):
        # TODO: Why override custom base queryset? (quiet filter company here)
        self.queryset = self.queryset.filter(pk__ne=self.request.user.id)
        return super(CustomUserModelViewSet, self).get_queryset()


class GetUserChildren(ListAPIView):
    serializer_class = UserSerializer

    def get_queryset(self):
        return CustomUser.objects.filter(user_hierarchy__contains = self.request.user).filter(status=1)


@api_view(['GET',])
def is_email_valid(request, *args, **kwargs):
    if request.user and kwargs.get('email'):
        if CustomUser.objects.filter(company=request.user.company, email=kwargs.get('email')).count() > 0:
            return Response({'is_valid': "false", 'message': "Email is exist."})

    return Response({'is_valid': "true", 'message': "Email can used"})


class RecomendationCreateView(CreateAPIView):
    serializer_class = RecomendationSerializer
    model = Recomendation

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, company=self.request.user.company)

    def create(self, request, *args, **kwargs):
        if request.DATA.get('email', None):
            email_data = request.DATA['email']
            # Validated if email exist in company or in recomendation.
            if CustomUser.objects.filter(email=email_data, company=request.user.company).count() > 0 \
                    or Recomendation.objects.filter(email=email_data).count() > 0:
                return Response({"message": "Email ready exists."}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return super(RecomendationCreateView, self).create(request, *args, **kwargs)
        else:
            return Response({"message": "Need email fields"}, status=status.HTTP_400_BAD_REQUEST)


class UserRecomendationCreateView(CreateAPIView):
    serializer_class = UserSerializer

    # TODO: Create custom serializer to only fields, first_name, last_name, birth_date, properties, phones and anddress
    def perform_create(self, serializer):
        recomendation = Recomendation.objects.get(pk=self.kwargs['recomendation'])
        recomendation.is_validate = True
        recomendation.save()
        serializer.save(user_father=recomendation.user, company=recomendation.company, email=recomendation.email, is_staff=True)

    def create(self, request, *args, **kwargs):
        if kwargs.get('recomendation', None):
            if Recomendation.objects.get(pk= kwargs['recomendation']).is_validate == False:
                return super(UserRecomendationCreateView, self).create(request, *args, **kwargs)

        return Response({"message": "Invalid url"}, status=status.HTTP_400_BAD_REQUEST)



class LoginView(FormView):
    template_name = 'user/login.html'
    form_class = LoginForm

    @method_decorator(csrf_protect)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        login(self.request, user)
        #login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        return '/'


class LogoutView(View):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LogoutView, self).dispatch(*args, **kwargs)

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/login/')


@api_view(['GET', 'POST', ])
def upload_img_profile(request, *args, **kwargs):
    """
        responde a la peticion del formulario de datos requeridos para poder usar el sistema
    """
    if request.method == 'POST':
        if request.FILES.get('img'):
            request.user.image = request.FILES.get('img')
            request.user.save()

            return Response({
                                'status': 'Ok request',
                                'message': 'Set data ok.',
                                'url_img': '/images/' + str(request.user.image._id)
                            }, status=status.HTTP_200_OK)

# class LoginView(views.APIView):
#
#     @method_decorator(ensure_csrf_cookie)
#     def post(self, request, format=None):
#         email = request.DATA['email']
#         password = request.DATA['password']
#
#         account = authenticate(username=email, password=password)
#
#         if account is not None:
#             if account.is_active:
#                 login(request, account)
#                 serialized = UserAuthenticationSerializer(account)
#                 return Response(serialized.data)
#             else:
#                 return Response({
#                     'status': 'Unauthorized',
#                     'message': 'This account has been disabled.'
#                 }, status=status.HTTP_401_UNAUTHORIZED)
#         else:
#             return Response({
#                 'status': 'Unauthorized',
#                 'message': 'Username/password combination invalid.'
#             }, status=status.HTTP_401_UNAUTHORIZED)
#
#
#     #@method_decorator(ensure_csrf_cookie)
#     #def get(self, *args, **kwargs):
#     #    return Response({
#     #                'status': 'CSRF',
#     #                'message': 'This csrf you use.'
#     #            }, status=status.HTTP_200_OK)
#
#
# class LogoutView(views.APIView):
#     permission_classes = (permissions.IsAuthenticated,)
#
#     def post(self, request, format=None):
#         logout(request)
#
#         return Response({}, status=status.HTTP_204_NO_CONTENT)

class CreateUserByRecomendationView(CreateView):
    model = CustomUser
    form_class = CreateUserForm
    template_name = 'user/create.html'
    recomendation = Recomendation()

    def dispatch(self, *args, **kwargs):
        return super(CreateUserByRecomendationView,
                     self).dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        try:
            self.recomendation = Recomendation.objects.get(pk=self.kwargs['key'])
            if self.recomendation and self.recomendation.is_validate == False:
                self.object = None
                form_class = self.get_form_class()
                form = self.get_form(form_class)
                return self.render_to_response(
                    self.get_context_data(form=form))
            else:
               messages.success(self.request, 'El codigo de verificacion esta expirado.')
            return HttpResponseRedirect('/login/')
        except Recomendation.DoesNotExist:
            messages.success(self.request, 'El codigo de verificacion es incorrecto.')
            return HttpResponseRedirect('/login/')

    def form_valid(self, form, *args, **kwargs):
        self.recomendation = Recomendation.objects.get(pk=self.kwargs['key'])
        user = CustomUser()
        user.is_company = False
        user.company = self.recomendation.company
        user.username = self.recomendation.email
        user.email = self.recomendation.email
        user.user_father = self.recomendation.user
        user.first_name = form.cleaned_data.get('first_name')
        user.last_name = form.cleaned_data.get('last_name')
        user.is_staff = True
        user.set_password(form.cleaned_data.get('password1'))
        user.save()
        self.recomendation.is_validate = True
        self.recomendation.save()
        messages.success(
            self.request,
            'Se ah registrado con exito, puede acceder al sistema con los datos q ingreso.')
        return HttpResponseRedirect('/login/')


class ProfileUpdateApiView(UpdateAPIView):
    serializer_class = UserAuthenticationSerializer

    def get_queryset(self):
        return CustomUser.objects.get(pk=self.request.user.id)

    def get_object(self):
        return CustomUser.objects.get(pk=self.request.user.id)