from rest_framework import serializers
from rest_framework_mongoengine.serializers import DocumentSerializer

import json
from apps.permission.serializers import PermissionGroupSerializer

from .models import CustomUser, UserType, Recomendation


class RecomendationSerializer(DocumentSerializer):

    class Meta:
        model = Recomendation
        depth = 1


class UserTypeSerializer(DocumentSerializer):

    class Meta:
        model = UserType
        extra_kwargs = {'description': {'allow_blank': True}}
        depth = 1


class UserSerializer(DocumentSerializer):
    email = serializers.EmailField(required=False, allow_blank=True)

    class Meta:
        model = CustomUser
        fields = ('id', 'first_name', 'last_name', 'user_type', 'email', 'user_type', 'is_staff', 'user_father', 'birth_date', 'addresses', 'phones', 'properties')
        read_only_fields = ('id',)
        write_only_fields = ('user_father',)
        #unique_together = ('company', 'email') # the company save in model
        extra_kwargs = {'user_type': {'required': True}}
        depth = 2

    def validate(self, attrs):
        if attrs.get('is_staff', False):
            if attrs.get('email', '') == '':
                raise serializers.ValidationError("If user is staff, email fields is required.")
        return attrs

    def create(self, validated_data):
        if validated_data.get('is_staff', False):
            request = self.context.get('request', None)
            if request is not None:
                if request.user:
                    # TODO : Validated if email exist in company in models and here
                    if CustomUser.objects.filter(company=request.user.company, email=validated_data['email']).count() > 0:
                        raise serializers.ValidationError("The email is exist in company")

        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if validated_data.get('is_staff', False):
            request = self.context.get('request', None)
            if request is not None:
                if request.user:
                    # TODO : Validated if email exist in company in models and here
                    if CustomUser.objects.filter(company=request.user.company, email=validated_data['email'], id__ne=instance.id).count() > 0:
                        raise serializers.ValidationError("The email is exist in company")

        return super(UserSerializer, self).update(instance, validated_data)

    def get_user_type(self, obj):
        return str(obj.user_type.id)


class JSONSerializerField(serializers.Field):

    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value

class UserAuthenticationSerializer(DocumentSerializer):
    """
        Serializer used to authentication.
    """
    img = serializers.SerializerMethodField(read_only=True)
    company_name = serializers.SerializerMethodField(read_only=True)
    has_children = serializers.SerializerMethodField(read_only=True)
    permissions = serializers.SerializerMethodField(read_only=True) #JSONSerializerField()

    class Meta:
        model = CustomUser
        depth = 2
        fields = ('first_name',
                  'last_name',
                  'email',
                  'phones',
                  'addresses',
                  'img',
                  'company_name',
                  'is_company',
                  'id',
                  'has_children',
                  'birth_date',
                  'permissions')
        read_only_fields = ('first_name', 'last_name', 'is_company', 'id', 'email' )

    def get_first_name(self, obj):
        if obj.first_name and obj.first_name != '':
            return obj.first_name
        else:
            return ""

    def get_last_name(self, obj):
        if obj.last_name and obj.last_name != '':
            return obj.last_name
        else:
            return ""

    def get_company_name(self, obj):
        return obj.company.company_name

    def get_img(self, obj):
        try:
            return '/images/' + str(obj.image._id)
        except:
            return "/static/image/profile.jpg"

    def get_has_children(self, obj):
        if CustomUser.objects.filter(user_hierarchy__contains= obj.id).count() > 0:
            return True
        else:
            return False


    def get_permissions(self, obj):
        if obj.is_company == True:
            return False
        else:
            return PermissionGroupSerializer(instance=obj.user_type.permission_group).data.get('permissions')