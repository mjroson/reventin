from django.utils.timezone import now as datetime_now
import random
import string
from mongoengine.django.auth import User
from mongoengine import Document, EmbeddedDocument, fields
from django.core.mail import EmailMultiAlternatives
from mongoengine import signals, ValidationError


class UserType(Document):
    """
        Probando
    """
    name = fields.StringField(max_length=200)
    description = fields.StringField(max_length=200)
    comision = fields.FloatField(default=0)
    price_list = fields.ReferenceField("PriceList")
    permission_group = fields.ReferenceField("PermissionGroup")
    company = fields.ReferenceField("Company")
    status = fields.IntField(default=1)

    def update(self, *args, **kwargs):
        return super(UserType, self).update(*args, **kwargs)


class UserTypeLog(Document):
    name = fields.StringField(max_length=200)
    user_type = fields.ReferenceField("UserType")
    comision = fields.FloatField(default=0)
    date_update = fields.DateTimeField()

    def save(self, *args, **kwargs):
        user_type = UserType.objects.get(pk=self.user_type)
        self.user_type = user_type
        self.date_update = datetime_now()
        self.comision = user_type.comision
        self.name = user_type.name
        super(UserTypeLog, self).save(*args, **kwargs)


class Phone(EmbeddedDocument):

    """
        Class phones
    """
    type_phone = fields.StringField(max_length=20)
    number = fields.IntField(min_value=0)

    def __str__(self):
        return self.type_phone + ": " + str(self.number)

    class Meta:
        verbose_name = "Phone"
        verbose_name_plural = "Phones"


class Address(EmbeddedDocument):
    street = fields.StringField(max_length=255)
    number = fields.IntField(min_value=0)
    city = fields.ReferenceField("City")
    default = fields.BooleanField()

    class Meta:
        verbose_name = "address"
        vervose_name_plural = "addresses"


class UserProperty(EmbeddedDocument):
    attr_id = fields.ObjectIdField(required=True)
    value = fields.StringField(max_length=255)
    name = fields.StringField(max_length=100)

    class Meta:
        allow_inheritance = False


class CustomUser(User):
    """Class for customer user, extends of user mongoengine,
        and rewrite some attribute.
    """
    username = fields.StringField(max_length=50)

    first_name = fields.StringField(max_length=30)

    last_name = fields.StringField(max_length=30)
    email = fields.StringField(required=False)

    is_company = fields.BooleanField(default=False)
    company = fields.ReferenceField("Company")
    sms = fields.StringField(max_length=200)
    user_father = fields.ReferenceField("self")
    user_hierarchy = fields.ListField(fields.ReferenceField("self"))
    birth_date = fields.StringField(max_length=255)
    phones = fields.ListField(fields.EmbeddedDocumentField(Phone))
    addresses = fields.ListField(fields.EmbeddedDocumentField(Address))
    user_type = fields.ReferenceField('UserType')
    image = fields.ImageField(collection_name='images')
    properties = fields.ListField(fields.EmbeddedDocumentField(UserProperty))
    status = fields.IntField(default=1)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name']

    def create_user_company(username, password, company, email):
        """
            Create user inactive when register company.
        """
        if email is not None:
            try:
                email_name, domain_part = email.strip().split('@', 1)
            except ValueError:
                pass
            else:
                email = '@'.join([email_name, domain_part.lower()])

        user = CustomUser()
        user.username = username
        user.email = email
        user.is_active = False
        user.is_superuser = False
        user.is_staff = True
        user.company = company
        user.is_company = True
        user.set_password(password)

        return user


    def __str__(self):
        return self.username


    def clean(self):
        # TODO: Email is to StringFields because cant validated to null when was EmailField
        if self.is_staff:
            try:
                email_name, domain_part = self.email.strip().split('@', 1)
            except ValueError:
                msg = 'If user is staff email is valid and required.'
                raise ValidationError(msg)
            else:
                self.email = '@'.join([email_name, domain_part.lower()])



    def save(self, *args, **kwargs):
        # Validated : If staff is False then clean email
        if not self.is_staff:
            self.email = ''

        # If email is valid set username
        if self.email and self.email != '':
            self.username = self.email

        if self.user_father:
            hierarchy = []
            hierarchy = self.user_father.user_hierarchy
            hierarchy.append(self.user_father)
            self.user_hierarchy = hierarchy

        return super(CustomUser, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'


def send_email_with_password(cls, document , **kwargs):

    if 'created' in kwargs:
        if kwargs['created']:
            if document.email != '' and document.is_staff == True:
                password = ''.join(random.choice(string.ascii_uppercase + string.digits) for n in range(20))
                document.set_password(password)
                html_content = 'Bienvenido a Reventin, su password es ' + password
                msg = EmailMultiAlternatives('Invitacion al sistema Reventin',
                                             html_content,
                                             'testnubiquo@gmail.com',
                                             [document.email])
                msg.attach_alternative(html_content, 'text/html')
                msg.send()


signals.post_save.connect(send_email_with_password, sender=CustomUser)

def send_email_recomendation(cls, document , **kwargs):

    html_content = 'Bienvenido Sr/a: \n  \
             Visite http://localhost:8000/register-by-email/%s/ \
             para poder registrarse.' % (document.id)

    msg = EmailMultiAlternatives('Invitacion al sistema Nubiquo',
                                              html_content,
                                              'testnubiquo@gmail.com',
                                              [document.email])
    msg.attach_alternative(html_content, 'text/html')
    msg.send()


class Recomendation(Document):
    email = fields.EmailField(required=True)
    user = fields.ReferenceField(CustomUser)
    #key_verification = fields.StringField()
    date_send = fields.DateTimeField()
    is_validate = fields.BooleanField(default=False)
    company = fields.ReferenceField("Company")

    def save(self, *args, **kwargs):
        self.date_send = datetime_now()
        #self.key_verification = ''.join(random.choice(
        #    string.ascii_uppercase + string.digits) for n in range(20))
        return super(Recomendation, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Recomendation'
        verbose_name_plural = 'Recomendations'



signals.post_save.connect(send_email_recomendation, sender=Recomendation)