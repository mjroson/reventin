from django.contrib.auth.forms import AuthenticationForm

from mongodbforms import forms, DocumentForm

from .models import CustomUser


class LoginForm(AuthenticationForm):
    forms.EmailField(widget=forms.TextInput(
        attrs={'maxlength': 100, 'class': 'form-control',
               'placeholder': "Username"}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'maxlength': 30, 'class': 'form-control',
               'placeholder': "Password"}))

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        if self.errors:
            for f_name in self.fields:
                classes = self.fields[f_name].widget.attrs.get('class', '')
                classes += ' has-error'
                self.fields[f_name].widget.attrs['class'] = classes


class CreateUserForm(DocumentForm):

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'maxlength': 60, 'class': 'form-control',
               'placeholder': "Nombres"}))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'maxlength': 60, 'class': 'form-control',
               'placeholder': "Apellido"}))


    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'maxlength': 30, 'class': 'form-control',
               'placeholder': "Contraseña"}))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'maxlength': 30, 'class': 'form-control',
               'placeholder': "Repetir contraseña"}))

    class Meta:
        document = CustomUser
        fields = ['first_name', 'last_name', 'birth_date', 'password1', 'password2']


    def clean_password_check(self):
        """
            Verifica que las contraseñas cohincidan
        """
        if 'password' in self.cleaned_data:
            password = self.cleaned_data['password']
            password_check = self.cleaned_data['password_check']
            if password == password_check:
                return password_check
            raise forms.ValidationError('Claves no coinciden,')


    def save(self, commit=True):
        user = super(CreateUserForm, self).save(commit=False)
        user.username = self.cleaned_data['email']
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        password = self.cleaned_data["password1"]
        self.instance = user.set_password(password)
        return self.instance