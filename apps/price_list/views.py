from rest_framework_mongoengine.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

from bson.objectid import ObjectId

from .models import PriceList, PriceListDetail
from .serializers import PriceListSerializer, PriceListDetailSerializer

from apps.product.models import Product
from apps.product.serializers import ProductSerializer

from apps.utils.views import ObjectGenericView

from apps.permission.views import has_permission


class PriceListModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = PriceListSerializer
    model = PriceList
    queryset = PriceList.objects.all()

    @has_permission
    def list(self, request, *args, **kwargs):
        return super(PriceListModelViewSet, self).list(request, *args, **kwargs)

    @has_permission
    def retrieve(self, request, *args, **kwargs):
        return super(PriceListModelViewSet, self).retrieve(request, *args, **kwargs)

    @has_permission
    def create(self, request, *args, **kwargs):
        return super(PriceListModelViewSet, self).create(request, *args, **kwargs)

    @has_permission
    def update(self, request, *args, **kwargs):
        return super(PriceListModelViewSet, self).update(request, *args, **kwargs)