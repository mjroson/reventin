from rest_framework_mongoengine import serializers

from .models import PriceList, PriceListDetail


class PriceListSerializer(serializers.DocumentSerializer):

    class Meta:
        model = PriceList
        depth = 1
        fields = ('id','name', 'description', 'date', 'status', 'discount')
        read_only_fields = ('id','status', 'date')


class PriceListDetailSerializer(serializers.DocumentSerializer):

    class Meta:
        model = PriceListDetail
        depth = 1

