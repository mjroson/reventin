from mongoengine import Document, EmbeddedDocument, \
    EmbeddedDocumentField, StringField, ImageField, ListField, \
    ReferenceField, FloatField, QuerySet, IntField, DateTimeField, QuerySetManager, post_save
from django.utils.timezone import now as datetime_now
from mongoengine import signals

from apps.product.models import Product



class PriceListManager(QuerySet):
    def actives(self):
        return self.filter(status=1)


class PriceList(Document):
    name = StringField(max_length=200)
    company = ReferenceField('Company')
    description = StringField(max_length=255)
    date = DateTimeField(required=True)
    status = IntField(default=1) # 1 = Active , 0 = Inactive
    discount = FloatField(default=0)
    #base_list = ReferenceField('PriceList')

    meta = {'queryset_class': PriceListManager}

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.date = datetime_now()
        return super(PriceList, self).save(*args, **kwargs)

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        products = Product.objects.filter(company=document.company, status=1)
        for product in products:
            price_list_detail = PriceListDetail()
            price_list_detail.price_list = document
            price_list_detail.product = product
            price_list_detail.company = product.company
            price_list_detail.product_name = product.name
            price_list_detail.product_sku = product.sku
            price_list_detail.product_price = product.sale_price
            price_list_detail.custom_price = product.sale_price - (document.discount * product.sale_price / 100 )
            price_list_detail.save()

# Set signal to post execute function save of PriceList.
signals.post_save.connect(PriceList.post_save, sender=PriceList)


class PriceListDetail(Document):
    price_list = ReferenceField("PriceList")

    product = ReferenceField('Product')
    product_sku = StringField(max_length=80, required=True)
    product_name = StringField(max_length=100, required=True)
    product_price = FloatField()
    custom_price = FloatField()

    status = IntField(default=1) # 1 = Active , 0 = Inactive
    company = ReferenceField('Company')
    #
    # def save(self, *args, **kwargs):
    #     product = Product.objects.get(pk=self.product)
    #     if product:
    #         self.product = product
    #         self.product_sku = product.sku
    #         self.product_name = product.name
    #         self.product_price = product.sale_price
    #
    #     return super(PriceListDetail, self).save(*args, **kwargs)

