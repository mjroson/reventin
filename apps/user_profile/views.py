from django.http import HttpResponseRedirect, HttpResponse

from django.utils.decorators import method_decorator

from django.views.generic import CreateView, DetailView, UpdateView

from django.contrib import messages
from django.contrib.auth.decorators import login_required

from mongoengine.django.shortcuts import get_list_or_404

from .models import UserProfile, City, UserProperty
from .forms import ProfileCreateForm, InlineAddressForm, InlinePhoneForm, AddressInLineForm

#from bson import ObjectId


class UserProfileCreateView(CreateView):
    """
        Vista de creacion de un perfil, donde se agregan dos
        formularios, uno para agregar N cantidad de telefonos
        y otro para agregar N cantidad de direcciones
    """
    model = UserProfile
    form_class = ProfileCreateForm
    template_name = 'user_profile/create.html'
    success_url = '/account/profile'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserProfileCreateView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        phone_form = InlinePhoneForm(prefix='phone')
        address_form = InlineAddressForm(prefix='address')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  phone_form=phone_form,
                                  address_form=address_form))

    def post(self, request, *args, **kwargs):
        self.object = UserProfile()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        #form.instance.birth_date = request.POST['birth_date']
        #form = ProfileCreateForm(request.POST, request.FILES)
        form.instance.properties = []
        #form.instance.user = request.user
        for attr in request.user.company.cfg_attr_user:
            prop = UserProperty()
            prop.property_name = attr.attr_name
            key = 'frm-' + attr.attr_name
            prop.value = request.POST.get(key, '')

            form.instance.properties.append(prop)

        phone_form = InlinePhoneForm(self.request.POST)
        address_form = InlineAddressForm(self.request.POST)
        if form.is_valid() and phone_form.is_valid() and address_form.is_valid():
            return self.form_valid(form, phone_form, address_form)
        else:
            return self.form_invalid(form, phone_form, address_form)

    def form_valid(self, form, phone_form, address_form):
        self.object = form.save(commit=False)

        phone_form.instance = phone_form
        address_form.instance = self.object

        for form_a in address_form:
            self.object.addresses.append(form_a.instance)

        for form_p in phone_form:
            self.object.phones.append(form_p.instance)

        self.object.birth_date = form.cleaned_data.get('birth_date')
        self.object.user = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, phone_form, address_form):
        """
            Si el formulario es invalido, devuelve los 3 formularios
        """
        return self.render_to_response(
            self.get_context_data(form=form,
                                  phone_form=phone_form,
                                  address_form=address_form))

    """
    class UserProfileUpdateView(UpdateView):
        model = UserProfile
        form_class = ProfileCreateForm
        context_object_name = "customuser"
        template_name = 'user_profile/edit.html'
        success_url = '/accounts/profile/'

        @method_decorator(login_required)
        def dispatch(self, *args, **kwargs):
            return super(UserProfileUpdateView, self).dispatch(*args, **kwargs)

        def get_context_data(self, **kwargs):
            context = super(UserProfileUpdateView, self).get_context_data(**kwargs)
            context['form'] = self.get_form(self.form_class)

            if self.request.method == 'POST':
                context['phone_form'] = InlinePhoneForm(
                    self.request.POST, self.request.FILES, instance=self.object)
                context['address_form'] = InlineAddressForm(
                    self.request.POST, self.request.FILES, instance=self.object)
            else:
                context['phone_form'] = InlinePhoneForm(
                    instance=self.get_object())
                context['address_form'] = InlineAddressForm(instance=self.get_object())
            return context


                def post(self, request, *args, **kwargs):
                    self.object = None
                    form_class = self.get_form_class()
                    form = self.get_form(form_class)
                    form.instance.user = request.user
                    phone_form = InlinePhoneForm(self.request.POST)
                    address_form = InlineAddressForm(self.request.POST)
                    if (phone_form.is_valid() and address_form.is_valid()):
                        return self.form_valid(form, phone_form, address_form)
                    else:
                        return self.form_invalid(form, phone_form, address_form)

        def form_valid(self, form):
            "
            self.object = form.save(commit=False)

            phone_form.instance = phone_form
            address_form.instance = self.object
            "
            context = self.get_context_data()
            form = context['form']
            phone_form = context['phone_form']
            address_form = context['address_form']
            form.save(commit=False)
            # le setea la lista de direcciones
            for form_a in address_form:
                form.instance.addresses.append(form_a.instance)

            for form_p in phone_form:
                form.instance.phones.append(form_p.instance)

            asdasd = self.get_object()
            #form.instance.birth_date = form.cleaned_data.get('birth_date')
            #form.instance.user = self.request.user
            messages.success(self.request, "Profile updated.")

            form.instance.save()
            #return super(UserProfileUpdateView, self).form_valid(form)
            return HttpResponseRedirect(self.get_success_url())

        def form_invalid(self, form, phone_form, address_form):

                #Si el formulario es invalido, devuelve los 3 formularios

            return self.render_to_response(
                self.get_context_data(form=form,
                                      phone_form=phone_form,
                                      address_form=address_form))

        def get_object(self):
            return UserProfile.objects.get(user=self.request.user)

    """


class UserProfileUpdateView(UpdateView):
    model = UserProfile
    form_class = ProfileCreateForm
    context_object_name = "customuser"
    template_name = 'user_profile/edit.html'
    success_url = '/account/profile'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserProfileUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self):
        return UserProfile.objects.get(user=self.request.user)

    def get(self, request, *args, **kwargs):
        return super(UserProfileUpdateView, self).get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        form.instance.properties = []
        for attr in request.user.company.cfg_attr_user:
            prop = UserProperty()
            prop.property_name = attr.attr_name
            key = 'frm-' + attr.attr_name
            prop.value = request.POST.get(key, '')

            form.instance.properties.append(prop)

        #form.instance.user = request.user
        form.is_valid()
        profile = self.get_object()
        form.instance.id = profile.id
        form.instance.addresses = profile.addresses
        form.instance.phones = profile.phones
        profile = form.instance
        profile.save()
        messages.success(request, 'El perfil fue modificado con exito')
        return HttpResponseRedirect(self.get_success_url())


class UserProfileDetailView(DetailView):
    model = UserProfile
    context_object_name = "userprofile"
    template_name = 'user_profile/profile.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self):
        # Comprueba si hay un perfile creado con el user logeado,
        # sino, devuelve Null
        try:
            user_profile = UserProfile.objects.get(user=self.request.user)
            return user_profile
        except:
            return None

    def get(self, request, *args, **kwargs):
        # Si el get object es null, redirige al UserProfileCrateView
        # para que cree el perfil.
        self.object = self.get_object()
        if self.object:
            context = self.get_context_data(object=self.object)
            return self.render_to_response(context)
        else:
            messages.warning(
                self.request, "Debe crear un perfil para \
                el correcto uso del sistema")
            return HttpResponseRedirect('/account/create')

    def get_queryset(self):
        return get_list_or_404(UserProfile,
                               user=self.request.user)


"""
def city_auto_complete(request):

        Toma de la url el parametro term , y busca ciudades que empiecen con
        los caracteres indicado en dicho parametro, genera un array de json,
        y lo devuelve

    q = request.REQUEST['term']

    if q:
        cities = City.objects.filter(name_city__istartswith=q)
    else:
        cities = []

    cities_list = []
    for c in cities:
        value = '%s' % (c.__str__())
        c_dict = {
            'id': c.id.__str__(), 'label': value, 'value': value}
        cities_list.append(c_dict)

    return HttpResponse(simplejson.dumps(cities_list))
 """