from mongodbforms.documents import embeddedformset_factory,\
    BaseInlineDocumentFormSet, DocumentForm, inlineformset_factory, EmbeddedDocumentForm, EmbeddedDocumentFormSet

from mongodbforms import forms

from .models import UserProfile, Phone, Address

OPTIONS = (
    ("TEL", "Telefono"),
    ("CEL", "Celular"),
    ("FAX", "Fax"),
)


class PhoneInLineForm(BaseInlineDocumentFormSet):

    def __init__(self, *args, **kwargs):
        super(PhoneInLineForm, self).__init__(*args, **kwargs)

    def add_fields(self, form, index):
        super(PhoneInLineForm, self).add_fields(form, index)
        form.fields['type_phone'] = forms.ChoiceField(choices=OPTIONS,
                                                      label="Tipo")
        form.fields['number'] = forms.CharField(label="Numero")

    class Meta:
        document = Phone


class AddressInLineForm(EmbeddedDocumentForm):

    def add_fields(self, form, index):
        super(AddressInLineForm, self).add_fields(form, index)
        #form.fields['city_select'] = forms.CharField(max_length=100, label="Ciudad")

    def __init__(self, *args, **kwargs):
        super(AddressInLineForm, self).__init__(*args, **kwargs)

    class Meta:
        document = Address
        embedded_field_name = 'addresses'


class ProfileCreateForm(DocumentForm):

    class Meta:
        document = UserProfile
        exclude = ('user')
        widgets = {'birth_date': forms.TextInput(attrs={'type': 'date'})}

    def clean_birth_date(self):
        return self.cleaned_data['birth_date']

InlinePhoneForm = inlineformset_factory(
    Phone, ProfileCreateForm, PhoneInLineForm)

InlineAddressForm = inlineformset_factory(
    Address, ProfileCreateForm)

"""
InlinePhoneForm = embeddedformset_factory(
    Phone, UserProfile, ProfileCreateForm, formset=PhoneInLineForm)

InlineAddressForm = embeddedformset_factory(
    Address, parent_document=UserProfile, form=AddressInLineForm)

InlineAddressForm = embeddedformset_factory(
    Address, parent_document=UserProfile, formset=AddressInLineForm)

InlineAddressForm = embeddedformset_factory(document=UserProfile, parent_document=UserProfile, form=AddressInLineForm)

"""
