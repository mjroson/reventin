from mongoengine import Document
from mongoengine import StringField, ReferenceField


class Country(Document):
    name_country = StringField(max_length=200)

    def __str__(self):
        return self.name_country


class State(Document):
    name_state = StringField(max_length=200)
    country = ReferenceField(Country, dbref=True)

    def __str__(self):
        return self.name_state


class City(Document):
    name_city = StringField(max_length=200)
    state = ReferenceField(State, dbref=True)

    def __str__(self):
        return self.name_city + ", " + self.state.name_state \
            + ", " + self.state.country.name_country
