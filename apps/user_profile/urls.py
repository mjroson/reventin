from django.conf.urls import patterns, url

from .views import UserProfileCreateView, UserProfileDetailView, \
    UserProfileUpdateView

urlpatterns = patterns('',
                       url(r'^profile$', UserProfileDetailView.as_view(),
                           name='profile'),
                       url(r'^create/$',
                           UserProfileCreateView.as_view(),
                           name='profile-create'),
                       url(r'^edit/$',
                           UserProfileUpdateView.as_view(),
                           name='profile-update'),
                       url(r'^city/autocomplete/$',
                           'apps.user_profile.views.city_auto_complete',
                           name='city_auto_complete'),
                       )
