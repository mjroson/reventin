from mongoengine import Document, DateTimeField
from mongoengine import StringField, IntField, ListField
from mongoengine import EmbeddedDocumentField, ReferenceField
from mongoengine import EmbeddedDocument, BooleanField, ImageField

from apps.user.models import CustomUser
from .test_model import City


class Phone(EmbeddedDocument):

    """
        Class phones
    """
    type_phone = StringField(max_length=20)
    number = IntField(min_value=0)

    def __str__(self):
        return self.type_phone + ": " + str(self.number)

    class Meta:
        verbose_name = "Phone"
        verbose_name_plural = "Phones"


class Address(EmbeddedDocument):
    street = StringField(max_length=255)
    number = IntField(min_value=0)
    city = ReferenceField(City, dbref=True)
    default = BooleanField()

    class Meta:
        verbose_name = "address"
        vervose_name_plural = "addresses"


class UserProperty(EmbeddedDocument):
    property_name = StringField(max_length=200)
    value = StringField(max_length=255)


class UserProfile(Document):
    birth_date = StringField(max_length=255)
    phones = ListField(EmbeddedDocumentField(Phone))
    addresses = ListField(EmbeddedDocumentField(Address))
    user = ReferenceField(CustomUser, unique=True)
    image = ImageField(collection_name='images')
    properties = ListField(EmbeddedDocumentField(UserProperty))