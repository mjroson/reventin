import json

from bson.objectid import ObjectId

from django.http import Http404, HttpResponse

from gridfs import GridFS, NoFile

from mongoengine.connection import get_db

from rest_framework import status
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from rest_framework.response import Response

from rest_framework_mongoengine.viewsets import ModelViewSet

from apps.utils.views import ObjectGenericView
from apps.permission.views import has_permission

from .models import Category, Product, ProductImage
from .serializers import CategorySerializer, ProductSerializer, ProductListSerializer


def serve_file(request, file_id):
    db = get_db()
    fs = GridFS(db)
    try:
        f = fs.get(ObjectId(file_id))
    except NoFile:
        # mongoengine stores images in a separate collection by default
        fs = GridFS(db, collection='images_product')
        try:
            f = fs.get(ObjectId(file_id))
        except NoFile:
            raise Http404

    response = HttpResponse(f.read(), content_type=f.content_type)
    #timestamp = datetime_now()  # time.mktime(gridout.upload_date.timetuple())
    response["Last-Modified"] = f.uploadDate
    # add other header data like etags etc. here
    return response




class CategoriesViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = CategorySerializer
    model = Category
    queryset = Category.objects.all()



class ProductModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = ProductSerializer
    model = Product
    queryset = Product.objects.all()
    parser_classes = (MultiPartParser, FormParser, FileUploadParser )

    @has_permission
    def list(self, request, *args, **kwargs):
        self.serializer_class = ProductListSerializer
        return super(ProductModelViewSet, self).list(request, *args, **kwargs)

    @has_permission
    def retrieve(self, request, *args, **kwargs):
        return super(ProductModelViewSet, self).retrieve(request, *args, **kwargs)

    @has_permission
    def create(self, request, *args, **kwargs):
        product_data = json.loads(request.data['data'])
        #product_data['images'] = []
        print("UPDATE")
        print(40*"=")
        print(product_data.get('product_variations'))
        print(40*"=")

        product_serializer = ProductSerializer(data=product_data)

        if product_serializer.is_valid():
            product_serializer.save()
            product = product_serializer.instance

            images = []
            try:
                # TODO: Whats happen if any images set default or more to one images are default?
                for image_data in product_data['images']:
                    for key, image in request.FILES.items():
                        if image_data['name'] == image.name:
                            product.images.append(ProductImage(image=image, default=image_data.get('default', False)))
                            break

                product.company = request.user.company
                product.save()
            except:
                product.delete()

                return Response({"Error al intentar guardar las imagenes"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            product_serializer.run_validation(product_data)

        return Response(product_serializer.data)

    @has_permission
    def update(self, request, *args, **kwargs):
        product_data = json.loads(request.data['data'])
        #variations_data = product_data.get('product_variations')


        print("UPDATE")
        print(40*"=")
        print(product_data.get('product_variations'))
        print(40*"=")
        print(product_data.get('properties'))

        #product_data['images'] = []
        print(kwargs)
        try:
            old_instance = Product.objects.get(pk=kwargs.get('id'))
            product_serializer = ProductSerializer(data=product_data, instance=old_instance)
        except:
            return Response({"message": "Error al intentar instanciar el producto"}, status=status.HTTP_400_BAD_REQUEST)
        #if len(variations_data) > 0:
        if product_serializer.is_valid():
            product_serializer.save()
            print("EL PRODUCTO SE GUARDO CORRECTAMENTE")
            print(60*"-")
            print(product_serializer.data)
            product = product_serializer.instance

            # if len(variations_data) > 0:
            #     for variation in variations_data:
            #         if variation.get('id'):
            #             for variation_old in product.product_variations:
            #                 if str(variation.get('id')) == str(variation_old.id):
            #                     for property_old in variation_old.properties:
            #                         for property_new in variation.get('properties'):
            #                             if str(property_old.id) == str(property_new.id):
            #                                 property_new = property_old
            #                                 break
            #                     break

            try:
                for image_data in product_data['images']:
                    if not image_data.get('deleted', False) or image_data.get('is_new', False) \
                            and image_data.get('name'):
                        if len(request.FILES) > 0:
                            for key, image in request.FILES.items():
                                if image_data.get('name', '') == image.name:
                                    product.images.append(ProductImage(image=image,
                                                                           default=image_data.get('default', False)))
                                    break
                    else:
                        if image_data.get('deleted'):
                            for image_old in product.images:
                                if str(image_data.name) == str(image_old.name):
                                    product_data.images.pop(image_old)

                product.save()
                return Response(product_serializer.data, status=status.HTTP_200_OK)
            except:
                return Response({"Error al intentar guardar las imagenes"},
                                status=status.HTTP_400_BAD_REQUEST)
            #else:
            #    return Response(product_serializer.data, status=status.HTTP_200_OK)

        return Response({"Error"}, status=status.HTTP_400_BAD_REQUEST)