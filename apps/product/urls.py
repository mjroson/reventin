from django.conf.urls import patterns, url

from apps.product import views


urlpatterns = patterns('',
                       url(r'^images/(?P<file_id>[0-9a-f]{24})/$',
                           'apps.product.views.serve_file',
                           name='file-view'),
                       # # Crear productos
                       # url(r'^create/$',
                       #     views.CreateProductView.as_view(),
                       #     name='create-product'),
                       # # Editar productos
                       # url(r'^edit/(?P<product_key>.+)/$',
                       #     views.UpdateProductView.as_view(),
                       #     name='update-product'),
                       # # Eliminar productos
                       # url(r'^delete/(?P<product_key>.+)/$',
                       #     views.DeleteProductView.as_view(),
                       #     name='delete-product'),
                       # # Lista productos
                       # url(r'^$',
                       #     views.ListProductView.as_view(),
                       #     name='list-product'),
                       # # Detalle productos
                       # url(r'^detail/(?P<product_key>.+)/$',
                       #     views.DetailProductView.as_view(),
                       #     name='detail-product'),
                       # # Crear categorias
                       # url(r'^categories/create/$',
                       #     views.CreateCategoryView.as_view(),
                       #     name='create-category'),
                       # # Editar categorias
                       # url(r'^categories/edit/(?P<category_key>.+)/$',
                       #     views.UpdateCategoryView.as_view(),
                       #     name='update-category'),
                       # # Eliminar categorias
                       # url(r'^categories/delete/(?P<category_key>.+)/$',
                       #     views.DeleteCategoryView.as_view(),
                       #     name='delete-category'),
                       # # Lista categorias
                       # url(r'^categories/$',
                       #     views.ListCategoryView.as_view(),
                       #     name='list-category'),
                       # # Detalle categorias
                       # url(r'^categories/detail/(?P<category_key>.+)/$',
                       #     views.DetailCategoryView.as_view(),
                       #     name='detail-category'),
                       # # Lista categorias
                       # url(r'^categoriess/$',
                       #     views.CategoryListView.as_view(),
                       #     name='list-category-rest'),
                       )
