from rest_framework_mongoengine import serializers

from .models import Category, Product, ProductProperty, ProductVariation, ProductImage


class CategorySerializer(serializers.DocumentSerializer):
    category_father_name = serializers.serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'description',
            'color',
            'category_father',
            'category_hierarchy', 'category_father_name')
        depth = 1
        read_only_fields = ('category_hierarchy',)

    def get_category_father_name(self, obj):
        if obj.category_father:
            return str(obj.category_father.name)
        else:
            return ""

class ProductPropertiSerializer(serializers.DocumentSerializer):

    class Meta:
        model = ProductProperty
        fields = ('id', 'value', 'name')


class ProductVariationSerializer(serializers.DocumentSerializer):

    class Meta:
        model = ProductVariation
        depth = 1

class ProductImageSerializer(serializers.DocumentSerializer):
    image_url = serializers.serializers.SerializerMethodField()
    image = serializers.serializers.SerializerMethodField()

    class Meta:
        model = ProductImage
        fields = ('default', 'image_url', 'image')
        depth = 1

    def get_image_url(self, obj):
        return '/products/images/' + str(obj) + '/'

    def get_image(self, obj):
        return '/products/images/' + str(obj) + '/'


class ProductSerializer(serializers.DocumentSerializer):
    category = CategorySerializer(many=False, read_only=True)
    images = ProductImageSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'sku',
            'name',
            'category',
            'cost_price',
            'sale_price',
            'score',
            'description',
            'images',
            'id',
            'product_variations',
            'properties')
        depth = 3

        read_only_fields = ('id', 'images',)

class ProductListSerializer(serializers.DocumentSerializer):
    #images_url = serializers.serializers.SerializerMethodField()
    images = ProductImageSerializer(many=True, read_only=True)
    # TODO: Fields category limits, need name and id
    category = serializers.serializers.StringRelatedField()

    class Meta:
        model = Product
        fields = (
            'sku',
            'name',
            'category',
            'cost_price',
            'sale_price',
            'score',
            'description',
            'images',
            'id',
            'product_variations',
            'properties')
        depth = 3

    def get_images_url(self, obj):
        images = []
        try:
            for img in obj.images:
                try:
                    images.append('/products/images/' + str(img) + '/')
                except:
                    pass
        except:
            pass
        return images
