from mongoengine import Document, EmbeddedDocument, fields

from bson.objectid import ObjectId


class Category(Document):
    name = fields.StringField(max_length=200)
    company = fields.ReferenceField("Company")
    description = fields.StringField(max_length=255)
    color = fields.StringField(max_length=255)
    category_father = fields.ReferenceField('Category')
    category_hierarchy = fields.ListField(fields.ReferenceField('Category'))
    status = fields.IntField(default=1)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.category_hierarchy = []
        if self.category_father:
            self.category_hierarchy = self.category_father.category_hierarchy
            self.category_hierarchy.append(self.category_father)
        return super(Category, self).save(*args, **kwargs)


class ProductProperty(EmbeddedDocument):
    id = fields.ObjectIdField(required=True)
    value = fields.StringField(max_length=255)
    name = fields.StringField(max_length=100)


class ProductVariation(EmbeddedDocument):
    id = fields.ObjectIdField(primary_key=True)
    name = fields.StringField(max_length=255)
    properties = fields.ListField(
        fields.EmbeddedDocumentField(ProductProperty))
    committed_stock = fields.FloatField(default=0, help_text="float")
    minimun_stock = fields.FloatField(help_text="float")
    real_stock = fields.FloatField(default=0, help_text="float")


class ProductImage(EmbeddedDocument):
    default = fields.BooleanField(default=False)
    image = fields.ImageField(collection_name='images_product')

    def __str__(self):
        return str(self.image._id)


class Product(Document):
    sku = fields.StringField(max_length=80, required=True, help_text="string")
    name = fields.StringField(
        max_length=100,
        required=True,
        help_text="string")
    category = fields.ReferenceField('Category', help_text="string")
    cost_price = fields.FloatField(help_text="float")
    sale_price = fields.FloatField(help_text="float")
    score = fields.IntField(default=0, help_text="int")
    description = fields.StringField(max_length=255, help_text="string")
    product_variations = fields.ListField(
        fields.EmbeddedDocumentField(ProductVariation))
    company = fields.ReferenceField("Company")
    images = fields.ListField(fields.EmbeddedDocumentField(ProductImage))
    status = fields.IntField(default=1)
    properties = fields.ListField(fields.DictField())

    def save(self, *args, **kwargs):
        for variation in self.product_variations:
            # TODO : No siempre tiene que generar un nuevo id
            variation.id = ObjectId()
            variation.name = ''
            #for prop in variation.properties:
            #    variation.name += prop.name + ": " + prop.value + " - "

        return super(Product, self).save(*args, **kwargs)
