import csv
import codecs
import ast

from django.http import HttpResponse

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from apps.product.models import Product, Category, ProductProperty, ProductVariation


@api_view(['POST', ])
def import_csv(request, *args, **kwargs):
    if request.FILES.get('file_csv', None):
        file = csv.reader(codecs.iterdecode(request.FILES['file_csv'], 'utf-8'), delimiter=',' )
        if request.DATA.get('config', None):
            i = 0
            # TODO: Cast to json.load (find in compraloahi)
            config_data = ast.literal_eval(request.DATA['config'])

            product_list = list(file)

            sku_index = '' # Flag to save sku index

            # Initial one because the first row is table head.
            for product_imp in product_list[1:]:

                # Save array to config properties
                config_prop_list = []
                product = Product()
                variations = []
                variation = ProductVariation()

                for config in config_data:
                    print("CONFIG")
                    print(config)
                    if config['name'] != '':
                        # Set flag to index sku for find equals product
                        if config['name'] == 'sku':
                            sku_index = config['column_index']

                        attr_product = ast.literal_eval(config['attr_product'])
                        if attr_product.get('default_value', None):
                            print("PORQUE DIO ERROR?")
                            print(attr_product)
                            product[config['name']] = cast_to_type(attr_product['default_value'], attr_product['type'])
                        else:
                            if attr_product['is_prop'] == "False":
                                if config['name'] == 'category':
                                    try:
                                        #Find category to name by set in product.
                                        category = Category.objects.get(company= request.user.company,
                                                                        name=product_imp[config['column_index']])
                                        # TODO : Determinar que pasa si no encuentra categoria con el mismo nombre
                                        product.category = category
                                    except:
                                        pass
                                else:
                                    product[config['name']] = cast_to_type(product_imp[config['column_index']], attr_product['type'])
                            else:

                                variation.name = "HOLA"
                                config_prop_list.append(config)
                                product_prop = ProductProperty()
                                product_prop.name = attr_product['label']
                                product_prop.id = config['name']
                                product_prop.value = product_imp[config['column_index']]
                                variation.properties.append(product_prop)

                variations.append(variation)

                try:
                    #import pdb; pdb.set_trace()
                    print(30*"====")
                    print(10*"PRODUCT LIST")
                    print(product_list)
                    print(10*"PRODUCT IMPORT")
                    print(product_imp)
                    print(type(product_imp))
                    print(type(product_list))
                    ids = product_list.index(product_imp)
                    print("INDEX")
                    print(ids)
                    plist = product_list[ids:]


                    print(plist)

                    print(20*"SKU INDEX")
                    print(sku_index)
                    for product_equals_imp in plist:
                        variation = ProductVariation()
                        print(10*"PRODUCT SKU")
                        print(product.sku)
                        print(product_equals_imp[sku_index])
                        if product.sku == product_equals_imp[sku_index]:
                            for config_prop in config_prop_list:
                                attr_product = ast.literal_eval(config_prop['attr_product'])
                                product_prop = ProductProperty()
                                product_prop.name = attr_product['label']
                                product_prop.id = config_prop['name']
                                product_prop.value = product_imp[config_prop['column_index']]
                                variation.properties.append(product_prop)

                            variations.append(variation)

                            # Remove product to list when product is save
                            product_list.pop(product_list.index(product_equals_imp))

                            print(20*"LIST PRODUCT CHANGE --")
                            print(product_list)
                except:
                    pass
                #product_list.pop(product_list.index(product_imp))
                product.product_variations = variations
                product.company = request.user.company
                print(product.name)
                product.save()
        else:
            pass

        return Response({'status': 'Ok request.', 'message': 'The file is correct'}, status.HTTP_200_OK)
    else:
        return Response(
            {
                'status': 'Bad request.',
                'message': 'The file is incorrect.'},
            status.HTTP_400_BAD_REQUEST)


import json


@api_view(['GET',])
def get_product_fields(request, *args, **kwargs):
    product = Product()
    fields = []
    for key in product._fields:
        if key != "company" and key != "properties" and key != "images":
            field = {}
            field['required'] = str(product._fields[key].required)
            field['value'] = key
            field['label'] = str(key).capitalize()
            field['is_prop'] = "False"
            field['type'] = product._fields[key].help_text
            fields.append(field)

    for field_property in request.user.company.cfg_attr_product:
        field = {}
        field['required'] = str(field_property.attr_required)
        field['value'] = str(field_property.id)
        field['label'] = str(field_property.attr_name).capitalize()
        field['is_prop'] = "True"
        field['type'] = field_property.attr_type
        fields.append(field)

    data = json.dumps(fields)
    return HttpResponse(data)


def cast_to_type(value, type):
    if type == 'float':
        return float(value)
    elif type == 'integer':
        return int(value)
    else:
        return value

