from mongoengine import Document, fields

from django.utils.timezone import now as datetime_now


class Order(Document):
    user = fields.ReferenceField("CustomUser")
    date = fields.DateTimeField()
    cancel_date = fields.DateTimeField()
    status = fields.IntField(default=1)
    description = fields.StringField(max_length=255)
    company = fields.ReferenceField("Company")
    user_action = fields.ReferenceField("CustomUser")

    def save(self, *args, **kwargs):
        self.date = datetime_now()
        self.company = self.user.company
        return super(Order, self).save(*args, **kwargs)


class OrderDetail(Document):
    order = fields.ReferenceField(Order)
    catalog_detail = fields.ReferenceField("CatalogDetail")
    product_variation = fields.ObjectIdField()
    price = fields.FloatField()
    quantity = fields.IntField(default=1)
    quantity_dispatch = fields.IntField(default=0)
    company = fields.ReferenceField("Company")

    def save(self, *args, **kwargs):
        self.company = self.order.company
        return super(OrderDetail, self).save(*args, **kwargs)

    def need_quantity(self):
        return self.quantity - self.quantity_dispatch
