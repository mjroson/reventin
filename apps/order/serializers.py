from rest_framework_mongoengine import serializers
from .models import Order, OrderDetail


class OrderDetailSerializer(serializers.EmbeddedDocumentSerializer):
    need_quantity = serializers.serializers.SerializerMethodField()

    class Meta:
        model = OrderDetail
        depth = 1
        exclude = ('company',)

    def get_need_quantity(self, obj):
        return obj.need_quantity()


class OrderSerializer(serializers.DocumentSerializer):
    details = serializers.serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Order
        depth = 2
        fields = ('id', 'description', 'cancel_date', 'date', 'details',)

    def get_details(self, obj):
        details = OrderDetailSerializer(OrderDetail.objects.filter(order=obj.id), many=True)
        return details.data
