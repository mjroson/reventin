from rest_framework_mongoengine.viewsets import ModelViewSet

from .serializers import OrderDetailSerializer, OrderSerializer
from .models import Order, OrderDetail

from apps.utils.views import ObjectGenericView

from bson.objectid import ObjectId
from apps.catalog.models import CatalogDetail

from rest_framework.response import Response
from rest_framework import status
from apps.user.models import CustomUser
from django.db.models import Q
from rest_framework.decorators import list_route

from apps.permission.views import has_permission


class OrderModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = OrderSerializer
    model = Order
    queryset = Order.objects.all()

    @has_permission
    def retrieve(self, request, *args, **kwargs):
        return super(OrderModelViewSet, self).retrieve(request, *args, **kwargs)

    @has_permission
    def update(self, request, *args, **kwargs):
        return super(OrderModelViewSet, self).update(request, *args, **kwargs)

    @has_permission
    def create(self, request, *args, **kwargs):
        if request.DATA and request.DATA.get('details'):
            order = Order()
            if request.DATA.get('user', '') != '':
                try:
                    order.user = CustomUser.objects.get(pk=request.DATA['user'], company= request.user.company)
                    order.user_action = request.user
                except:
                    return Response({'message': "Error user selected"}, status=status.HTTP_400_BAD_REQUEST)
                #CatalogDetail.objects.get(pk=detail.get('id'))
            else:
                order.user = request.user
            order.description = request.DATA.get('description', '')
            if len(request.DATA.get('details', [])) > 0:
                order.save()
                for detail in request.DATA.get('details'):
                    order_detail = OrderDetail()
                    order_detail.order = order
                    c_detail = CatalogDetail.objects.get(pk=detail.get('id'))
                    order_detail.catalog_detail = c_detail
                    order_detail.price = c_detail.custom_price
                    order_detail.quantity = detail.get('quantity')
                    order_detail.product_variation = ObjectId(detail.get('product_variation'))
                    order_detail.save()
        return Response({'message': "Created order success!"}, status=status.HTTP_200_OK)

    @has_permission
    def list(self, request, *args, **kwargs):
        return super(OrderModelViewSet, self).list(request, *args, **kwargs)


    def get_queryset(self):
        if self.kwargs.get('user', '') != '':
            user = self.kwargs['user']
            if user != 'all':
                return Order.objects.filter(user=user)
            else:
                filters = Q(user=user)
                filters = filters | Q(user_action=self.request.user)
                return Order.objects.filter(filters)

        return Order.objects.filter(user=self.request.user, status=1)