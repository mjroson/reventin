from rest_framework_mongoengine.serializers import DocumentSerializer

from .models import Company, Attr_cfg

from rest_framework.permissions import IsAdminUser

class AttrSerializer(DocumentSerializer):
    class Meta:
        model = Attr_cfg
        fields = ('attr_name', 'attr_type', 'attr_default', 'attr_values', 'attr_required', 'id', 'status')
        depth = 1


class CompanySerializer(DocumentSerializer):
    class Meta:
        model = Company
        fields = ('company_name', 'status_stock', 'status_points',
                  'currency', 'country_work',)
        read_only_fields = ('date_register', )



    #def restore_object(self, attrs, instance=None):
    #    if instance is not None:
    #        instance.company_name = attrs.get('company_name', instance.company_name)
    #        instance.category = attrs.get('category', instance.category)
    #        instance.status_stock = attrs.get('status_stock', instance.status_stock)
    #        instance.status_points = attrs.get('status_points', instance.status_points)
    #        instance.currency = attrs.get('currency', instance.currency)
    #        instance.logo = attrs.get('logo', instance.logo)
    #
    #        # (falta) recorrer y armar la lista
    #        # instance.country_work = attrs.get('country_work', instance.country_work)
    #        # instance.cfg_attr_product = attrs.get('cfg_attr_product', instance.cfg_attr_product)
    #        # instance.cfg_attr_user = attrs.get('cfg_attr_user', instance.cfg_attr_user)

    #        return instance
    #    return Company(**attrs)


