import random
import string

from bson.objectid import ObjectId

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View, CreateView, UpdateView
from django.utils.decorators import method_decorator

from django.core.mail import EmailMultiAlternatives

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework_mongoengine.generics import UpdateAPIView, RetrieveAPIView

from apps.permission.views import is_company, user_is_company
from apps.product.models import Product
from apps.user.models import CustomUser
from apps.utils.models import Country

from .forms import RegistrationForm, RequiredDataForm
from .models import Company, Attr_cfg
from .serializers import AttrSerializer, CompanySerializer


class CompanyUpdateApiView(UpdateAPIView):
    serializer_class = CompanySerializer

    def get_queryset(self):
        return Company.objects.get(pk=self.request.user.company)

    def get_object(self):
        if self.request.user.is_company:
            return self.request.user.company
        else:
            return None

    @is_company
    def dispatch(self, request, *args, **kwargs):
        return super(CompanyUpdateApiView, self).dispatch(request, *args, **kwargs)

class CompanyRetrieveApiView(RetrieveAPIView):
    serializer_class = CompanySerializer

    def get_queryset(self):
        return Company.objects.get(pk=self.request.user.company)

    def get_object(self):
        if self.request.user.is_company:
            return self.request.user.company
        else:
            return None


class RegisterCompany(CreateView):
    model = Company
    form_class = RegistrationForm
    template_name = 'company/register.html'
    success_url = '/login/'

    def get_success_url(self):
        return '/login/'

    def form_valid(self, form):
        form.instance.is_active = False
        form.instance.activation_key = ''.join(random.choice(
            string.ascii_uppercase + string.digits) for n in range(20))
        company = Company()
        company.company_name = form.cleaned_data.get('company_name')
        company.activation_key = form.instance.activation_key
        company.save()

        user_email_company = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        CustomUser.create_user_company(username=user_email_company,
                                       password=password, company=company,
                                       email=user_email_company)

        # Arma el contenido del email.
        html_content = 'Bienvenido ' + company.company_name + '. \
            Visite http://localhost:8000/companies/activation/%s/ \
            para activar su cuenta.' % (company.activation_key)

        msg = EmailMultiAlternatives('Invitacion al sistema Reventin',
                                     html_content,
                                     'testnubiquo@gmail.com',
                                     [user_email_company])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()


        messages.success(self.request, "Para complear la registracion debe seguir los paso que le hemos enviado por email")
        return HttpResponseRedirect(self.get_success_url())
        # return super(RegisterCompany, self).form_valid(form)


class ActiveUserView(View):
    """
        Cuando el usuario intenta validar su cuenta por email.
        verifica el codigo que llego por la url y verifica si el
        usuario/Empresa esta inactivo.
    """

    def dispatch(self, *args, **kwargs):
        self.cod_activation = self.kwargs['cod_activation']
        return super(ActiveUserView, self).dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        try:
            self.company = Company.objects.get(
                activation_key=self.cod_activation)
            self.usuario = CustomUser.objects.get(
                company=self.company)

            if not self.usuario.is_active:
                self.usuario.is_active = True
                self.usuario.save()
                # Si la cuenta se activa con exito
                messages.success(self.request, "El usuario se activo con exito!")
                return HttpResponseRedirect("/login/")
            else:
                # Si la cuenta ya esta activa
                messages.success(self.request, "El usuario ya esta activo")
                return HttpResponseRedirect("/login/")
        except Company.DoesNotExist:
            # Si hay algun error en la activacion
            messages.error(self.request, "Error en el codigo de activacion")
            return HttpResponseRedirect('/login/')



class DataRequiredView(UpdateView):
    model = Company
    form_class = RequiredDataForm
    template_name = 'company/data_required.html'
    success_url = '/'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DataRequiredView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        messages.warning(request, "Debe completar los datos minimos requeridos para el correcto uso del sistema.")
        return super(DataRequiredView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, "Puede empezar a usar el sitio, como primer paso te recomendamos cargar tipos de usuarios y productos.")
        return super(DataRequiredView, self).form_valid(form)

    def get_object(self):
        return self.request.user.company



def update_config_attributes(company, config_attr_x, data_attributes):
    """
        Update config attributes users and product
        Params:
            company = Object Company
            config_attr_x = belongs_to (cfg_attr_user or cfg_attr_product)
            data_attributes = new data to update config attributes
    """
    # Si no existe ningun attributo cargado...
    if len(company[config_attr_x]) > 0:

        for cfg_attr in company[config_attr_x]:
            for p in data_attributes:

                if p.get('id', None) and cfg_attr.id == ObjectId(p.get('id', '')):
                    cfg_attr.attr_default = p['attr_default']
                    cfg_attr.attr_required = p.get('attr_required', False)
                    cfg_attr.status = p['status']
                    cfg_attr.attr_values = p['attr_values']

                    if cfg_attr.status == 0: #Si se borra ....
                        if config_attr_x == 'cfg_attr_product':
                            if Product.objects.filter(properties__id = ObjectId(cfg_attr.id)).count() == 0:
                                # Si no existe ningun producto se elimina fisicamente, sino logicamente.
                                company[config_attr_x].remove(cfg_attr)
                        else:
                            if CustomUser.objects.filter(properties__id = ObjectId(cfg_attr.id)).count() == 0:
                                # Si no existe ningun producto se elimina fisicamente, sino logicamente.
                                company[config_attr_x].remove(cfg_attr)

    for cfg_attr_data in data_attributes:
        cfg_attr_id = cfg_attr_data.get('id', '')
        # If the id is empty and status is 1 signifies a new configuration 
        if cfg_attr_id == '' and cfg_attr_data.get('status', 0) != 0:
            cfg_attr_new = Attr_cfg()
            cfg_attr_new.attr_name = cfg_attr_data['attr_name']
            cfg_attr_new.attr_default = cfg_attr_data.get('attr_default', '')
            cfg_attr_new.attr_type = cfg_attr_data['attr_type']
            cfg_attr_new.attr_values = cfg_attr_data.get('attr_values', [])
            cfg_attr_new.attr_required = cfg_attr_data.get('attr_required', False)
            cfg_attr_new.id = ObjectId()
            company[config_attr_x].append(cfg_attr_new)

    return company.save()


@api_view(['PATCH', ])
def config_attributes(request, *args, **kwargs):
    """
        defined to update attribute.
        Request params:
            belongs_to = 'user' or 'product'
    """
    user_is_company(request.user)
    b_to = kwargs.get('belongs_to', '')
    if request.user.is_company:
        belongs_to = 'cfg_attr_user' if b_to == 'user' else 'cfg_attr_product'
        data_attributes = request.DATA.get('properties', [])
        if update_config_attributes(request.user.company, belongs_to, data_attributes):
            return Response({
                                'status': 'Ok request',
                                'message': 'Los attributos del usuario se configuraron correctamente'
                            }, status=status.HTTP_200_OK)
        else:
            return Response({
                                'status': 'Bad request',
                                'message': 'Error to create attribute user'
                            }, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({
                        'status': 'Bad request',
                        'message': 'No tiene permiso para la accion'
                    }, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET', ])
def get_custom_attr(request, *args, **kwargs):
    """
        Define function to return config attributes
        Request params:
            belongs_to = 'user' or 'product'
            filter = 'active' or 'all'
    """
    if request.user.is_company:

        belongs_to = kwargs.get('belongs_to', 'user')
        filter = kwargs.get('filter', 'all')

        if belongs_to == 'user':
            attributes = request.user.company.cfg_attr_user
        else:
            attributes = request.user.company.cfg_attr_product

        attributes_selected = []
        if filter == 'active':
            for attr in attributes:
                if attr.status == 1:
                    attributes_selected.append(attr)
        else:
            attributes_selected = attributes

        cfg_attrs = AttrSerializer(attributes_selected, many=True)

        return Response(cfg_attrs.data, status.HTTP_200_OK)
    else:
        return Response(
            {
                'status': 'Bad request.',
                'message': 'Para acceder a los datos necesita ser un usuario de tipo empresa'},
            status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'POST', ])
def set_data_required(request, *args, **kwargs):
    """
        responde a la peticion del formulario de datos requeridos para poder usar el sistema
    """
    if request.method == 'POST':
        company = request.user.company

        if company:
            logo = request.FILES['logo']

            company.currency = request.DATA['currency']


            countries_work = []

            strcountry = request.DATA['country_work']

            idcountry = ObjectId(str(strcountry))

            country = Country.objects.get(id=idcountry)
            countries_work.append(country)

            company.country_work = countries_work

            if request.DATA['work_stock'] == 'true':
                company.status_stock = True
            else:
                company.status_stock = False

            if request.DATA['work_point'] == 'true':
                company.status_points = True
            else:
                company.status_points = False

            company.logo = logo
            company.save()

            return Response({
                                'status': 'Ok request',
                                'message': 'Set data ok.',
                                'logo_url': '/images/' + str(company.logo._id)
                            }, status=status.HTTP_200_OK)


def show_image(request, md5):
    image = request.user.company.logo.read()
    return HttpResponse(image, content_type="image/" + request.user.company.logo.format)



# class RegisterCompany(CreateAPIView):
#     """
#         New register Company
#         Verify is email not exists.
#         Generate key activation and send email.
#     """
#     serializer_class = CompanySerializer
#
#     def get_permissions(self):
#         if self.request.method in permissions.SAFE_METHODS:
#             return (permissions.AllowAny(),)
#
#         if self.request.method == 'POST':
#             return (permissions.AllowAny(),)
#
#         return (permissions.IsAuthenticated(),)
#
#     def create(self, request, *args, **kwargs):
#         serializer = self.serializer_class(data=request.DATA)
#
#
#         if serializer.is_valid():
#             # verifico si el mail existe registrado como una empresa.
#             is_exist = CustomUser.objects.filter(email__iexact=request.DATA['email'], is_company=True).count()
#             if is_exist == 0:
#                 company = Company()
#                 company.company_name = request.DATA['company_name']
#                 # Genera el codigo de activacion
#                 company.activation_key = ''.join(random.choice(
#                     string.ascii_uppercase + string.digits) for n in range(20))
#
#                 email = request.DATA['email']
#                 password = request.DATA['password']
#
#                 company.save()
#                 # Crea el usuario para la empresa
#                 CustomUser.create_user_company(username=email, password= password, company=company, email=email)
#
#                 # Arma el contenido del email.
#                 html_content = 'Bienvenido ' + company.company_name + '. \
#                     Visite http://localhost:8000/companies/activation/%s/ \
#                     para activar su cuenta.' % (company.activation_key)
#                 msg = EmailMultiAlternatives('Código de Activación',
#                                              html_content,
#                                              'testnubiquo@gmail.com',
#                                              [email])
#                 # Definimos contenido como html
#                 msg.attach_alternative(html_content, 'text/html')
#                 # Enviamos el email
#                 msg.send()
#
#                 return Response(serializer.data, status=status.HTTP_200_OK)
#             else:
#                 return Response({
#                                     'status': 'Bad request',
#                                     'message': 'El email ya se encuentra registrado con una cuenta de tipo empresa'
#                                 }, status=status.HTTP_400_BAD_REQUEST)
#
#         return Response({
#                             'status': 'Bad request',
#                             'message': 'Register could not be created with received data.'
#                         }, status=status.HTTP_400_BAD_REQUEST)