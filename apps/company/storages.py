"""from django.core.files.storage import FileSystemStorage
from proxy_storage.storages.base import ProxyStorageBase
from proxy_storage.meta_backends.mongo import MongoMetaBackend
from apps.company import get_mongo_db

class FileSystemProxyStorage(ProxyStorageBase):
    original_storage = FileSystemStorage(location='/tmp/')
    meta_backend = MongoMetaBackend(
        database=get_mongo_db(),
        collection='meta_backend_collection'
    )
"""