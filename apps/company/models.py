import random
import string
from mongoengine import Document, EmbeddedDocument, fields


class Attr_cfg(EmbeddedDocument):
    OPTIONS = (
        ("TEXT", "Texto corto"),
        ("TEXTAREA", "Texto largo"),
        ("NUMBER", "Numero"),
        ("LIST", 'Lista de valores')
    )
    id = fields.ObjectIdField(primary_key=True)
    attr_name =fields. StringField(max_length=100)
    attr_type = fields.StringField(help_text='En caso de seleccionar Lista de valores, el campo de Lista de valores es obligatorio')
    attr_default = fields.StringField(max_length=255)
    attr_values = fields.ListField(fields.StringField(max_length=50), help_text='Agrega multiples valores separandolos con comas.')
    attr_required = fields.BooleanField(default=False)
    status = fields.IntField(default=1)

    class Meta:
        verbose_name = 'Config Attribute'
        verbose_name_plural = 'Config Attributes'
        allow_inheritance = False

    def __str__(self):
        return self.attr_name


class Company(Document):

    """
        Model Company:
        Fields :
        - company_name = Nombre de la empresa
        - category = Rubro
        - status = estado (borrado logico)
        - status_stock = variable de conf (¿trabaja teniendo cuenta el stock en las ventas?)
        - status_points = variable de conf (¿trabaja con puntos?)
        - date_register = Fecha de registracion
        - currency = Typo de moneda con la que trabaja
        - logo =  Logotipo de la empresa
        - country_work =  Lista de paises en los que trabaja
        - cfg_attr_product = Lista de configuraciones de nuevos atributos para productos
        - cfg_attr_user = Lista de configuraciones de nuevos atributos para usuarios

    """
    MONEY_LIST = (
        ('$', 'ARG'),
        ('B$', 'BRA'),
        ('CH$', 'CH'),
        ('@$', 'PER'),
    )
    company_name = fields.StringField(max_length=255)
    category = fields.StringField(max_length=255)
    status = fields.BooleanField(default=False)
    status_stock = fields.BooleanField(default=False)
    status_points = fields.BooleanField(default=False)
    activation_key = fields.StringField(max_length=30)
    date_register = fields.DateTimeField()
    currency = fields.StringField(choices=MONEY_LIST)
    logo = fields.ImageField(collection_name='images')
    country_work = fields.ListField(fields.ReferenceField('util.Country'))
    cfg_attr_product = fields.ListField(fields.EmbeddedDocumentField(Attr_cfg))
    cfg_attr_user = fields.ListField(fields.EmbeddedDocumentField(Attr_cfg))

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return self.company_name



