from django.conf.urls import patterns, url
from apps.company import views

urlpatterns = patterns('',
                       # REGISTER AND ACTIVATION ACCOUNT
                       url(r'^register/$', views.RegisterCompany.as_view(),
                           name='register'),
                       url(r'^data-required/$', views.DataRequiredView.as_view(),
                           name='data-required'),
                       url(r'^activation/(?P<cod_activation>.+)/$',
                           views.ActiveUserView.as_view(),
                           name='activation'),

                       # # Load company logo
                       url(r'images/(?P<md5>\w+)$', 'apps.company.views.show_image', name='show_image'),
                       )
