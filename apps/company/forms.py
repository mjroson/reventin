from mongodbforms import forms
from mongodbforms import DocumentForm

from .models import Company, Attr_cfg

from apps.user.models import CustomUser


class RegistrationForm(DocumentForm):

    """
        Formulario para registrar una empresa
        valida que el email no exista, y que la verificacion
        de contraseña sean iguales.
    """
    email = forms.EmailField(max_length=200)
    password = forms.CharField(
        label='Contraseña', widget=forms.PasswordInput())
    password_check = forms.CharField(
        label='Ingrese su contraseña nuevamente',
        widget=forms.PasswordInput())

    class Meta:
        document = Company
        fields = ["company_name", ]

    def clean_password_check(self):
        """
            Verifica que las contraseñas cohincidan
        """
        if 'password' in self.cleaned_data:
            password = self.cleaned_data['password']
            password_check = self.cleaned_data['password_check']
            if password == password_check:
                return password_check
            raise forms.ValidationError('Claves no coinciden,')

    def clean_email(self):
        """
            Valida que el email sea unico.
        """
        if CustomUser.objects.filter(
                email__iexact=self.cleaned_data['email']).count():
            raise forms.ValidationError(
                u'Esta direccion de correos ya esta en uso.')

        return self.cleaned_data['email']


class RequiredDataForm(DocumentForm):

    """
        Formulario para completar los datos requeridos,
        para poder empezar a usar el sistema.
    """

    class Meta:
        document = Company
        fields = ['logo', 'currency']
        #excluded = ['company_name', 'date_register', 'activation_key', 'cfg_attr_product']
#
#
# class CreateAttrProductForm(DocumentForm):
#
#     class Meta:
#         document = Attr_cfg
#         excluded = []
#         fields = ['attr_name', 'attr_type', 'attr_values', 'attr_default', 'attr_required']
#         widgets = {'attr_required': forms.CheckboxInput({'class': 'form-control'})}
#
#     def clean_attr_values(self):
#         """
#             Convierte cadena de string a una lista, valida si hay mas de 1 opcion en el caso qe se selecciona
#             attr_type = LIST.
#         """
#         if 'attr_type' in self.cleaned_data:
#             if self.cleaned_data['attr_type'] == 'LIST':
#                 values = self.cleaned_data['attr_values'][0].split(',')
#                 if len(values) > 1:
#                     return values
#                 raise forms.ValidationError(
#                     'Si selecciona una lista, agregar mas de 1 valor separado por coma')
#             else:
#                 self.cleaned_data['attr_values']
#
#
# class AttrCfgCreateForm(DocumentForm):
#
#     class Meta:
#         document = Attr_cfg
#         excluded = []
#         fields = ['attr_name', 'attr_type', 'attr_values', 'attr_default', 'attr_required']
#         widgets = {'attr_required': forms.CheckboxInput({'class': 'form-control'})}
#
#     def clean_attr_values(self):
#         """
#             Convierte cadena de string a una lista, valida si hay mas de 1 opcion en el caso qe se selecciona
#             attr_type = LIST.
#         """
#         if 'attr_type' in self.cleaned_data:
#             if self.cleaned_data['attr_type'] == 'LIST':
#                 values = self.cleaned_data['attr_values'][0].split(',')
#                 if len(values) > 1:
#                     return values
#                 raise forms.ValidationError(
#                     'Si selecciona una lista, agregar mas de 1 valor separado por coma')
#             else:
#                 self.cleaned_data['attr_values']
