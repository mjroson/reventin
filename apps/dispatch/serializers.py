from rest_framework_mongoengine import serializers
from .models import Dispatch, DispatchDetail


class DispatchDetailSerializer(serializers.EmbeddedDocumentSerializer):

    class Meta:
        model = DispatchDetail
        depth = 1
        exclude = ('company', 'status')

class DispatchSerializer(serializers.DocumentSerializer):
    dispatch_details = serializers.serializers.SerializerMethodField(read_only=True)
    #order_link = serializers.serializers.HyperlinkedIdentityField(view_name="Order-detail", read_only=True)

    class Meta:
        model = Dispatch
        depth = 2
        fields= ('order', 'date', 'notes', 'dispatch_details', 'tracking_code', 'id')
        read_only_fields = ('tracking_code', 'id', )

    def get_dispatch_details(self, obj):
        details = DispatchDetailSerializer(DispatchDetail.objects.filter(dispatch=obj.id), many=True)
        return details.data



