from rest_framework_mongoengine.viewsets import ModelViewSet

from .serializers import DispatchDetailSerializer, DispatchSerializer
from .models import Dispatch, DispatchDetail

from apps.utils.views import ObjectGenericView

from rest_framework.response import Response
from rest_framework import status
from bson.objectid import ObjectId
from apps.order.models import OrderDetail, Order

from apps.permission.views import has_permission, is_company, user_is_company

class DispatchModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = DispatchSerializer
    model = Dispatch
    queryset = Dispatch.objects.all()

    @has_permission
    def create(self, request, *args, **kwargs):
        if request.DATA and request.DATA.get('details'):
            if len(request.DATA.get('details', [])) > 0:
                dispatch = Dispatch()
                dispatch.user = request.user

                dispatch.order = Order.objects.get(pk=request.DATA.get('order'))
                dispatch.notes = request.DATA.get('notes', '')
                dispatch.save()
                dispatch_all = request.DATA.get('all', False)
                for detail in request.DATA['details']:
                    dispatch_detail = DispatchDetail()

                    if dispatch_all:
                        dispatch_detail.quantity_dispatched = detail.get('need_quantity')
                    else:
                        dispatch_detail.quantity_dispatched = detail.get('quantity_dispatched')

                    dispatch_detail.dispatch = dispatch

                    o_detail = OrderDetail.objects.get(pk=detail.get('id'))

                    dispatch_detail.order_detail = o_detail

                    o_detail.quantity_dispatch += dispatch_detail.quantity_dispatched
                    o_detail.save()

                    dispatch_detail.company = request.user.company
                    dispatch_detail.save()

                order_is_dispatch_all = True
                for o_detail in OrderDetail.objects.filter(order=dispatch.order):
                    if o_detail.need_quantity() > 0:
                        order_is_dispatch_all = False

                if order_is_dispatch_all == True:
                    dispatch.order.status = 2
                    dispatch.order.save()
            else:
                return Response({'message': "Need details"}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'message': "Create dispatch success!"}, status=status.HTTP_200_OK)

    @has_permission
    def list(self, request, *args, **kwargs):
        user_is_company(request.user)
        return super(DispatchModelViewSet, self).list(request, *args, **kwargs)

    @has_permission
    def retrieve(self, request, *args, **kwargs):
        return super(DispatchModelViewSet, self).retrieve(request, *args, **kwargs)

    @has_permission
    def update(self, request, *args, **kwargs):
        return super(DispatchModelViewSet, self).update(request, *args, **kwargs)