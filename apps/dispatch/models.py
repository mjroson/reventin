import random, string

from mongoengine import Document, fields
from django.utils.timezone import now as datetime_now
from apps.order.models import OrderDetail

class Dispatch(Document):
    order = fields.ReferenceField("Order")
    date = fields.DateTimeField()
    notes = fields.StringField(max_length=255)
    tracking_code = fields.StringField(max_length=255)
    user = fields.ReferenceField("CustomUser")
    status = fields.IntField(default=1)
    company = fields.ReferenceField("Company")

    def save(self, *args, **kwargs):
        self.date = datetime_now()
        self.company = self.user.company
        self.tracking_code = ''.join(random.choice(
                    string.ascii_uppercase + string.digits) for n in range(20))

        return super(Dispatch, self).save(*args, **kwargs)


class DispatchDetail(Document):
    dispatch = fields.ReferenceField(Dispatch)
    order_detail = fields.ReferenceField("OrderDetail")
    quantity_dispatched = fields.IntField(default=1)
    status = fields.IntField(default=1)
    company = fields.ReferenceField("Company")
