# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

import base64
import pickle
import uuid

try:
    # Django >= 1.4.5
    from django.utils.encoding import force_bytes, force_text
except ImportError:
    # Django < 1.4.5
    from django.utils.encoding import (
        smart_unicode as force_text, smart_str as force_bytes)
from django.db import models

from django.dispatch import receiver
from django.db.models.signals import pre_save


from mongoengine import Document, fields
from mongoengine import signals

STATUS_DRAFT = 10
STATUS_PENDING = 20
STATUS_SENT = 30
STATUS_FAILED = 40
STATUS_DISCARDED = 50

PRIORITY_LOW = 20
PRIORITY_STANDARD = 50


class Message(Document):
    STATUS_CHOICES = (
        (STATUS_DRAFT, "Draft"),
        (STATUS_SENT, "Sent"),
        (STATUS_FAILED, "Failed"),
        (STATUS_DISCARDED, "Discarded"),
    )

    uuid = fields.StringField(max_length=40, primary_key=True)

    from_email = fields.StringField(max_length=1024)
    to_email = fields.StringField()
    body_text = fields.StringField()
    body_html = fields.StringField()
    subject = fields.StringField(max_length=1024)

    data = fields.StringField()

    retry_count = fields.IntField(default=-1)
    status = fields.IntField(choices=STATUS_CHOICES, default=STATUS_DRAFT)
    priority = fields.IntField(default=PRIORITY_STANDARD)

    created_at = fields.DateTimeField() #TODO : Add autoadd now
    sent_at = fields.DateTimeField(default=None)
    exception = fields.StringField()

    def get_email_message(self):
        raw_pickle_data = base64.b64decode(force_bytes(self.data))
        return pickle.loads(raw_pickle_data)

    @classmethod
    def from_email_message(cls, email_message, save=False):
        print("LLEGO AL FROM EMAIL")
        kwargs = {
            "from_email": force_text(email_message.from_email),
            "to_email": ",".join(force_text(x) for x in email_message.to),
            "body_text": force_text(email_message.body),
            "subject": force_text(email_message.subject),
            "data": base64.b64encode(pickle.dumps(email_message)),
        }
        print("PORQUE NO ANDA?")
        if hasattr(email_message, "alternatives") and len(email_message.alternatives) > 0:
            html_alts = [x[0] for x in email_message.alternatives if x[1] == "text/html"]
            kwargs["body_html"] = html_alts[0] if len(html_alts) > 0 else ""

        print("CREANDO INSTANCIA")
        instance = cls(**kwargs)
        print(save)
        if save:
            print("ENTRO")
            print(instance)
            instance.save()

        import pdb; pdb.set_trace()
        print(instance)

        print("ASDASDASDASDASDASDASD")

        return instance

    class Meta:
        ordering = ["created_at"]
        verbose_name = "Message"
        verbose_name_plural = "Messages"





#@receiver(pre_save, sender=Message, dispatch_uid='message_uuid_signal')
def generate_uuid(sender, instance, **kwargs):
    if not instance.uuid:
        instance.uuid = str(uuid.uuid1())


signals.pre_save.connect(generate_uuid, sender=Message)