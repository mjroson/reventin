from django.conf.urls import patterns, url

from apps.utils import views

urlpatterns = patterns('',
                       # get all countries
                       url(r'^countries/$', views.CountryListView.as_view(), name='list-countries'),
                       # get all states
                       url(r'^states/$', views.StateListView.as_view(), name='list-states'),
                       # get all cities
                       url(r'^cities/$', views.CityListView.as_view(), name='list-cities'),

                       # method return result search autocomplete city
                       #url(r'^city/autocomplete/$', 'apps.utils.views.city_auto_complete', name='city_auto_complete'),
                       )
