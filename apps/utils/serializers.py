from rest_framework_mongoengine.serializers import DocumentSerializer

from .models import City, State, Country


class CitySerializer(DocumentSerializer):

    class Meta:
        model = City
        depth = 2
        fields = ('name', 'state', 'id')
        read_only_fields = ('state')


class StateSerializer(DocumentSerializer):

    class Meta:
        model = State
        depth = 2
        fields = ('name', 'country', 'id')
        read_only_fields = ('country')


class CountrySerializer(DocumentSerializer):

    class Meta:
        model = Country
        depth = 2
        fields = ('name', 'id')

