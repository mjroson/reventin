from rest_framework_mongoengine.generics import ListAPIView

from .models import City, Country, State
from .serializers import CountrySerializer, StateSerializer, CitySerializer

from bson.objectid import ObjectId
from rest_framework.response import Response
from rest_framework import status




class CountryListView(ListAPIView):
    serializer_class = CountrySerializer
    model = Country
    queryset = Country.objects.all()


class StateListView(ListAPIView):
    serializer_class = StateSerializer
    model = State
    queryset = State.objects.all()


class CityListView(ListAPIView):
    serializer_class = CitySerializer
    model = City
    queryset = City.objects.all()


class ObjectGenericView(object):

    def get_queryset(self):
        return self.queryset.filter(company=self.request.user.company, status=1)

    def pre_save(self, obj):
        obj.company = self.request.user.company
        return obj
        #return super(self.__class__(), self).pre_save(obj)

    #"""
    #Filter that only allows users to see their own objects.
    #"""
    #def filter_queryset(self, request, queryset, view):
    #    return queryset.filter(owner=request.user)

    def perform_create(self, serializer):
        serializer.save(company=self.request.user.company)

    def destroy(self, request, *args, **kwargs):
        pk = ObjectId(kwargs.get('id', None))
        if pk:
            try:
                object = self.model.objects.get(pk=pk)
                object.status = 0
                object.save()
                return Response({
                    'status': 'Ok request',
                    'message': 'Fue eliminado correctamente'
                }, status=status.HTTP_200_OK)
            except self.model.DoesNotExist:
                return Response({
                    'status': 'Bad request',
                    'message': 'El objecto que intentas eliminar, no existe'
                }, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({
                'status': 'Bad request',
                'message': 'Error al eliminar el '
            }, status=status.HTTP_204_NO_CONTENT)

# def city_auto_complete(request):
#     """
#         Toma de la url el parametro term , y busca ciudades que empiecen con
#         los caracteres indicado en dicho parametro, genera un array de json,
#         y lo devuelve
#     """
#     q = request.REQUEST['term']
#
#     if q:
#         cities = City.objects.filter(name_city__istartswith=q)
#     else:
#         cities = []
#
#     cities_list = []
#     for c in cities:
#         value = '%s' % (c.__str__())
#         c_dict = {
#             'id': c.id.__str__(), 'label': value, 'value': value}
#         cities_list.append(c_dict)
#
#     return HttpResponse(simplejson.dumps(cities_list))
