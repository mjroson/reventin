from mongoengine import Document
from mongoengine import StringField, ReferenceField


class Country(Document):
    name = StringField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'



class State(Document):
    name = StringField(max_length=200)
    country = ReferenceField(Country)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'State'
        verbose_name_plural = 'States'


class City(Document):
    name = StringField(max_length=200)
    state = ReferenceField(State)

    def __str__(self):
        return self.name + ", " + self.state.name \
            + ", " + self.state.country.name

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

