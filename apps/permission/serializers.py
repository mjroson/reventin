#from mongoengine.django.auth import Group, ContentType

from rest_framework_mongoengine import serializers

from .models import Permission, PermissionGroup


class PermissionGroupSerializer(serializers.DocumentSerializer):

    class Meta:
        model = PermissionGroup
        exclude = ('company',)
        #depth = 1


class PermissionSerializer(serializers.EmbeddedDocumentSerializer):

    class Meta:
        model = Permission
        depth = 1


# class GroupSerializer(serializers.DocumentSerializer):
#
#     class Meta:
#         model = Group
#         depth = 1
#         exclude = ('id',)
#
#
# class ContentTypeSerializer(serializers.DocumentSerializer):
#
#     class Meta:
#         model = ContentType,
#         depth = 1
