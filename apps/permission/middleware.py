# from django.contrib.auth.decorators import login_required
# from django.conf import settings
#
# EXEMPT_URL_PREFIXES = getattr(settings, 'LOGIN_EXEMPT_URL_PREFIXES', ())
#
# # Middleware to login required,
# # Add in settings:
# 	#LOGIN_EXEMPT_URL_PREFIXES = ('/login/', '/reset_password/')
# # In view.py (function and class)
# 	# add var login_required = True/False (default=True)
#
# class LoginRequiredMiddleware(object):
#     def process_view(self, request, view_func, view_args, view_kwargs):
#         path = request.path
#         for exempt_url_prefix in EXEMPT_URL_PREFIXES:
#             if path.startswith(exempt_url_prefix):
#                 return None
#         is_login_required = getattr(view_func, 'login_required', True)
#         if not is_login_required:
#             return None
#         return login_required(view_func)(request, *view_args, **view_kwargs)
#
#
#
# from apps.user.models import CustomUser
from django.http import HttpResponseRedirect

from django.core.urlresolvers import resolve

class ValidUserdMiddleware(object):

    def process_request(self, request):
        if not request.user.is_authenticated():
            if hasattr(request, 'path') and request.path != '/companies/register/' and request.path != '/login/' and request.path != '/logout/' and resolve(request.path).url_name != 'activation' and resolve(request.path).url_name != 'user-recomendation-create':
                return HttpResponseRedirect('/login/')
            else:
                return None
        else:
            if request.path == '/companies/register/' \
                    or request.path == '/login/':
                return HttpResponseRedirect('/')

            if request.user.is_company:
                if request.user.company.currency: # and len(request.user.company.country_work) > 0:
                    return None
                elif request.path != '/companies/data-required/':
                    return HttpResponseRedirect('/companies/data-required/')
            else:
                return None


