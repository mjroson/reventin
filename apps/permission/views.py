from rest_framework_mongoengine.viewsets import ModelViewSet

from .serializers import PermissionGroupSerializer
#from mongoengine.django.auth import Group, ContentType, Permission


from .models import PermissionGroup
from apps.utils.views import ObjectGenericView
from rest_framework.views import Response
from rest_framework import status

from django.http import HttpResponseRedirect

from .models import PermissionGroup

from apps.user.models import UserType





def get_method(func_name):
    if func_name == 'list':
        return "List"
    elif func_name == 'retrieve':
        return "Detail"
    elif func_name == 'create':
        return "Create"
    elif func_name == 'update':
        return "Edit"
    else:
        return ""



def has_permission(func):
    """
        Decorator to add permissions in view
    """
    def wrapper(view, request, *args, **kwargs):
        # set method name
        method = get_method(func.__name__)

        if request.user.is_company == True:
            return func(view, request, *args, **kwargs)

        permissions_group = request.user.user_type.permission_group
        if permissions_group:
            for permission in permissions_group.permissions:
                if permission.entity == str(view.model._class_name).upper():
                    for action in permission.actions:
                        if action.name == method:
                            if action.value == True:
                                return func(view, request, *args, **kwargs)
                            else:
                                return Response({"message": "No tenes permiso para realizar la accion."},
                                                status=status.HTTP_401_UNAUTHORIZED)


        return Response({"message": "No tenes permiso para realizar la accion."},
                        status=status.HTTP_401_UNAUTHORIZED)

    return wrapper


def is_company(func):

    def wrapper(view, request, *args, **kwargs):
        if request.user.is_company == True:
            return func(view, request, *args, **kwargs)
        else:
            return Response({"message": "No tenes permiso para realizar la accion."},
                        status=status.HTTP_401_UNAUTHORIZED)


def user_is_company(user):
    print("ANDA")
    print(user)
    if not user.is_company:
        print("NO ES COMPANIA")
        return Response({"message": "No tenes permiso para realizar la accion."},
                        status=status.HTTP_401_UNAUTHORIZED)

class PermissionModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = PermissionGroupSerializer
    model = PermissionGroup
    queryset = PermissionGroup.objects.all()


# def permission_classes(permission_classes):
#     def decorator(func):
#         func.permission_classes = permission_classes
#         return func
#     return decorator

# class GroupModelViewSet(ModelViewSet):
#     serializer_class = GroupSerializer
#     model = Group
#
#     def get_queryset(self):
#         return Group.objects.all()
#
#
# class ContentTypeModelViewSet(ModelViewSet):
#     serializer_class = ContentTypeSerializer
#     model = ContentType
#
#     def get_queryset(self):
#         return ContentType.objects.all()
