from mongoengine import Document, EmbeddedDocument,fields


ENTITIES = (
        ("USER", "User"),
        ("PRODUCT", "Product"),
        ("CATEGORY", "Category product"),
        ("ROLE", "Roles"),
        ("CATALOG", "Catalog"),
        ("PRICELIST", "Price List"),
        ("ORDER", "Order"),
        ("DISPATCH", "Dispatch")
    )

OPTIONS = (
        ("0", "List"),
        ("1", "Detail"),
        ("2", "Delete"),
        ("3", 'Create'),
        ("4", "Edit")
    )

class ActionDetail(EmbeddedDocument):
    name = fields.StringField(max_length=200)
    value = fields.BooleanField(default=False)


class Permission(EmbeddedDocument):
    entity = fields.StringField(required=True)
    actions = fields.ListField(fields.EmbeddedDocumentField(ActionDetail))


class PermissionGroup(Document):
    name = fields.StringField(max_length=100)
    description = fields.StringField(max_length=255)
    permissions = fields.ListField(fields.EmbeddedDocumentField(Permission))
    company = fields.ReferenceField("Company")
    status = fields.IntField(default=1)
    
    class Meta:
        verbose_name = 'Role'
        verbose_name_plural = 'Roles'

