from mongoengine import Document, fields


class Catalog(Document):
    name = fields.StringField(max_length=200)
    company = fields.ReferenceField('Company')
    description = fields.StringField(max_length=255)
    start_date = fields.DateTimeField(required=True)
    end_date = fields.DateTimeField(required=True)
    status = fields.IntField(default=1)
    # 1 = Active , 0 = Inactive
    factor = fields.FloatField(default=0)

    def __str__(self):
        return self.name


class CatalogDetail(Document):
    catalog = fields.ReferenceField(Catalog)

    product = fields.ReferenceField('Product')
    product_sku = fields.StringField(max_length=80, required=True)
    product_name = fields.StringField(max_length=100, required=True)
    product_category = fields.ReferenceField('Category')
    product_price = fields.FloatField()
    product_score = fields.IntField(default=0)
    product_description = fields.StringField(max_length=255)
    product_variations = fields.ListField(fields.EmbeddedDocumentField('ProductVariation'))
    custom_price = fields.FloatField()
    edit_custom_price = fields.IntField(default=0)
    # Si el valor es 0, el precio fue calculado del factor comun de aumento del catalogo
    # Si el valor es 1, el precio fue editado manualmente

    status = fields.IntField(default=1)
    # 1 = Active , 0 = Inactive
    company = fields.ReferenceField('Company')
