from _curses import def_shell_mode
from rest_framework_mongoengine import serializers

from apps.product.models import Category, Product, ProductProperty

from .models import Catalog, CatalogDetail
from django.utils.timezone import now as datetime_now
from datetime import datetime
#from drf_compound_fields.fields import ListField


class CatalogDetailSerializer(serializers.DocumentSerializer):

    class Meta:
        model = CatalogDetail
        depth = 3
        fields = ('id', 'product_sku', 'product_name', 'custom_price', 'product_variations', 'product_description', 'product_score')


    def save(self, **kwargs):
        pass


class CatalogSerializer(serializers.DocumentSerializer):
    catalog_status = serializers.serializers.SerializerMethodField()

    class Meta:
        model = Catalog
        fields = (
            'id',
            'start_date',
            'name',
            'end_date',
            'description',
            'catalog_status',
            'factor')

    def get_catalog_status(self, obj):
        """ STATUS OPT:
                0 : Catalogo vencido
                1 : Catalogo vigente
                2 : Catalogo en espera
        """
        # TODO: Cuando esta en el mismo dia la fecha de vencimiendo, el catalogo queda como vencido, deberia ser vigente?
        now = datetime.today()
        if obj.start_date < now and obj.end_date < now:
            return 0
        elif obj.start_date < now and obj.end_date >= now:
            return 1
        elif obj.start_date > now:
            return 2
