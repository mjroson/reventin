from datetime import datetime

from rest_framework_mongoengine.viewsets import ModelViewSet
from rest_framework_mongoengine.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

from bson.objectid import ObjectId

from .models import CatalogDetail, Catalog
from .serializers import CatalogDetailSerializer, CatalogSerializer

from apps.product.models import Product
from apps.product.serializers import ProductSerializer

from apps.utils.views import ObjectGenericView
from apps.permission.views import has_permission


class ActiveCatalogDetailListView(ListAPIView):
    serializer_class = CatalogDetailSerializer
    queryset = CatalogDetail.objects.all()

    def get_queryset(self):
        # TODO : Set date dynamic
        now = datetime.now()
        date = str(now.year) + '-' + str(now.month) + '-' + str(now.day)
        catalogs = Catalog.objects.filter(start_date__lt = date, end_date__gte = date, company=self.request.user.company).values_list('id')
        return CatalogDetail.objects.filter(catalog__in = catalogs)


def create_detail_catalog(catalog, products, company):

    if products and len(products) > 0:
        for product in products:

            product_obj = Product.objects.get(pk=product['id'], company=company)
            if product_obj:
                catalog_detail = CatalogDetail()
                catalog_detail.product_category = product_obj.category
                catalog_detail.product_description = product_obj.description
                catalog_detail.product_name = product_obj.name
                catalog_detail.product_price = float(product_obj.sale_price)
                catalog_detail.product_variations = product_obj.product_variations
                catalog_detail.product_score = int(product_obj.score)
                catalog_detail.product_sku = product_obj.sku
                catalog_detail.product = product_obj
                catalog_detail.custom_price = product.get(
                    'custom_price', 0)
                if product.get('edit_custom_price', None):
                        catalog_detail.edit_custom_price = 1
                catalog_detail.catalog = catalog

                catalog_detail.save()


class CatalogModelViewSet(ObjectGenericView, ModelViewSet):
    serializer_class = CatalogSerializer
    model = Catalog
    queryset = Catalog.objects.all()

    @has_permission
    def retrieve(self, request, *args, **kwargs):
        catalog = self.get_object()
        catalog_serializer = self.get_serializer(catalog)
        catalog_details_serializer = CatalogDetailSerializer(
            CatalogDetail.objects.filter(catalog=catalog), many=True)
        return Response({
            'catalog': catalog_serializer.data,
            'catalog_details': catalog_details_serializer.data
        }, status=status.HTTP_200_OK)

    @has_permission
    def destroy(self, request, *args, **kwargs):
        catalog = Catalog.objects.get(pk=ObjectId(kwargs.get('id', None)))
        if catalog.start_date > datetime.today():
            return super(CatalogModelViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response({
                'status': 'Bad request',
                'message': 'The catalog has to be in the "waiting" to be removed.'
            }, status=status.HTTP_200_OK)

    @has_permission
    def create(self, request, *args, **kwargs):
        # try:
        catalog_data = request.DATA.get('catalog', None)
        catalog = Catalog()
        if catalog_data:
            catalog.description = catalog_data['description']
            catalog.company = request.user.company
            catalog.end_date = str(catalog_data['end_date']).split("T")[0]
            catalog.start_date = str(catalog_data['start_date']).split("T")[0]
            catalog.name = catalog_data['name']
            catalog.factor = float(catalog_data['factor'])
            catalog.save()

        products = request.DATA.get('products_select', [])

        if products and len(products) > 0:
            for product in products:
                catalog_detail = CatalogDetail()

                product_obj = Product.objects.get(
                    pk=product['id'], company=request.user.company)
                if product_obj:
                    catalog_detail.product_category = product_obj.category
                    catalog_detail.product_description = product_obj.description
                    catalog_detail.product_name = product_obj.name
                    catalog_detail.product_price = float(
                        product_obj.sale_price)
                    catalog_detail.product_variations = product_obj.product_variations
                    catalog_detail.product_score = int(product_obj.score)
                    catalog_detail.product_sku = product_obj.sku
                    catalog_detail.product = product_obj
                    catalog_detail.custom_price = product.get('custom_price', 0)
                    if product.get('edit_custom_price', None):
                        catalog_detail.edit_custom_price = 1

                    catalog_detail.catalog = catalog

                    catalog_detail.save()
        return Response({
            'status': 'Ok request',
            'message': 'The catalog was created successfully'
        }, status=status.HTTP_200_OK)

    @has_permission
    def update(self, request, *args, **kwargs):
        if kwargs.get('id', None):
            catalog = Catalog.objects.get(pk=ObjectId(kwargs['id']))

            # Si el catalogo esta en "espera"
            if catalog.start_date > datetime.today():
                CatalogDetail.objects.filter(catalog=catalog).delete()
                catalog_data = request.DATA.get('catalog', None)
                if catalog_data:
                    catalog.description = catalog_data['description']
                    catalog.end_date = str(catalog_data['end_date']).split("T")[0]
                    catalog.start_date = str(catalog_data['start_date']).split("T")[0]
                    catalog.name = catalog_data['name']
                    catalog.factor = float(catalog_data['factor'])
                    catalog.save()

                    products = request.DATA.get('products_select', [])
                    create_detail_catalog(catalog, products, request.user.company)

            # Si el catalogo esta vigente
            elif catalog.end_date > datetime.today():
                catalog_data = request.DATA.get('catalog', None)
                if catalog_data:
                    catalog.end_date = str(catalog_data['end_date']).split("T")[0]
                    catalog.save()

                    products = request.DATA.get('products_select', [])

                    for product in products:
                        #Si no existe un detalle de catalogo con el id del producto que se va a agregar. Se guarda un detalle nuevo.
                        if CatalogDetail.objects.filter(catalog=catalog, product=ObjectId(product['id']), company=request.user.company).count() == 0:
                            product_obj = Product.objects.get(pk=product['id'], company=request.user.company)
                            if product_obj:
                                catalog_detail = CatalogDetail()
                                catalog_detail.product_category = product_obj.category
                                catalog_detail.product_description = product_obj.description
                                catalog_detail.product_name = product_obj.name
                                catalog_detail.product_price = float(product_obj.sale_price)
                                catalog_detail.product_variations = product_obj.product_variations
                                catalog_detail.product_score = int(product_obj.score)
                                catalog_detail.product_sku = product_obj.sku
                                catalog_detail.product = product_obj
                                catalog_detail.custom_price = product.get('custom_price', 0)
                                if product.get('edit_custom_price', None):
                                    catalog_detail.edit_custom_price = 1

                                catalog_detail.catalog = catalog

                                catalog_detail.save()
            else:
                return Response({
                    'status': 'Bad request',
                    'message': 'The catalog has to be in the "waiting" to be update.'
                }, status=status.HTTP_200_OK)

            return Response({
                'status': 'Ok request',
                'message': 'The catalog was updated successfully'
            }, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_data_to_update_catalog(request, *args, **kwargs):
    """
        Funcion que devuelve los datos necesarios para poder editar un catalogo,
        dependiendo del estado del catalogo, son los datos que devuelve.
    """
    response = {}
    #status = status.HTTP_200_OK
    if kwargs.get('id', None):

        catalog = Catalog.objects.get(pk=ObjectId(kwargs['id']))

        catalog_details = CatalogDetail.objects.filter(catalog=catalog.id)

        cat_serializer = CatalogSerializer(catalog)

        response['catalog'] = cat_serializer.data
        response['catalog_details'] = CatalogDetailSerializer(catalog_details, many=True).data

        details_product_id = []
        if len(catalog_details) > 0:
            for details in catalog_details:
                details_product_id.append(details.product.id)
        # Si el catalogo esta "en espera", le mando el listado de productos con
        # los productos ya seleccionados
        if catalog.start_date > datetime.today():
            # Productos que estan en el catalogo
            products_is = Product.objects.filter(id__in=details_product_id)
            # Productos que no estan en el catalogo
            products_isnt = Product.objects.filter(id__not__in=details_product_id)

            products = []
            products_select = []

            for prod in products_is:
                cat_detail = CatalogDetail.objects.get(catalog=catalog, product=prod)
                product = {}
                product['id'] = str(prod.id)
                product['custom_price'] = cat_detail.custom_price
                product['edit_custom_price'] = False
                if cat_detail.edit_custom_price:
                    product['edit_custom_price'] = True
                product['check'] = True
                product['sale_price'] = prod.sale_price
                product['name'] = prod.name

                products_select.append(product)
                products.append(product)

            for prod in products_isnt:
                product = {}
                product['id'] = str(prod.id)
                product['custom_price'] = 0
                product['edit_custom_price'] = False
                product['check'] = False
                product['sale_price'] = prod.sale_price
                product['name'] = prod.name

                products.append(product)

            response['products'] = products
            response['products_select'] = products_select


        # Si el catalogo esta vigente (solo le mando la lista de productos que puede agregar)
        elif catalog.end_date > datetime.today():


                if len(details_product_id) > 0:
                    # Productos que no estan en el catalogo
                    products = Product.objects.filter(id__not__in=details_product_id)
                    response['products'] = ProductSerializer(products, many=True).data
        else:
            response['status'] = 'Bad Request'
            response['message'] = 'The catalog has to be in the "waiting" to be removed.'
            #status = status.HTTP_404_NOT_FOUND

        return Response(response)
